#!/bin/bash

java -Xmx8g -cp target/parallel.corpus-2.1.0-SNAPSHOT.jar:target/dependency/* ca.concordia.clac.discourse.AnnotationProjectionJSonFile "$@"
