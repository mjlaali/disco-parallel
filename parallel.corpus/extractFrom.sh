#!/bin/bash
java -Xmx100g -cp "target/parallel.corpus-0.0.1-SNAPSHOT.jar:target/dependency/*" -Dlog4j.configurationFile=src/main/java/log4j2.xml ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator -i $1 -o $2 -t 1
