Problem:
Some discourse connectives like 'que' in French are frequently appeared in the French text. 
If we remove all French texts with more than one discourse connective, we may end up with a small training set.

Potential Solution:
We may be able to learn a discourse parser in each iteration and disambiguate each French discourse connective before generating the dataset.
Note that, we may still have the problem because even with gold dataset, we cannot disambiguate all discourse connective with high accuracy just by using the syntactic features.
 