package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

public class OchAlignmentReaderTest {

	@SuppressWarnings("unchecked")
	@Test
	public void whenReadingAnAlignment(){
		String alignment = "0-0 1-1 2-2 3-3";
		IBMAlignmentReader ibmReader = mock(IBMAlignmentReader.class);
		String langs = "en-fr";
		
		OchAlignmentReader reader = new OchAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), ibmReader, langs);
		
		reader.next();
		List<Pair<Integer, Integer>> alignments = reader.getAlignments();
		
		assertThat(alignments).containsOnly(
				new Pair<>(0, 0),
				new Pair<>(1, 1),
				new Pair<>(2, 2),
				new Pair<>(3, 3)
			);
		
		reader.close();
		verify(ibmReader, times(1)).next();
		verify(ibmReader, times(1)).close();
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenChangingLangs(){
		String alignment = "0-0 1-1 2-2 2-3";
		IBMAlignmentReader ibmReader = mock(IBMAlignmentReader.class);
		
		OchAlignmentReader reader = null;
		List<Pair<Integer, Integer>> alignments = null;
		
		reader = new OchAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), ibmReader, "en-fr");
		
		reader.next();
		alignments = reader.getAlignments();
		
		assertThat(alignments).containsOnly(
				new Pair<>(0, 0),
				new Pair<>(1, 1),
				new Pair<>(2, 2),
				new Pair<>(2, 3)
			);
		
		reader.close();
		
		reader = new OchAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), ibmReader, "fr-en");
		
		reader.next();
		alignments = reader.getAlignments();
		
		assertThat(alignments).containsOnly(
				new Pair<>(0, 0),
				new Pair<>(1, 1),
				new Pair<>(2, 2),
				new Pair<>(3, 2)
				);
		
		reader.close();
	}
}
