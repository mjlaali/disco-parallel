package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

public class IntersectAlignmentTest {

	@SuppressWarnings("unchecked")
	@Test
	public void whenMakingAnIntersection(){
		AlignmentReader first = mock(AlignmentReader.class);
		AlignmentReader second = mock(AlignmentReader.class);
		
		when(first.getAlignments()).thenReturn(Arrays.asList(new Pair<>(1, 1), new Pair<>(1, 2), new Pair<>(2, 2)));
		when(second.getAlignments()).thenReturn(Arrays.asList(new Pair<>(1, 1), new Pair<>(2, 1), new Pair<>(2, 2)));
		
		AlignmentReader intersect = new IntersectAlignment(first, second);
		intersect.next();
		verify(first, times(1)).next();
		verify(second, times(1)).next();
		
		assertThat(intersect.getAlignments()).containsOnly(new Pair<>(1, 1), new Pair<>(2, 2));
		
		intersect.close();
		verify(first, times(1)).close();
		verify(second, times(1)).close();

	}
}
