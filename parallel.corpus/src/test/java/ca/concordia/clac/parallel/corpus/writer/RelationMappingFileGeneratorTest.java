package ca.concordia.clac.parallel.corpus.writer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

public class RelationMappingFileGeneratorTest {
	Lexconn lexconn, lexicon; 
	RelationMappingFileGenerator relationMappingFileGenerator;
	@Before
	public void setup(){
		lexconn = new Lexconn(Arrays.asList(new LexconnEntry[]{
				new LexconnEntry("a", "l1,l2", Arrays.asList("a1", "a2")),
				new LexconnEntry("b", "l2, l3", Arrays.asList("b1")),
				new LexconnEntry("c", "l1, l3", Arrays.asList("c1", "c2")),
		}));
		
		lexicon = new Lexconn(Arrays.asList(new LexconnEntry[]{
				new LexconnEntry("a", Arrays.asList("a1", "a2"), 50, ImmutableMap.of("p1", 1.0)),
				new LexconnEntry("b", Arrays.asList("b1"), 10, ImmutableMap.of("p1", 0.4, "p2", 0.6)),
		}));
		
		relationMappingFileGenerator = new RelationMappingFileGenerator(lexconn, lexicon);

	}
	
	@Test
	public void whenPrunedThenNotUsedRelationsAreRemoved() throws IOException{
		
		assertThat(relationMappingFileGenerator.getDcId()).containsOnly(entry("a", 0), entry("b", 1));
		assertThat(relationMappingFileGenerator.getLexconnRelId()).containsOnly(entry("l1", 0), entry("l2", 1), entry("l3", 2));
		assertThat(relationMappingFileGenerator.getPdtbRelId()).containsOnly(entry("p1", 0), entry("p2", 1));

		relationMappingFileGenerator.prun(50);
		assertThat(relationMappingFileGenerator.getDcId()).containsOnly(entry("a", 0));
		assertThat(relationMappingFileGenerator.getLexconnRelId()).containsOnly(entry("l1", 0), entry("l2", 1));
		assertThat(relationMappingFileGenerator.getPdtbRelId()).containsOnly(entry("p1", 0));

	}
	
	@Test
	public void whenExtractEmission() throws IOException{
		List<List<Double>> emission = relationMappingFileGenerator.generateEmission();
		
		assertThat(emission).hasSize(2);
		assertThat(emission.get(0)).containsExactly(1.0, 0.0);
		assertThat(emission.get(1)).containsExactly(0.4, 0.6);
	}
	
	@Test
	public void whenExtractEntries() throws IOException{
		List<List<Integer>> entries = relationMappingFileGenerator.generateEntries();
		assertThat(entries).hasSize(4);
		assertThat(entries.get(0)).containsExactly(0, 0);
		assertThat(entries.get(1)).containsExactly(0, 1);
		assertThat(entries.get(2)).containsExactly(1, 1);
		assertThat(entries.get(3)).containsExactly(1, 2);
	}
	
	@Test
	public void whenPrintingEmission() throws IOException{
		List<List<Double>> emission = relationMappingFileGenerator.generateEmission();
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		RelationMappingFileGenerator.print(emission, new OutputStreamWriter(output));
		assertThat(output.toString()).isEqualTo("1.0 0.0\n0.4 0.6\n");
		
	}
	
	@Test
	public void whenPrintingEntries() throws IOException{
		List<List<Integer>> entries = relationMappingFileGenerator.generateEntries();
		
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		RelationMappingFileGenerator.print(entries, new OutputStreamWriter(output));
		assertThat(output.toString()).isEqualTo("0 0\n0 1\n1 1\n1 2\n");
		
	}
}
