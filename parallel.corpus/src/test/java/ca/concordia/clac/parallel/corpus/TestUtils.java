package ca.concordia.clac.parallel.corpus;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.util.CasIOUtil;
import org.apache.uima.jcas.JCas;
import org.xml.sax.SAXException;

import ca.concordia.clac.batch_process.BatchProcess;

public class TestUtils {
	private File outputDir;

	public TestUtils(String sample, String... toBeCleanedProcesses) throws URISyntaxException, IOException, UIMAException, SAXException, CpeDescriptorException{
		File inputDir = new File("resources/" + sample);
		outputDir = new File("outputs/FrenchTrainingDataBuilderTest/" + sample);
    	
    	if (!outputDir.exists())
    		outputDir.mkdirs();
    	
    	BatchProcess process = new ParallelCorpusAnnotator().getBatchProcess(inputDir, outputDir);
    	if (toBeCleanedProcesses.length == 0)
    		process.clean();
    	else{
    		for (String toBeCleanedProcess: toBeCleanedProcesses)
    			process.clean(toBeCleanedProcess);
    	}
		process.run();
	}

	public JCas getJCas(String processName, String fileName) throws UIMAException, IOException {
		JCas aJCas = JCasFactory.createJCas();
		File aFile = new File(outputDir, processName + "/" + fileName);
		
		CasIOUtil.readJCas(aJCas, aFile);
		return aJCas;
	}
	
	public File getOutputDir() {
		return outputDir;
	}
}
