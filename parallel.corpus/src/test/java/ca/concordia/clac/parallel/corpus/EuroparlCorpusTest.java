package ca.concordia.clac.parallel.corpus;

import java.io.File;
import java.io.IOException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.ParalleDocumentTextReader;
import org.cleartk.corpus.europarl.ParallelFileCollectionReader;

public class EuroparlCorpusTest {

	//check the number of lines of all files are equal.
	public static void main(String[] args) throws UIMAException, IOException {
		File inputDir = new File("outputs/en-fr/");
		CollectionReaderDescription reader = ParallelFileCollectionReader.getReaderDescription(inputDir, "en", "fr");

		SimplePipeline.runPipeline(reader, 
				ParalleDocumentTextReader.getDescription(),
				EuroparlParallelTextAnnotator.getDescription());
		
	}
}
