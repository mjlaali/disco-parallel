package ca.concordia.clac.parallel.corpus.writer;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.Test;

public class LexconnTest {

	@Test
	public void whenRequestingForRelations(){
		Lexconn lexconn = new Lexconn(Arrays.asList(new LexconnEntry[]{
				new LexconnEntry("a", "r1,r2", null),
				new LexconnEntry("a", "r2, r3", null),
				new LexconnEntry("a", "r1, r3", null),
		}));
		
		assertThat(lexconn.getRelations()).containsOnly("r1", "r2", "r3");
	}
	
	@Test
	public void whenRequestingForIds(){
		Lexconn lexconn = new Lexconn(Arrays.asList(new LexconnEntry[]{
				new LexconnEntry("a1", "r1,r2", null),
				new LexconnEntry(" a2", "r2, r3", null),
				new LexconnEntry("a3 ", "r1, r3", null),
		}));
		
		assertThat(lexconn.getIds()).containsOnly("a1", "a2", "a3");
	}
}
