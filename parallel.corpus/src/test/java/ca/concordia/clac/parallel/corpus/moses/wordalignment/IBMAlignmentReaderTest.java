package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.util.List;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

public class IBMAlignmentReaderTest {
	String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
			"ws1 ws2 ws3 ws4\n" +  
			"NULL ({ }) wt1 ({ 1 }) wt2 ({ 2 }) wt3 ({ 3 }) wt4 ({ 4 })\n";

	@Test
	public void whenReadingAlignmentThenWordsAreCorrect(){
		
		IBMAlignmentReader reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		
		reader.next();
		
		assertThat(reader.getEnWords()).containsOnly("ws1", "ws2", "ws3", "ws4");
		assertThat(reader.getFrWords()).containsOnly("wt1", "wt2", "wt3", "wt4");
		
		reader.close();
		
		reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "fr-en");

		reader.next();
		
		assertThat(reader.getFrWords()).containsOnly("ws1", "ws2", "ws3", "ws4");
		assertThat(reader.getEnWords()).containsOnly("wt1", "wt2", "wt3", "wt4");

		reader.close();
	}
	
	@Test (expected=RuntimeException.class)
	public void whenThePatternDoesNotMatchThrowAnExeption(){
		String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
				"ws1 ws2 ws3 ws4\n" +  
				"NULL ({ }) wt1 ({ 1 }) wt2 ({ 2 }) wt3 ({ 3 }) wt4 ({ 4 }) wt5\n";
		IBMAlignmentReader reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		try{
			reader.next();
		} finally{
			reader.close();
		}
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void whenReadingAlignmentThenAlignmentsAreCorrect(){
		String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
				"ws1 ws2 ws3 ws4\n" +  
				"NULL ({ }) wt1 ({ 4 }) wt2 ({ 3 }) wt3 ({ 2 }) wt4 ({ 1 })\n";

		IBMAlignmentReader reader ;
		List<Pair<Integer, Integer>> alignments;

		reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		reader.next();
		alignments = reader.getAlignments();
		
		assertThat(alignments).containsExactly(
				new Pair<>(3, 0),
				new Pair<>(2, 1),
				new Pair<>(1, 2),
				new Pair<>(0, 3)
				);
		
		reader.close();
		
		reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "fr-en");
		
		reader.next();
		alignments = reader.getAlignments();
		assertThat(alignments).containsExactly(
				new Pair<>(0, 3),
				new Pair<>(1, 2),
				new Pair<>(2, 1),
				new Pair<>(3, 0)
				);
		reader.close();
	}
	
	@Test
	@SuppressWarnings("unchecked")
	public void whenRaedingManyToOneAlignment(){
		
		String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
				"ws1 ws2 ws3 ws4\n" +  
				"NULL ({ }) wt1 ({ 1 2 }) wt2 ({ }) wt3 ({ 3 }) wt4 ({ 4 })\n";

		
		IBMAlignmentReader reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		
		reader.next();
		List<Pair<Integer, Integer>> alignments = reader.getAlignments();
		
		assertThat(alignments).containsOnly(
				new Pair<>(0, 0),
				new Pair<>(1, 0),
				new Pair<>(2, 2),
				new Pair<>(3, 3)
				);
		
		reader.close();
	}
	
	@Test
	public void whenThereIsNoAlignment(){
		
		String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
				"ws1\n" +  
				"NULL ({ 1 }) wt1 ({ })\n";

		
		IBMAlignmentReader reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		
		reader.next();
		List<Pair<Integer, Integer>> alignments = reader.getAlignments();
		
		assertThat(alignments).isEmpty();
		
		reader.close();
	}
	
	@Test
	public void whenContainingHtmlTag(){
		String alignment = "# Sentence pair (1) source length 4 target length 4 alignment score : 0.000454191\n"+
				"&amp; &#35; &apos; @-@\n" +  
				"NULL ({ 1 }) wt1 ({ })\n";
		
		IBMAlignmentReader reader = new IBMAlignmentReader(new ByteArrayInputStream(alignment.getBytes()), "en-fr");
		
		reader.next();

		List<String> enWords = reader.getEnWords();
		
		assertThat(enWords).containsExactly("&", "#", "'", "-");
		
		reader.close();
	}
}
