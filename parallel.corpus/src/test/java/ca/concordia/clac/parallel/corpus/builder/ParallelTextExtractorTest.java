package ca.concordia.clac.parallel.corpus.builder;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.conll2015.DiscourseRelationExample;
import org.cleartk.corpus.conll2015.DiscourseRelationFactory;
import org.cleartk.discourse.type.DiscourseRelation;
import org.junit.Before;
import org.junit.Test;

public class ParallelTextExtractorTest {
	private JCas aJCas;
	private final String OUTPUT_VIEW = "outputView";
	
	@Before
	public void setup() throws UIMAException {
		aJCas = JCasFactory.createJCas();
	}
	
	@Test
	public void dcAugmentedWithItsSense() throws CASException, AnalysisEngineProcessException, ResourceInitializationException{
		KongExample example = new KongExample();
		DiscourseRelation discourseRelation = new DiscourseRelationFactory().makeDiscourseRelationFrom(aJCas, example);
		discourseRelation.addToIndexesRecursively();
		SimplePipeline.runPipeline(aJCas, ParallelTextExtractor.getEngineDescription(false, true, OUTPUT_VIEW));
		JCas view = aJCas.getView(OUTPUT_VIEW);
		String expected = example.getText().replace("so", "so__" + example.getSense());
		assertThat(view.getDocumentText()).isEqualTo(expected);
	}
	
	@Test
	public void multiWordDCsCreateASignleUnit() throws CASException, AnalysisEngineProcessException, ResourceInitializationException{
		DiscourseRelationExample example = new ForExampleExample();
		DiscourseRelation discourseRelation = new DiscourseRelationFactory().makeDiscourseRelationFrom(aJCas, example);
		discourseRelation.addToIndexesRecursively();
		
		SimplePipeline.runPipeline(aJCas, ParallelTextExtractor.getEngineDescription(false, true, OUTPUT_VIEW));
		
		JCas view = aJCas.getView(OUTPUT_VIEW);
		String expected = example.getText().replace("for example", "for_example__" + example.getSense());
		assertThat(view.getDocumentText()).isEqualTo(expected);
	}

}
