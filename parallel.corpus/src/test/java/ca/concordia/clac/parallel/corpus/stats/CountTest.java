package ca.concordia.clac.parallel.corpus.stats;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.xml.sax.SAXException;

import ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator;
import ca.concordia.clac.parallel.corpus.TestUtils;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

@Ignore
public class CountTest {
	public static final String sample = "annotationProjectionTest";
	public static final String FILE_UNDER_THE_TEST = "a.txt.xmi";
	
	private static TestUtils testUtils;
	
	private JCas jCas;
	private File outputFld;
	private AnalysisEngineDescription counterEngine;
	
	@BeforeClass
	public static void initialize() throws UIMAException, IOException, URISyntaxException, SAXException, CpeDescriptorException{
		testUtils = new TestUtils(sample, "nothing");
		
	}
	
	@Before
	public void setup() throws UIMAException, IOException, URISyntaxException{
		jCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_DISCOURSEPARSING_EN, FILE_UNDER_THE_TEST);
		File dcList = new File("resources/french-dc.txt");
		outputFld = new File("outputs/stats");
		
		counterEngine = Count.getWriterDescription(dcList.toURI().toURL(), outputFld);
	}
	
	@Test
	public void extractStats() throws Exception{
		SimplePipeline.runPipeline(jCas, counterEngine);
		
		File file = new File(outputFld, "/training-data.arff");
		DataSource source = new DataSource(new FileInputStream(file));
		Instances instances = source.getDataSet();
		
		assertThat(instances).hasSize(9);
	}
}
