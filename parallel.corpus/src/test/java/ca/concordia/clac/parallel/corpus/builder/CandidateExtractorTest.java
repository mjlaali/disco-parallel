package ca.concordia.clac.parallel.corpus.builder;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.uima.UIMAException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;
import org.junit.Test;

import ca.concordia.clac.uima.engines.LookupInstanceExtractor;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class CandidateExtractorTest {

	private JCas jCas;
	private JCas enView;
	private JCas frView;
	
	public void buildAParallelText(String enText, String frText) throws UIMAException{
		jCas = JCasFactory.createJCas();
		enView = jCas.createView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		frView = jCas.createView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		enView.setDocumentText(enText);
		frView.setDocumentText(frText);
		addToken(frView);
		addToken(enView);
		
		ParallelChunk enChunk = new ParallelChunk(enView, 0, enText.length());
		ParallelChunk frChunk = new ParallelChunk(frView, 0, frText.length());
		enChunk.setTranslation(frChunk);
		frChunk.setTranslation(enChunk);
		enChunk.addToIndexes();
		frChunk.addToIndexes();
	}
	
	private void addToken(JCas aView) {
		String text = aView.getDocumentText();
		int begin = 0;
		for (String token: text.split(" ")){
			int end = begin + token.length();
			new Token(aView, begin, end).addToIndexes();
			begin = end + 1;
		}
	}

	public void addDCAnnotations(JCas aJCas, List<String> dcs){
		String text = aJCas.getDocumentText();
		for (String dc: dcs){
			int begin = text.indexOf(dc);
			new DiscourseConnective(aJCas, begin, begin + dc.length()).addToIndexes();
		}
	}
	
	@Test
	public void givenTwoDiscourseConnectiveInTheFrenchTextOfAParallelChunkWhenExtractingCandidateThenNoDiscourseConnectiveIsAdddedToTheCandidateList() throws UIMAException{
		buildAParallelText(
				"However , it is also extremely important to me to point something out that many people actually take for granted.",
				"Toutefois , il m'importe tout particulièrement d' aussi constater quelque chose qui va en fait de soit pour beaucoup.");
		List<String> dcs = Arrays.asList("toutefois", "aussi");

		
		LookupInstanceExtractor<DiscourseConnective> lookupInstanceExtractor = new LookupInstanceExtractor<>();
		lookupInstanceExtractor.init(dcs, (jCas, begin, end) -> new DiscourseConnective(jCas, begin, end));
		assertThat(lookupInstanceExtractor.getInstances(frView)).hasSize(2);

		CandidateExtractor candidateExtractor = new CandidateExtractor(lookupInstanceExtractor);
		assertThat(candidateExtractor.getInstances(jCas)).hasSize(0);
	}

	@Test
	public void givenTwoDiscourseConnectiveInTheEnglsihTextOfAParallelChunkWhenExtractingCandidateThenNoDiscourseConnectiveIsAdddedToTheCandidateList() throws UIMAException{
		buildAParallelText(
				"However , it is also extremely important to me to point something out that many people actually take for granted.",
				"il m'importe tout particulièrement d' aussi constater quelque chose qui va en fait de soit pour beaucoup.");
		List<String> frDcs = Arrays.asList("toutefois", "aussi");
		List<String> enDcs = Arrays.asList("However", "also");

		addDCAnnotations(enView, enDcs);
		enDcs = enDcs.stream().map(String::toLowerCase).collect(Collectors.toList());
		
		LookupInstanceExtractor<DiscourseConnective> lookupInstanceExtractor = new LookupInstanceExtractor<>();
		lookupInstanceExtractor.init(frDcs, (jCas, begin, end) -> new DiscourseConnective(jCas, begin, end));
		assertThat(lookupInstanceExtractor.getInstances(frView)).hasSize(1);

		CandidateExtractor candidateExtractor = new CandidateExtractor(lookupInstanceExtractor);
		assertThat(candidateExtractor.getInstances(jCas)).hasSize(0);
	}
	
	@Test
	public void givenOneDiscourseConnectiveInAParallelChunkWhenExtractingCandidateThenOneDiscourseConnectiveIsAdddedToTheCandidateList() throws UIMAException{
		buildAParallelText(
				"it is also extremely important to me to point something out that many people actually take for granted.",
				"il m'importe tout particulièrement d' aussi constater quelque chose qui va en fait de soit pour beaucoup.");
		List<String> dcs = Arrays.asList("toutefois", "aussi");
		
		LookupInstanceExtractor<DiscourseConnective> lookupInstanceExtractor = new LookupInstanceExtractor<>();
		lookupInstanceExtractor.init(dcs, (jCas, begin, end) -> new DiscourseConnective(jCas, begin, end));
		CandidateExtractor candidateExtractor = new CandidateExtractor(lookupInstanceExtractor);
		assertThat(candidateExtractor.getInstances(jCas)).hasSize(1);
	}

}
