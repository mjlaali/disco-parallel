package ca.concordia.clac.parallel.corpus.builder;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.conll2015.DefaultDiscourseRelationExample;
import org.cleartk.corpus.conll2015.DiscourseRelationFactory;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.junit.After;
import org.junit.Test;

import ca.concordia.clac.parallel.corpus.moses.wordalignment.WordAlignmentAnnotator;
import ca.concordia.clac.parallel.corpus.moses.wordalignment.WordAlignmentAnnotatorTest;

public class AnnotationProjectionFilterTest {
	File alignmentFile = new File("outputs/test/AnnotationProjectionFilter/en-fr.A3.final");
	File retainLinesFile = new File("outputs/test/AnnotationProjectionFilter/retained");
	JCas enView, frView;
	String sense;

	public void init(String alignment, String retained) throws IOException{
		if (!alignmentFile.getParentFile().exists()){
			alignmentFile.getParentFile().mkdirs();
		}
		FileUtils.writeStringToFile(alignmentFile, alignment, "UTF-8");
		FileUtils.writeStringToFile(retainLinesFile, retained, "UTF-8");

	}

	@After
	public void cleanup(){
		alignmentFile.delete();
		retainLinesFile.delete();
	}

	public void init(boolean filterOn, String wordAlignments) throws IOException, UIMAException{
		String alignments = "# Sentence pair (60) source length 24 target length 17 alignment score : 1.38666e-30\n" +
				"so Parliament should send a message , since that is the wish of the vast majority .\n" + wordAlignments;
		String retained = "1\n";
		init(alignments, retained);

		JCas aJCas = JCasFactory.createJCas();
		DiscourseRelationFactory discourseRelationFactory = new DiscourseRelationFactory();

		enView = aJCas.createView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		sense = "CAUSE";
		discourseRelationFactory.makeDiscourseRelationFrom(enView, new DefaultDiscourseRelationExample(
				"So Parliament should send a message, since that is the wish of the vast majority.", 
				new String[]{"(ROOT (S (IN So) (NP (NNP Parliament)) (VP (MD should) (VP (VB send) (NP (DT a) (NN message)) (, ,) (SBAR (IN since) (S (NP (DT that)) (VP (VBZ is) (NP (NP (DT the) (NN wish)) (PP (IN of) (NP (DT the) (JJ vast) (NN majority))))))))) (. .)))"}, 
				"So Parliament should send a message,", "that is the wish of the vast majority.", "since", sense
				)).addToIndexesRecursively();
		frView = aJCas.createView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		discourseRelationFactory.makeDiscourseRelationFrom(frView, new DefaultDiscourseRelationExample(
				"Le Parlement devrait dès lors envoyer un message en ce sens, étant donné qu'une grande majorité des députés le souhaite.", 
				new String[]{"(ROOT (SENT (NP (DET Le) (NC Parlement)) (VN (V devrait)) (MWADV (P dès) (ADV lors)) (VPinf (VN (VINF envoyer)) (NP (DET un) (NC message)) (MWADV (P en) (DET ce) (N sens))) (PUNC ,) (VPpart (VN (VPR étant) (VPP donné)) (Ssub (CS qu') (NP (DET une) (ADJ grande) (NC majorité) (PP (P des) (NP (NC députés)))) (VN (CLO le) (V souhaite)))) (PUNC .)))"}, 
				"Le Parlement devrait dès lors envoyer un message en ce sens,", "une grande majorité des députés le souhaite.", "étant donné qu'", null
				)).addToIndexesRecursively();

		WordAlignmentAnnotatorTest.addParallelChunk(enView, frView);

		AnalysisEngineDescription annotator = WordAlignmentAnnotator.getDescription(alignmentFile, retainLinesFile);

		SimplePipeline.runPipeline(aJCas, annotator, AnnotationProjectionFilter.getDescription(filterOn));
	}

	@Test
	public void whenThereIsAnAlignmentToEnDcThenTheSenseAreSet() throws UIMAException, IOException{
		for (boolean filter: new boolean[]{true, false}){
			init(filter, "NULL ({ 10 }) le ({ }) Parlement ({ 2 }) devrait ({ 3 }) dès ({ 1 }) lors ({ }) envoyer ({ 4 }) un ({ 5 }) message ({ 6 }) en ({ }) ce ({ }) sens ({ }) , ({ 7 }) étant ({ 8 }) donné ({ }) qu ({ 9 }) &apos; ({ }) une ({ 14 }) grande ({ 15 }) majorité ({ 16 }) des ({ 13 }) députés ({ }) le ({ 11 }) souhaite ({ 12 }) . ({ 17 })");

			Collection<DiscourseConnective> dcs = JCasUtil.select(frView, DiscourseConnective.class);
			assertThat(dcs).hasSize(1);
			assertThat(dcs.iterator().next().getSense()).isEqualTo(sense);
		}
	}
	
	@Test
	public void whenThereIsNoAlignmentToEnDcThenTheConnectiveIsRemoved() throws UIMAException, IOException{
		init(true, "NULL ({ 10 }) le ({ }) Parlement ({ 2 }) devrait ({ 3 }) dès ({ 1 }) lors ({ }) envoyer ({ 4 }) un ({ 5 }) message ({ 6 }) en ({ }) ce ({ }) sens ({ }) , ({ 7 }) étant ({ }) donné ({ }) qu ({ }) &apos; ({ }) une ({ 14 }) grande ({ 15 }) majorité ({ 16 }) des ({ 13 }) députés ({ }) le ({ 11 }) souhaite ({ 12 }) . ({ 17 })");

		Collection<DiscourseConnective> dcs = JCasUtil.select(frView, DiscourseConnective.class);
		assertThat(dcs).hasSize(0);
	}

	@Test
	public void whenThereIsNoAlignmentToEnDcAndFilterIsOffThenTheConnectiveIsNotRemoved() throws UIMAException, IOException{
		init(false, "NULL ({ 10 }) le ({ }) Parlement ({ 2 }) devrait ({ 3 }) dès ({ 1 }) lors ({ }) envoyer ({ 4 }) un ({ 5 }) message ({ 6 }) en ({ }) ce ({ }) sens ({ }) , ({ 7 }) étant ({ }) donné ({ }) qu ({ }) &apos; ({ }) une ({ 14 }) grande ({ 15 }) majorité ({ 16 }) des ({ 13 }) députés ({ }) le ({ 11 }) souhaite ({ 12 }) . ({ 17 })");

		Collection<DiscourseConnective> dcs = JCasUtil.select(frView, DiscourseConnective.class);
		assertThat(dcs).hasSize(1);
		assertThat(dcs.iterator().next().getSense()).isEqualTo(null);
	}
	
	@Test
	public void whenThereIsAnAlignmentToEnWordThenSenseIsNull() throws UIMAException, IOException{
		for (boolean filter: new boolean[]{true, false}){
			init(filter, "NULL ({ 10 }) le ({ }) Parlement ({ 2 }) devrait ({ 3 }) dès ({ 1 }) lors ({ }) envoyer ({ 4 }) un ({ 5 }) message ({ 6 }) en ({ }) ce ({ }) sens ({ }) , ({ 7 }) étant ({ }) donné ({ }) qu ({ 9 }) &apos; ({ }) une ({ 14 }) grande ({ 15 }) majorité ({ 16 }) des ({ 13 }) députés ({ }) le ({ 11 }) souhaite ({ 12 }) . ({ 17 })");

			Collection<DiscourseConnective> dcs = JCasUtil.select(frView, DiscourseConnective.class);
			assertThat(dcs).hasSize(1);
			assertThat(dcs.iterator().next().getSense()).isNull();
		}
	}

	@Test
	public void whenThereIsAnAlignmentToPunctuationThenRemoveFromAnnotaions() throws UIMAException, IOException{
		init(true, "NULL ({ 10 }) le ({ }) Parlement ({ 2 }) devrait ({ 3 }) dès ({ 1 }) lors ({ }) envoyer ({ 4 }) un ({ 5 }) message ({ 6 }) en ({ }) ce ({ }) sens ({ }) , ({ 7 }) étant ({ }) donné ({ 17 }) qu ({ 7 }) &apos; ({ 7 }) une ({ 14 }) grande ({ 15 }) majorité ({ 16 }) des ({ 13 }) députés ({ }) le ({ 11 }) souhaite ({ 12 }) . ({ 17 })");

		Collection<DiscourseConnective> dcs = JCasUtil.select(frView, DiscourseConnective.class);
		assertThat(dcs).hasSize(0);
	}
}
