package ca.concordia.clac.parallel.corpus.builder;


import java.util.List;

import org.cleartk.corpus.conll2015.DiscourseRelationExample;

public class ForExampleExample implements DiscourseRelationExample{
	protected String[] parseTree = new String[]{
			"(ROOT (S (NP (DT This) (NN time)) (, ,) (NP (DT the) (NNS firms)) (VP (VBD were) (ADJP (JJ ready))) (. .)))",
			"(ROOT (S (NP (NP (NNP Fidelity)) (, ,) (PP (IN for) (NP (NN example))) (, ,)) (VP (VBD prepared) (NP (NNS ads)) (PP (ADVP (NP (JJ several) (NNS months)) (RB ago)) (IN in) (NP (NP (NN case)) (PP (IN of) (NP (DT a) (NN market) (NN plunge)))))) (. .)))"
	};
	protected String arg1 = "This time, the firms were ready.";
	protected String[] arg2 = new String[]{"Fidelity,", ", prepared ads several months ago in case of a market plunge."};
	protected String dc = "for example";
	protected String sense = "instantition";
	private String text = "This time, the firms were ready.\nFidelity, for example, prepared ads several months ago in case of a market plunge.\n\n";
	
	@Override
	public String getText() {
		return text;
	}

	@Override
	public String getArg1() {
		return arg1;
	}

	@Override
	public String[] getArg2() {
		return arg2;
	}

	@Override
	public String getDiscourseConnective() {
		return dc;
	}

	@Override
	public String[] getParseTree() {
		return parseTree;
	}

	@Override
	public String getSense() {
		return sense;
	}

	@Override
	public List<List<List<String>>> getDependencies() {
		return null;
	}
	
}
