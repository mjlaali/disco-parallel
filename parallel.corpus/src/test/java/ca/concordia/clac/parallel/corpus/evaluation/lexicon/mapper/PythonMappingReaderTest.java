package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import com.google.common.collect.SetMultimap;

public class PythonMappingReaderTest {

	@Test
	public void whenLoadingAtLeastThereIsOneMapping() throws IOException{
		File directory = new File("resources/test/mapping/c0");
		PythonMappingReader loader = new PythonMappingReader(directory);
		
		assertThat(loader.getMapping().size()).isGreaterThan(0);
	}
	
	@Test
	public void whenLoadingThenTheAverageMapping() throws IOException{
		File directory = new File("resources/test/mapping/c0");
		PythonMappingReader loader = new PythonMappingReader(directory);
		
		SetMultimap<String, Pair<String, Double>> mapping = loader.getMapping();
		assertThat(mapping.size()).isGreaterThan(0);
	}
	@Test
	public void whenLoading() throws IOException{
		File directory = new File("resources/test/mapping/c0");
		PythonMappingReader loader = new PythonMappingReader(directory);
		
		MapperToString toString = new MapperToString(0.99, 5, loader.getMapping());
		System.out.println(toString);
	}
}
