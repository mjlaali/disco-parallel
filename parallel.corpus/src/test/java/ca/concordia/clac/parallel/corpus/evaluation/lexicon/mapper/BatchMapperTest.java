package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.math3.util.Pair;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;


public class BatchMapperTest {

	private BatchMapper mapper;
	
	@Before
	public void setup() throws IOException{
		String buf = 
				 "a1,b1,0.1\n" 
				+"a1,b2,0.2\n" 
				+"a2,b2,0.2\n" 
				+"a2,b3,0.2\n" ;
		InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(buf.getBytes()));
		
		mapper = new BatchMapper(reader);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenMappingExistThenReturnAllMappedRelaions(){
		Collection<Pair<String, Double>> mapping = mapper.getMapping("dc", "a1");
		assertThat(mapping).containsOnly(new Pair<>("b1", 0.1), new Pair<>("b2", 0.2));
	}

	@Test
	public void whenMappingDoesNotExistThenReturnAllMappedRelaions(){
		Collection<Pair<String, Double>> mapping = mapper.getMapping("dc", "a3");
		assertThat(mapping).isEmpty();
	}

	@Test
	public void whenMapThenTheProbIsCorrect(){
		Map<Pair<String, String>, Double> mappedLexicon = mapper.mapLexicon(ImmutableMap.of(new Pair<>("dc", "a1"), 0.3));
		assertThat(mappedLexicon).containsOnly(entry(new Pair<>("dc", "b1"), 0.03), entry(new Pair<>("dc", "b2"), 0.06));
	}

	@Test
	public void whenRelationDoesNotExistInTheMapThenResultIsEmpty(){
		Map<Pair<String, String>, Double> mappedLexicon = mapper.mapLexicon(ImmutableMap.of(new Pair<>("dc", "a3"), 0.3));
		assertThat(mappedLexicon).isEmpty();
	}

}
