package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;

public class SenseRemovalTest {

	@Test
	public void whenNormalizingAPDTBRelations(){
		SenseRemoval.RelationNormalzier normalzier = new SenseRemoval.RelationNormalzier();
		assertThat(normalzier.apply("Contingency_Cause_Result")).isEqualTo("Contingency.Cause.Result");
	}
	
	@Test
	public void whenNormalizingALexconRelations(){
		SenseRemoval.RelationNormalzier normalzier = new SenseRemoval.RelationNormalzier();
		assertThat(normalzier.apply("condition")).isEqualTo("Condition");
	}

}
