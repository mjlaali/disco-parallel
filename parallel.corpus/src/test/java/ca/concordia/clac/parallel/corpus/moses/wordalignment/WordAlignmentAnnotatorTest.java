package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math3.util.Pair;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.conll2015.SyntaxReader;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.junit.Test;

import com.google.common.collect.ImmutableMap;

import ca.concordia.clac.parallel.corpus.moses.wordalignment.WordAlignmentAnnotator;
import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class WordAlignmentAnnotatorTest {

	@Test
	public void whenAllWordsAreAligned() throws UIMAException{
		JCas aJCas = JCasFactory.createJCas();
		SyntaxReader reader  = new SyntaxReader();
		reader.initJCas(aJCas, "(ROOT (S (NP (PRP it)) (VP (VBZ 's) (NP (DT a) (NN test))) (. .)))");
		
		List<Token> tokens = new ArrayList<>(JCasUtil.select(aJCas, Token.class));
		List<String> words = Arrays.asList(aJCas.getDocumentText().split(" "));
		
		Map<Integer, Integer> map = WordAlignmentAnnotator.map(words, tokens);
		
		for (Entry<Integer, Integer> anEntry: map.entrySet()){
			assertThat(anEntry.getKey()).isEqualTo(anEntry.getValue());
		}
	}
	
	@Test
	public void whenAWordIsSplittedToTwoWords() throws UIMAException{
		JCas aJCas = JCasFactory.createJCas();
		SyntaxReader reader  = new SyntaxReader();
		reader.initJCas(aJCas, "(ROOT (S (NP (PRP it)) (VP (VBZ 's) (NP (DT a) (NN test))) (. .)))");
		
		List<Token> tokens = new ArrayList<>(JCasUtil.select(aJCas, Token.class));
		List<String> words = Arrays.asList("it ' s a test .".split(" "));
		
		Map<Integer, Integer> map = WordAlignmentAnnotator.map(words, tokens);
		
		assertThat(map).containsEntry(1, 1).containsEntry(2, 1);
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void whenMappingAlignments(){
		List<Pair<Integer, Integer>> alignments = Arrays.asList(
				new Pair<>(1, 2),
				new Pair<>(1, 3),
				new Pair<>(2, 2),
				new Pair<>(2, 3));
		Map<Integer, Integer> mosesToUimaForEn = ImmutableMap.of(1, 0, 2, 1);
		Map<Integer, Integer> mosesToUimaForFr = ImmutableMap.of(2, 1, 3, 2);
		List<Pair<Integer, Integer>> mappedAlignment = WordAlignmentAnnotator.map(alignments, mosesToUimaForEn, mosesToUimaForFr);
		
		assertThat(mappedAlignment).containsOnly(
				new Pair<>(0, 1),
				new Pair<>(0, 2),
				new Pair<>(1, 1),
				new Pair<>(1, 2));
	}
	
	@Test
	public void whenAddingAlignmentToAJCas() throws UIMAException{
		File alignmentFile = new File("resources/test/moses-alignments/en-fr.A3.sample");
		File retainLinesFile = new File("resources/test/moses-alignments/retained");
		
		JCas aJCas = JCasFactory.createJCas();
		JCas enView = tokenize(aJCas.createView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW), "resumption of the session");
		JCas frView = tokenize(aJCas.createView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW), "reprise de la session");
		
		addParallelChunk(enView, frView);
		
		AnalysisEngineDescription annotator = WordAlignmentAnnotator.getDescription(alignmentFile, retainLinesFile);
		
		SimplePipeline.runPipeline(aJCas, annotator);
		
		Collection<Alignment> enAlignments = JCasUtil.select(enView, Alignment.class);
		Collection<Alignment> frAlignments = JCasUtil.select(frView, Alignment.class);
		assertThat(enAlignments).hasSize(4);
		assertThat(frAlignments).hasSize(4);
		
		int idx = 0;
		List<Token> enTokens = new ArrayList<>(JCasUtil.select(enView, Token.class));
		List<Token> frTokens = new ArrayList<>(JCasUtil.select(frView, Token.class));
		for (Alignment alignment: enAlignments){
			Token enToken = (Token) alignment.getWrappedAnnotation();
			Token frToken = (Token) alignment.getAlignment().getWrappedAnnotation();
			
			assertThat(enToken).isEqualTo(enTokens.get(idx));
			assertThat(frToken).isEqualTo(frTokens.get(idx));
			idx++;
		}
		
		Map<Alignment, Collection<Token>> indexCovered = JCasUtil.indexCovering(enView, Alignment.class, Token.class);
		for (Entry<Alignment, Collection<Token>> anEntry: indexCovered.entrySet()){
			assertThat(anEntry.getValue()).hasSize(1);
		}
	}

	public static void addParallelChunk(JCas enView, JCas frView) {
		ParallelChunk enChunk = new ParallelChunk(enView, 0, enView.getDocumentText().length());
		ParallelChunk frChunk = new ParallelChunk(frView, 0, frView.getDocumentText().length());
		enChunk.setTranslation(frChunk);
		frChunk.setTranslation(enChunk);
		
		enChunk.addToIndexes();frChunk.addToIndexes();
	}

	public static JCas tokenize(JCas aView, String text) {
		aView.setDocumentText(text);
		String[] words = text.split(" ");
		int begin = 0;
		for (String word: words){
			new Token(aView, begin, begin + word.length()).addToIndexes();
			begin += word.length() + 1;
		}
		return aView;
	}
	
}
