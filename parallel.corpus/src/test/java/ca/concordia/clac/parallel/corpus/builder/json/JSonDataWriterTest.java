package ca.concordia.clac.parallel.corpus.builder.json;

import static ca.concordia.clac.ml.feature.FeatureExtractors.dummyFunc;
import static ca.concordia.clac.ml.feature.FeatureExtractors.getText;
import static ca.concordia.clac.ml.feature.FeatureExtractors.makeFeature;
import static ca.concordia.clac.ml.feature.FeatureExtractors.multiMap;
import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.ml.CleartkProcessingException;
import org.cleartk.ml.Feature;
import org.cleartk.ml.Instance;
import org.junit.Before;
import org.junit.Test;

import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.ml.classifier.ClassifierAlgorithmFactory;
import ca.concordia.clac.ml.classifier.GenericClassifierLabeller;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class JSonDataWriterTest {
	File directory = new File("outputs/tests", getClass().getSimpleName());
	JSonDataWriter<String> jSonDataWriter;
	
	@Before
	public void setup() throws IOException{
		if (directory.exists())
			FileUtils.deleteDirectory(directory);
		directory.mkdirs();
		jSonDataWriter = new JSonDataWriter<>(directory);
	}

	@Test
	public void whenCreateAnEmptyInstance() throws IOException, CleartkProcessingException{
		
		List<Feature> features = Arrays.asList(new Feature[]{
		});
		jSonDataWriter.write(new Instance<String>(features));
		
		jSonDataWriter.finish();
		
		String outputJson = loadJSon();
		assertThatJson(outputJson).isEqualTo("[{\"features\":[]}]");
	}
	
	@Test
	public void whenCreateOneInstance() throws IOException, CleartkProcessingException{
		List<Feature> features = Arrays.asList(new Feature[]{
				new Feature("f1", 1),
				new Feature("f2", "1"),
				new Feature("f3", true),
		});
		jSonDataWriter.write(new Instance<String>("Test", features));
		
		jSonDataWriter.finish();
		
		String outputJson = loadJSon();
		assertThatJson(outputJson).isEqualTo("[{\"features\":[{\"name\":\"f1\",\"value\":1},{\"name\":\"f2\",\"value\":\"1\"},{\"name\":\"f3\",\"value\":true}],\"outcome\":\"Test\"}]");
	}
	
	@Test
	public void whenCreateTwoInstances() throws IOException, CleartkProcessingException{
		List<Feature> features = Arrays.asList(new Feature[]{
				new Feature("f1", 1),
				new Feature("f2", "1"),
				new Feature("f3", true),
		});
		jSonDataWriter.write(new Instance<String>("Test", features));
		jSonDataWriter.write(new Instance<String>("Test", features));
		
		jSonDataWriter.finish();
		
		String expectedJson = "[{\"features\":[{\"name\":\"f1\",\"value\":1},{\"name\":\"f2\",\"value\":\"1\"},{\"name\":\"f3\",\"value\":true}],\"outcome\":\"Test\"},"
				+ "{\"features\":[{\"name\":\"f1\",\"value\":1},{\"name\":\"f2\",\"value\":\"1\"},{\"name\":\"f3\",\"value\":true}],\"outcome\":\"Test\"}]";
		String outputJson = loadJSon();
		assertThatJson(outputJson).isEqualTo(expectedJson);
	}

	private String loadJSon() throws IOException {
		String outputJson = FileUtils.readFileToString(new File(directory, JSonDataWriter.JSON_FILE_NAME), StandardCharsets.UTF_8);
		return outputJson;
	}
	
	public static class SampleClassifierAlgorithmFactory implements ClassifierAlgorithmFactory<Integer, Token>{

		@Override
		public void initialize(UimaContext context) throws ResourceInitializationException {
			
		}

		@Override
		public InstanceExtractor<Token> getExtractor(JCas jCas) {
			return (aJCas) -> JCasUtil.select(aJCas, Token.class);
		}

		@Override
		public List<Function<Token, List<Feature>>> getFeatureExtractor(JCas jCas) {
			Function<Token, List<Feature>> features = dummyFunc(Token.class).andThen(multiMap(
					getText().andThen(makeFeature("text")),
					getText().andThen((str) -> new Integer(str.length())).andThen(makeFeature("len"))
					)); 
			return Arrays.asList(features);
		}

		@Override
		public Function<Token, Integer> getLabelExtractor(JCas jCas) {
			return (ann) -> ann.getBegin();
		}

		@Override
		public BiConsumer<Integer, Token> getLabeller(JCas jCas) {
			return null;
		}
		
	}
	
	@Test
	public void whenIntegratedWithClassifierAndEmptyInstances() throws UIMAException, IOException{
		JCas jCas = JCasFactory.createJCas();
		jCas.setDocumentText("it is a test");
		AnalysisEngineDescription jsonExporter = GenericClassifierLabeller.getWriterDescription(SampleClassifierAlgorithmFactory.class, JSonDataWriter.class, directory);
		SimplePipeline.runPipeline(jCas, jsonExporter);
		
		String expectedJson = "[]";
		String outputJson = loadJSon();
		assertThatJson(outputJson).isEqualTo(expectedJson);
	}
	
	@Test
	public void whenIntegratedWithClassifierAndTwoInstances() throws UIMAException, IOException{
		JCas jCas = JCasFactory.createJCas();
		jCas.setDocumentText("it is a test");
		new Token(jCas, 0, 2).addToIndexes();
		new Token(jCas, 3, 5).addToIndexes();
		AnalysisEngineDescription jsonExporter = GenericClassifierLabeller.getWriterDescription(SampleClassifierAlgorithmFactory.class, JSonDataWriter.class, directory);
		SimplePipeline.runPipeline(jCas, jsonExporter);
		
		String expectedJson = "[{\"features\":[{\"name\":\"text\",\"value\":\"is\"},{\"name\":\"len\",\"value\":2}],\"outcome\":3},"
				+ "{\"features\":[{\"name\":\"text\",\"value\":\"it\"},{\"name\":\"len\",\"value\":2}],\"outcome\":0}]";
		String outputJson = loadJSon();
		System.out.println(outputJson);
		assertThatJson(outputJson).isEqualTo(expectedJson);
	}

}

