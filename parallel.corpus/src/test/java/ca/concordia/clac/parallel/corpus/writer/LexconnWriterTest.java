package ca.concordia.clac.parallel.corpus.writer;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.common.collect.ImmutableMap;

import ca.concordia.clac.lexconn.DefaultLexconnReader;

public class LexconnWriterTest {

	@Test
	public void whenSaveAndLoadALexiconThenTheyAreEqual() throws JAXBException{
		List<LexconnEntry> lexicon = Arrays.asList(new LexconnEntry[]{
				new LexconnEntry("0", Arrays.asList("a", "b"), 1, ImmutableMap.of("R1", 1.0)),
				new LexconnEntry("1", Arrays.asList("c", "d"), 2, ImmutableMap.of("R2", 1.0)),
				new LexconnEntry("2", "R1, R2", Arrays.asList("e", "f")),
				new LexconnEntry("3", null, Arrays.asList("g"))
				});
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		LexconnWriter.saveAsXml(lexicon, output);
		Lexconn loaded = LexconnWriter.loadFromAnXML(new InputStreamReader(new ByteArrayInputStream(output.toByteArray())));
		assertThat(loaded).isEqualTo(new Lexconn(lexicon));
	}
	
	@Test
	public void whenReadingDefaultLexconnFile() throws JAXBException, ParserConfigurationException, SAXException, IOException{
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		LexconnWriter.saveAsXml(DefaultLexconnReader.getLexconnMap(), output);
		Lexconn loaded = LexconnWriter.loadFromAnXML(new InputStreamReader(new ByteArrayInputStream(output.toByteArray())));
		assertThat(loaded.getEntries()).hasSize(371);
	}
}
