package ca.concordia.clac.parallel.corpus.builder;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.jcas.JCas;
import org.cleartk.discourse.type.DiscourseConnective;
import org.junit.Test;

import ca.concordia.clac.uima.engines.LookupInstanceExtractor;
import ca.concordia.clac.uima.test.util.DocumentFactory;

public class GetNumberFrenchDiscouresConnective {
	
		
	public List<String> load(File file) throws IOException {
		List<String> terms = FileUtils.readLines(file, "utf8");
		Collections.sort(terms, new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o2.length() - o1.length();
			}
		});
		String smallestDc = terms.get(terms.size() - 1);
		int smallestDcLength = smallestDc.length();
		if (smallestDcLength == 0 || smallestDcLength > 3){
			throw new RuntimeException("Are you sure the content of the discourse connective list is correct: " + smallestDc);
		}
		return terms;
	}
	@Test
	public void getNumberOfFrechDiscourseConnectives() throws UIMAException, IOException{
		DocumentFactory factory = new DocumentFactory();
		
		LookupInstanceExtractor<DiscourseConnective> extractDcs = new LookupInstanceExtractor<>();
		File frDCListFile = new File("resources/french-dc.txt");
		List<String> terms = load(frDCListFile);
		extractDcs.init(terms, (jcas, begin, end) -> new DiscourseConnective(jcas, begin, end));
		String parseTree = "( (ROOT (SENT (VN (CL Je) (V pense)) (Ssub (C qu) (NP (N `)) (VN (CL il) (V est)) (AP (ADV aussi) (A nécessaire)) (VPinf (P de) (VN (V relever)) (NP (D le) (N règlement) (AP (A minimal))))) (. .))) )";
		JCas aJCas = factory.createADcoument(parseTree);

		Collection<DiscourseConnective> instances = extractDcs.getInstances(aJCas);
		instances.stream().forEach((dc) -> System.out.println(dc.getCoveredText()));
		
	}
}
