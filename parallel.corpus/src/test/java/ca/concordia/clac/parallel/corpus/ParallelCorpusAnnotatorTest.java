package ca.concordia.clac.parallel.corpus;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.junit.BeforeClass;
import org.junit.Test;
import org.xml.sax.SAXException;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.api.syntax.type.constituent.Constituent;

public class ParallelCorpusAnnotatorTest {
	private static final String sample = "small-sample";
	private static TestUtils testUtils;
	private static final String FILE_UNDER_TEST = "a.txt.xmi";

	@BeforeClass
	public static void initialize() throws UIMAException, IOException, URISyntaxException, SAXException, CpeDescriptorException{
		testUtils = new TestUtils(sample);
	}

	@Test
	public void thenRawContainEnAndFrTextView() throws IOException, UIMAException{
		JCas aJCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_LOADING, FILE_UNDER_TEST);
		JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		JCas frView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		assertThat(enView).isNotNull();
		assertThat(frView).isNotNull();
	}


	@Test
	public void thenTokensAndTreebanksExist() throws UIMAException, IOException{
		JCas aJCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_PARSING_EN, FILE_UNDER_TEST);
		JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		System.out.println(enView.getDocumentText());
		assertThat(JCasUtil.select(enView, Sentence.class)).hasSize(1);
		assertThat(JCasUtil.select(enView, Token.class)).hasSize(13);
		assertThat(JCasUtil.select(enView, Constituent.class)).hasSize(8);
	}

	@Test
	public void thenOneEnglishDiscourseConnectiveExists() throws UIMAException, IOException{
		JCas aJCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_DISCOURSEPARSING_EN, FILE_UNDER_TEST);;
		JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		assertThat(JCasUtil.select(enView, DiscourseConnective.class)).hasSize(1);
		DiscourseConnective discourseConnective = JCasUtil.selectByIndex(enView, DiscourseConnective.class, 0);
		assertThat(discourseConnective.getSense()).isEqualTo("Comparison.Contrast");
	}
	

	@Test
	public void thenFrenchTextsAreParsed() throws UIMAException, IOException{
		JCas aJCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_PARSING_FR, FILE_UNDER_TEST);;
		JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		assertThat(JCasUtil.select(enView, Constituent.class)).isNotEmpty();
	}
	
	@Test
	public void thenOneFrenchDiscourseConnectiveExists() throws UIMAException, IOException{
		JCas aJCas = testUtils.getJCas(ParallelCorpusAnnotator.STEP_DISCOURSEPARSING_FR, FILE_UNDER_TEST);;
		JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		assertThat(JCasUtil.select(enView, DiscourseConnective.class)).hasSize(1);
		DiscourseConnective discourseConnective = JCasUtil.selectByIndex(enView, DiscourseConnective.class, 0);
		assertThat(discourseConnective.getCoveredText()).isEqualTo("Toutefois");
		
	}
	

}
