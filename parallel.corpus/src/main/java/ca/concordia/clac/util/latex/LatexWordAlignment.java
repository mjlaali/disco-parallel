package ca.concordia.clac.util.latex;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

import ca.concordia.clac.parallel.corpus.moses.wordalignment.AlignmentReader;
import ca.concordia.clac.parallel.corpus.moses.wordalignment.AlignmentReaderFactory;

public class LatexWordAlignment {
	AlignmentReader alignmentReader;
	
	public LatexWordAlignment(AlignmentReader alignmentReader){
		this.alignmentReader = alignmentReader;
	}
	
	public String search(List<String> enQueries, List<String> frQueries){
		StringBuilder sb = new StringBuilder();
		
		Map<String, Integer> enQueriesWithoutSpace = new HashMap<>();
		for(int i = 0; i < enQueries.size(); i++)
			enQueriesWithoutSpace.put(enQueries.get(i).replace(" ", "").toLowerCase(), i);
		
		while (alignmentReader.hasNext() && !enQueriesWithoutSpace.isEmpty()){
			alignmentReader.next();
			String enTextNoSpace = alignmentReader.getEnWords().stream().collect(Collectors.joining("")).toLowerCase();
			Integer removedIdx = enQueriesWithoutSpace.remove(enTextNoSpace);
			if (removedIdx != null){
				sb.append(alignmentReader.getLineNumber());
				sb.append("\n");
				sb.append(enQueries.get(removedIdx));
				sb.append("\n");
				if (frQueries != null){
					sb.append(frQueries.get(removedIdx));
					sb.append("\n");
				}
				sb.append(createLatexScript(alignmentReader.getEnWords(), alignmentReader.getFrWords(), alignmentReader.getAlignments()));
			}
		}
		return sb.toString();
	}
	
	public String createLatexScript(String english, String french, String line){
		List<Pair<Integer, Integer>> wordAlignments = new ArrayList<>();
		String[] alignments = line.split(" ");
		for (String alignment: alignments){
			List<Integer> parsedAlignments = Arrays.stream(alignment.split("-")).map(Integer::parseInt).collect(Collectors.toList());
			wordAlignments.add(new Pair<>(parsedAlignments.get(0) - 1, parsedAlignments.get(1) - 1));
		}
		
		List<String> enWords = Arrays.asList(english.split(" "));
		List<String> frWords = Arrays.asList(french.split(" "));
		
		return createLatexScript(enWords, frWords, wordAlignments);
	}
	
	public String createLatexScript(List<String> enWords, List<String> frWords, List<Pair<Integer, Integer>> alignments){
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(buffer);
		
		ps.println("\\begin{tikzpicture}[rounded corners=2pt,inner sep=2pt,text height=1em, node distance=1pt]");
		
		printWords(enWords, "", "en", ps);
		ps.println();
		printWords(frWords, ",below=1.0cm of en0", "fr", ps);
		ps.println();

		for (Pair<Integer, Integer> alignment: alignments){
			ps.printf("\\draw [<->] (en%d) .. controls +(down:1) and +(up:1) .. (fr%d);", alignment.getFirst(), alignment.getSecond());
			ps.println();
		}
		
		ps.println("\\end{tikzpicture}");
		
		ps.close();
		return new String(buffer.toByteArray());
	}

	private void printWords(List<String> words, String firstWordPos, String tag, PrintStream ps) {
		for (int i = 0; i < words.size(); i++){
			String pos = firstWordPos;
			if (i > 0){
				pos = String.format(",right=of %s%d", tag, i - 1);
			}
			ps.printf("\\node [draw%s](%s%d) {\\textit{%s}};", pos, tag, i, words.get(i));
			ps.println();
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		String[] englishTexts = new String[]{
//			"Thank you, Mr Segni, I shall do so gladly.",
//			"The White Paper intends to resolve these problems and we therefore support these proposals."
//			"I would ask that they reconsider, since this is not the case.",
//			"The Member States mustalsobearin mind their responsibility.",
			"When I speak of optimum utilisation, I am referring both to the national and regional levels.",
//			"It is not a lot to ask."
//			"Like everybody in this House, I want freedom, justice and security. I do not want to see these degenerate into over-centralisation, chaos and confusion.", //mais is dropped
//			"If we are anything less than ambitious in this field, we shall simply not provide our industry, our research and development experts with the modern patent which they need.",
//			"In the past, this Parliament has viewed the social economy as an important potential provider of employment.",	//true-negative as <-> comme
//			"If the House agrees, I shall do as Mr Evans has suggested.", //true-positive as <-> comme
//			"Now, however, he is to go before the courts once more because the public prosecutor is appealing.",	//because become implicit
//			"If your ruling is that I cannot give an explanation of vote, I accept that but with reservations.",  	//recall
//			"That didnot happen.",	//That is not supposed to align Mais
//			"The conclusion is that we must make the case for guidelines to be broad, indicative and flexible to assist our programme managers and fund-users and to get the maximum potential out of our new fields of regeneration."
				
		};
//		String[] frenchTexts = new String[]{
//			"Puis , nous ferons comme d' habitude : nous entendrons un orateur pour et un orateur contre."	
//		};
		File alignmentFile = new File("/Users/majid/Documents/resource/europarl_en_fr_tokenized/aligned.1.grow-diag-final-and.fr-en");
//		File alignmentFile = new File("/Users/majid/Documents/resource/europarl_en_fr_tokenized/en-fr.A3.final");
//		File alignmentFile = new File("/Users/majid/Documents/resource/europarl_en_fr_tokenized/fr-en.A3.final");
		
		AlignmentReader alignmentReader = AlignmentReaderFactory.get(alignmentFile);
		LatexWordAlignment latexWordAlignment = new LatexWordAlignment(alignmentReader);
		String script = latexWordAlignment.search(Arrays.asList(englishTexts), null);//Arrays.asList(frenchTexts));
		System.out.println(script);
	}
}
