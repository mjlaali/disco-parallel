package ca.concordia.clac.util.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

public class SelectLineFromFile {

	public void select(String fromFile, String indexesFile) throws IOException{
		List<Integer> indexes = FileUtils.readLines(new File(indexesFile), StandardCharsets.UTF_8).stream().map(Integer::parseInt).collect(Collectors.toList());
		BufferedReader br = new BufferedReader(new FileReader(new File(fromFile)));
		String line;
		int idx = 0;
		while ((line = br.readLine()) != null && indexes.size() > 0){
			++idx;
			if (idx == indexes.get(0)){
				System.out.println(line);
				indexes.remove(0);
			}
		}
		br.close();
	}

	public interface Options{
		@Option(
				defaultToNull=true,
				shortName = "i",
				longName = "indexes", 
				description = "The indexes file.")
		public String getIndexes();

		@Option(
				shortName = "f",
				longName = "file",
				description = "The target file")
		public String getFile();
	}

	
	public static void main(String[] args) throws IOException {
		Options options = CliFactory.parseArguments(Options.class, args);
		String fromFile = options.getFile();
		String indexesFile = options.getIndexes();
//		String fromFile = "/Users/majid/Dropbox/fr.txt";
//		String indexesFile = "/Users/majid/Dropbox/lines.txt";
		
		new SelectLineFromFile().select(fromFile, indexesFile);
	}
}
