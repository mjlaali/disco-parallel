package ca.concordia.clac.parallel.corpus.moses;

import org.apache.commons.lang.StringEscapeUtils;

public class MosesUtils {

	public static String escapeMosesString(String str){
		return StringEscapeUtils.unescapeXml(str).replace("@-@", "-");
	}
}
