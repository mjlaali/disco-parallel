package ca.concordia.clac.parallel.corpus.evaluation.lexicon;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.math3.util.Pair;
import org.xml.sax.SAXException;

import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.filter.Filter;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.filter.Filter.FilterInFrequendDC;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper.BatchMapper;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper.Mapper;
import ir.laali.tools.EvaluationOfRankList;

public class Evaluation {
	String[] phraseTableNames;
	String[] dcFreqFileNames;
	int minFreq;
	public Evaluation(String[] phraseTableNames, String[] dcFreqFileNames, int minFreq) {
		this.phraseTableNames = phraseTableNames;
		this.dcFreqFileNames = dcFreqFileNames;
		this.minFreq = minFreq;
	}

	
	public void evaluate(Mapper mapper, String mappingName) throws ParserConfigurationException, SAXException, IOException, JAXBException{
		int total = 0;
		for (int i = 0;i < phraseTableNames.length; i++){
			System.out.println(phraseTableNames[i] + ", " + dcFreqFileNames[i]);
			String phraseTableName = phraseTableNames[i];
			File frDcFreq = new File("resources/freq/" + dcFreqFileNames[i]);
			
			Loader loader = new Loader(DefaultLexconnReader.getLexconnMap());

			
			Map<Pair<String, String>, Double> extractedLexicon = loader.loadFromPhraseTable(phraseTableName);
			Map<Pair<String, String>, Double> goldLexicon = loader.loadLexconn();
			
			FilterInFrequendDC filterInFrequendDC = new FilterInFrequendDC(loader.loadCounts(frDcFreq), 50);
			DefaultLexconnReader lexconnReader = loader.getLexconnReader();

			System.out.println("The size of lexicons before filtering: " + extractedLexicon.size() + ", " + goldLexicon.size());
			extractedLexicon = Filter.filterBaseDc(extractedLexicon, filterInFrequendDC);
			extractedLexicon = Filter.filterBaseRelation(extractedLexicon, new Filter.FilterNotSeenRelations(mapper.getPDTBRelations()));
			
			goldLexicon = Filter.filterBaseDc(goldLexicon, filterInFrequendDC);
			goldLexicon = Filter.filterBaseRelation(goldLexicon, new Filter.FilterNotSeenRelations(mapper.getLexconnRelations()));
			
			System.out.println("The size of lexicons after filtering: " + extractedLexicon.size() + ", " + goldLexicon.size());
			System.out.println("The selected discourse connectives from LEXCONN: ");
			goldLexicon.forEach((id, rel) -> {
				System.out.println(lexconnReader.getIdToForms().get(id.getFirst()) + " & " + id.getSecond());
			});
			
			extractedLexicon = mapper.mapLexicon(extractedLexicon);
			LexiconEvaluator lexiconEvaluation = new LexiconEvaluator(goldLexicon, extractedLexicon);

			total = Math.max(total, goldLexicon.size());
			EvaluationOfRankList evaluationOfRankList = new EvaluationOfRankList(lexiconEvaluation.getJudges(), total);
			System.out.println(evaluationOfRankList);

			int founded = lexiconEvaluation.getIntersection().size();
			System.out.printf("intersection = %d, notfound = %d, suggested = %d\n", founded, lexiconEvaluation.getNotFound().size(), lexiconEvaluation.getSuggested().size());
			System.out.println("Recall = " + evaluationOfRankList.getRecallPoints()[founded  - 1] + 
					", Precision = " + evaluationOfRankList.getPrecisions()[founded - 1]);
			File dir = new File("outputs/lexicon/", phraseTableName.substring(0, phraseTableName.lastIndexOf('.')));
			dir.mkdirs(); 
			
			Printer printer = new Printer(lexconnReader);
			printer.printALexicon(lexiconEvaluation.getIntersection(), loader.loadCounts(frDcFreq), new File(dir, "intersection-" + mappingName + ".txt"));
			printer.saveALexicon(lexiconEvaluation.getIntersection(), new File(dir, "intersection-" + mappingName + ".json"), phraseTableName, mapper);
			printer.printALexicon(lexiconEvaluation.getNotFound(), loader.loadCounts(frDcFreq), new File(dir, "notFound-" + mappingName + ".txt"));
			printer.printALexicon(lexiconEvaluation.getSuggested(), loader.loadCounts(frDcFreq), new File(dir, "suggested-" + mappingName + ".txt"));
			printer.saveALexicon(lexiconEvaluation.getSuggested(), 	new File(dir, "suggested-" + mappingName + ".json"), phraseTableName, mapper);
		}
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, JAXBException {
		String[] phraseTableNames = {
				"en-discourse-usage.en", 
//				"enfr-discourse-usage.enfr"
				};
		String[] dcFreqFileNames = {
				"fr_dc_lcase.freq.txt", 
//				"fr_dc_lcase_sense.freq.txt"
				};

		int minFreq = 50;
		File mappingFile = new File("resources/simple-mapping.csv");
//		File mappingDirectory = new File("outputs/leave-one-out");
//		File rozeMappingFile = new File("resources/expert-mapping.csv");

		Mapper[] mappers = {
//				new LeaveOneOutMapper(mappingDirectory), 
				new BatchMapper(mappingFile)
				};
		String[] mappingNames = {
//				mappingDirectory.getName(), 
				mappingFile.getName().replace(".csv", "")
				};
		
		for (int i = 0; i < mappers.length; i++){
//			MapperToString strMapping = new MapperToString(0.99, 3, mappers[i].getMapping());
//			System.out.println("Manual mapping size = " + strMapping.loadAMapping(rozeMappingFile));
//			System.out.println(strMapping);
			new Evaluation(phraseTableNames, dcFreqFileNames, minFreq).evaluate(mappers[i], mappingNames[i]);
		}
	}
	
}
