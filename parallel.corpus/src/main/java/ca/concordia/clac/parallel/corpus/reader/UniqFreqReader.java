package ca.concordia.clac.parallel.corpus.reader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.builder.LexconnBuilder;

/**
 * This class used to extract the distribution of discourse connectives in Europarl base on 
 * the output of uniq -c command and LEXCONN file.   
 * @author majid
 *
 */
public class UniqFreqReader {
	Map<String, Integer> termCount = new TreeMap<>();
	List<String> termInOrder = new ArrayList<>();
	Map<String, String> mapToCanonicalForm;
	Function<String, String> normalizer;

	public UniqFreqReader(File aFile) throws IOException {
		Scanner scanner = new Scanner(new FileInputStream(aFile), "UTF-8");

		while (scanner.hasNext()){
			int freq = scanner.nextInt();
			String aText = scanner.nextLine().trim();

			termCount.put(aText, freq);
			termInOrder.add(aText);
		}

		scanner.close();
	}

	public static void mapToNorm(File input, File output, Function<String, String> norm) throws FileNotFoundException, UnsupportedEncodingException{
		Scanner scanner = new Scanner(new FileInputStream(input), "UTF-8");
		PrintStream ps = new PrintStream(new FileOutputStream(output), true, "UTF-8"); 

		Set<String> seen = new HashSet<>();
		while (scanner.hasNext()){
			int freq = scanner.nextInt();
			String aText = scanner.nextLine().trim();
			
			String normalized = norm.apply(aText);
			if (!seen.contains(normalized)){
				ps.println(freq + "\t" + normalized);
				seen.add(normalized);
			}
		}

		scanner.close();
		ps.close();
	}

	public Map<String, Integer> getTermCount() {
		return termCount;
	}

	public void print(Set<String> terms, File outputFile) throws FileNotFoundException{
		PrintStream output = new PrintStream(new FileOutputStream(outputFile));
		for (String term: termInOrder){
			if (terms.contains(term))
				output.printf("%d\t%s\n", termCount.get(term), term);
		}
		System.out.println(terms.size());
		System.out.println((double)terms.size() / termCount.size());
		output.close();
	}

	public Set<String> getDiff(Set<String> terms){
		Set<String> diff = new TreeSet<>(termCount.keySet());
		diff.removeAll(terms);
		return diff;
	}

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		analyzeEnglishDC();
		analyzeFrenchDC();
	}

	private static void analyzeFrenchDC() throws IOException, FileNotFoundException, ParserConfigurationException, SAXException {
		File rawFreqFile = new File(baseDir, "fr_dc_lcase.freq.txt");
		File duFreqFile = new File(baseDir, "fr_dc_lcase_sense.freq.txt");
		String intersectionFileName = "intersection.txt";
		String diffFileName = "diff.txt";

		DefaultLexconnReader lexconnReader = LexconnBuilder.getLEXCONN();
		Map<String, String> idToCanonicalForm = lexconnReader.getIdToCanonicalForm();
		Map<String, String> formToId = lexconnReader.getFormToId();
		
		Function<String, String> mapToCanonicalForm = (str) -> idToCanonicalForm.get(str) != null ? idToCanonicalForm.get(str) : str.toUpperCase();
		Function<String, String> normalizer = (str) -> formToId.get(str) != null ? formToId.get(str) : str.toUpperCase();
		
//		Map<String, String> canonicalToId = new HashMap<>();
//		for (Entry<String, String> id2Canonical: idToCanonicalForm.entrySet()){
//			String oldId = canonicalToId.put(id2Canonical.getValue(), id2Canonical.getKey());
//			if (oldId != null){
//				System.out.println(id2Canonical.getValue() + "->" + oldId + ", " + id2Canonical.getKey());
//			}
//		}
		for (File file: new File[]{rawFreqFile, duFreqFile}){
			String fileName = file.getName();
			File dir = new File(baseDir, fileName.substring(0, fileName.lastIndexOf('.')));
			File intersectFile = new File(dir, intersectionFileName);
			mapToNorm(file, intersectFile, normalizer);
			mapToNorm(intersectFile, new File(dir, "from_" + intersectionFileName), mapToCanonicalForm);
			dir.mkdir();
			
			UniqFreqReader reader = new UniqFreqReader(intersectFile);
			
			Set<String> diff = new TreeSet<String>(idToCanonicalForm.keySet());
			diff.removeAll(reader.getTermCount().keySet());
			FileUtils.writeLines(new File(dir, diffFileName), diff);
			FileUtils.writeLines(new File(dir, "form_" + diffFileName), diff.stream().map(mapToCanonicalForm).collect(Collectors.toList()));
		}
	}

	static File baseDir = new File("/Users/majid/Documents/resource/dc_freq");
	public static void analyzeEnglishDC() throws IOException, FileNotFoundException {
		File pdtbFreqFile = new File(baseDir, "conll_pruned_dc_sense.freq.txt");
		File europarlFreqFile = new File(baseDir, "en_dc_lcase_sense.freq.txt");
		File intersectionFile = new File(baseDir, "intersection.freq.txt");
		File diffPdtbFile = new File(baseDir, "diff_pdtb.freq.txt");
		File diffEuroparlFile = new File(baseDir, "diff_europarl.freq.txt");

		UniqFreqReader pdtbReader = new UniqFreqReader(pdtbFreqFile);
		UniqFreqReader europarlReader = new UniqFreqReader(europarlFreqFile);

		Map<String, Integer> pdtbTermCount = pdtbReader.getTermCount();
		Map<String, Integer> europarlTermCount = europarlReader.getTermCount();

		Set<String> intersection = new TreeSet<>(pdtbTermCount.keySet());
		intersection.retainAll(europarlTermCount.keySet());

		System.out.println("Intersection:");
		europarlReader.print(intersection, intersectionFile);

		System.out.println("diff pdtb:");
		pdtbReader.print(pdtbReader.getDiff(intersection), diffPdtbFile);

		System.out.println("diff europarl:");
		europarlReader.print(europarlReader.getDiff(intersection), diffEuroparlFile);
	}
}
