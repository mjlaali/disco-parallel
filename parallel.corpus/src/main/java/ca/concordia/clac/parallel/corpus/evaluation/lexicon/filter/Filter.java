package ca.concordia.clac.parallel.corpus.evaluation.lexicon.filter;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import org.apache.commons.math3.util.Pair;

public class Filter {

	public static Map<Pair<String, String>, Double> filter(Map<Pair<String, String>, Double> aLexicon, BiPredicate<String, String> predicate){
		Map<Pair<String, String>, Double> filteredLexicon = new HashMap<>();
		
		aLexicon.forEach(
				(pair, prob) ->	{
					if (predicate.test(pair.getFirst(), pair.getSecond())) 
						filteredLexicon.put(pair, prob);
				});
		return filteredLexicon;
	}
	
	public static Map<Pair<String, String>, Double> filterBaseDc(Map<Pair<String, String>, Double> aLexicon, Predicate<String> predicate){
		return filter(aLexicon, (dc, rel) -> predicate.test(dc));
	}
	
	public static Map<Pair<String, String>, Double> filterBaseRelation(Map<Pair<String, String>, Double> aLexicon, Predicate<String> predicate){
		return filter(aLexicon, (dc, rel) -> predicate.test(rel));
	}
	
	public static class FilterInFrequendDC implements Predicate<String>{
		private Map<String, Integer> counts;
		private int threshold;
		
		public FilterInFrequendDC(Map<String, Integer> counts, int threshold){
			this.counts = counts;
			this.threshold = threshold;
		}
		
		@Override
		public boolean test(String t) {
			Integer count = counts.get(t);
			return count != null && count >= threshold;
		}
	}
	
	public static class FilterNotSeenRelations implements Predicate<String>{
		private Set<String> seenRelations;
		
		public FilterNotSeenRelations(Set<String> seenRelations) {
			this.seenRelations = seenRelations;
		}
		
		@Override
		public boolean test(String t) {
			return seenRelations.contains(t);
		}
		
	}
	
}
