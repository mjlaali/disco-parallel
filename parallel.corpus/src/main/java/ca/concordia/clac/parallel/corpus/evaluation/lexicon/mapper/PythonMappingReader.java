package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.math3.util.Pair;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;

public class PythonMappingReader{
	public static final Set<String> PDTB_RELATIONS = new TreeSet<>(Arrays.asList("Comparison.Concession", "Comparison.Contrast", "Contingency.Cause.Reason", "Contingency.Cause.Result", 
				"Contingency.Condition", "Expansion.Alternative", "Expansion.Alternative.Chosen-Alternative", "Expansion.Conjunction", "Expansion.Instantiation", 
				"Expansion.Restatement", "Temporal.Asynchronous.Precedence", "Temporal.Asynchronous.Succession", "Temporal.Synchrony"));
	public static final Set<String> LEXCONN_RELATIONS = new TreeSet<>(Arrays.asList("alternation", "background", "background-inverse", "commentary", "concession", "condition", "consequence",
				"continuation", "contrast", "detachment", "elaboration", "evidence", "explanation", "explanation*", "flashback", "goal", "narration", "opposition",
				"parallel", "rephrasing", "result", "result*", "summary", "temploc", "temporal", "unknown"));
 
	public static final String MAPPING_PROB_FILE = "mapping.txt";
	public static final String PDTB_REL_FILE = "pdtb-relations.txt";
	public static final String LEXCONN_REL_FILE = "lexconn-relations.txt";
	
	public static double[][] loadMappingProb(Reader reader) throws IOException{
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withDelimiter(' ');

		@SuppressWarnings("resource")
		CSVParser csvFileParser = new CSVParser(reader, csvFileFormat);
		List<CSVRecord> csvRecords  = csvFileParser.getRecords();
		List<double[]> table = new LinkedList<>();
		for (CSVRecord record: csvRecords){
			List<Double> values = new LinkedList<>();
			for (int i = 0; i < record.size(); i++){
				values.add(Double.parseDouble(record.get(i)));
			}
			Double[] array = values.toArray(new Double[values.size()]);
			table.add(ArrayUtils.toPrimitive(array));
		}

		return table.toArray(new double[table.size()][]);
	}
	private SetMultimap<String, Pair<String, Double>> mapping = HashMultimap.create();
	private SetMultimap<String, Pair<String, Double>> reverseMapping = HashMultimap.create();
	
	public PythonMappingReader(File directory) throws IOException {
		List<String> pdtbRelations = FileUtils.readLines(new File(directory, PDTB_REL_FILE), StandardCharsets.UTF_8).stream().map(SenseRemoval.relationNormalizer).collect(Collectors.toList());
		List<String> lexconnRelations = FileUtils.readLines(new File(directory, LEXCONN_REL_FILE), StandardCharsets.UTF_8);
		if (!PDTB_RELATIONS.containsAll(pdtbRelations) || !LEXCONN_RELATIONS.containsAll(lexconnRelations)){
			pdtbRelations.removeAll(PDTB_RELATIONS);
			lexconnRelations.removeAll(LEXCONN_RELATIONS);
			System.err.println("PythonMappingReader.Loader() Some relations are missing: " + pdtbRelations.toString() + ", " + lexconnRelations.toString());
		}
		Reader reader = new FileReader(new File(directory, MAPPING_PROB_FILE));
		double[][] mappingProbs = loadMappingProb(reader);
	
		for (int i = 0; i < mappingProbs.length; i++){
			for (int j = 0; j < mappingProbs[i].length; j++){
				mapping.put(pdtbRelations.get(i), new Pair<>(lexconnRelations.get(j), new Double(mappingProbs[i][j])));
				reverseMapping.put(lexconnRelations.get(j), new Pair<>(pdtbRelations.get(i), new Double(mappingProbs[i][j])));
			}
		}
	}
	
	public SetMultimap<String, Pair<String, Double>> getMapping() {
		return mapping;
	}
	
	public SetMultimap<String, Pair<String, Double>> getReverseMapping() {
		return reverseMapping;
	}
}