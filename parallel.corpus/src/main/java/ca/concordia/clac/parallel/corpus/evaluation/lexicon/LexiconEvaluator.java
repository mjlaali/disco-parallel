package ca.concordia.clac.parallel.corpus.evaluation.lexicon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math3.util.Pair;

public class LexiconEvaluator {
	Map<Pair<String, String>, Double> intersection;
	Map<Pair<String, String>, Double> notFound;
	Map<Pair<String, String>, Double> suggested;
	
	Map<Pair<String, String>, Double> goldLexicon;
	Map<Pair<String, String>, Double> extractedLexicon;
	
	public LexiconEvaluator(Map<Pair<String, String>, Double> goldLexicon, Map<Pair<String, String>, Double> extractedLexicon) {
		this.goldLexicon = goldLexicon;
		this.extractedLexicon = extractedLexicon;
		evaluate();
	}

	private void evaluate(){
		intersection = new HashMap<>();
		notFound = new HashMap<>();
		suggested = new HashMap<>();

		partition(goldLexicon, extractedLexicon, intersection, notFound);
		partition(extractedLexicon, goldLexicon, null, suggested);
	}

	public Map<Pair<String, String>, Double> getIntersection() {
		return intersection;
	}

	public Map<Pair<String, String>, Double> getNotFound() {
		return notFound;
	}

	public Map<Pair<String, String>, Double> getSuggested() {
		return suggested;
	}

	private void partition(Map<Pair<String, String>, Double> goldLexicon, Map<Pair<String, String>, Double> extractedLexicon,
			Map<Pair<String, String>, Double> intersection, Map<Pair<String, String>, Double> notFound) {
		for (Entry<Pair<String, String>, Double> aGoldEntry: goldLexicon.entrySet()){
			Double extractedLexiconProb = extractedLexicon.get(aGoldEntry.getKey());
			if (extractedLexiconProb != null){
				if (intersection != null)
					intersection.put(aGoldEntry.getKey(), extractedLexiconProb);
			} else {
				notFound.put(aGoldEntry.getKey(), aGoldEntry.getValue());
			}
		}
	}

	public List<Boolean> getJudges() {
		ArrayList<Entry<Pair<String, String>, Double>> lexicon = new ArrayList<>(extractedLexicon.entrySet());
		Collections.sort(lexicon, (a, b) -> b.getValue().compareTo(a.getValue()));
		List<Boolean> judges = new ArrayList<>();
		for (Entry<Pair<String, String>, Double> anEntry: lexicon){
			if (intersection.containsKey(anEntry.getKey()))
				judges.add(true);
			else
				judges.add(false);
		}
		return judges;
	}
	
	
}
