package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import org.apache.uima.jcas.JCas;

public interface JCasInitializable {
	public void init(JCas jcas);
}
