package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

public class OchAlignmentReader implements AlignmentReader{
	int lineNumber = -1;
	IBMAlignmentReader ibmReader;
	Scanner scanner;
	List<Pair<Integer, Integer>> alignments;
	
	int idxEn;
	int idxFr;
	
	public OchAlignmentReader(InputStream input, IBMAlignmentReader ibmReader, String langs) {
		this.ibmReader = ibmReader;
		scanner = new Scanner(input, "UTF-8");
		if (langs.equals("en-fr") || langs.equals("fr-en")){
			idxEn = Arrays.asList(langs.split("-")).indexOf("en");
			idxFr = 1 - idxEn; 
		} else {
			throw new RuntimeException(langs + " does not indicate the languages properly");
		}

	}

	public OchAlignmentReader(File alignmentFile, IBMAlignmentReader reader) throws FileNotFoundException {
		this(new FileInputStream(alignmentFile), reader, alignmentFile.getName().substring(alignmentFile.getName().lastIndexOf('.') + 1));
	}

	@Override
	public List<String> getEnWords() {
		return ibmReader.getEnWords();
	}

	@Override
	public List<String> getFrWords() {
		return ibmReader.getFrWords();
	}

	@Override
	public List<Pair<Integer, Integer>> getAlignments() {
		return alignments;
	}

	@Override
	public void next() {
		lineNumber += 1;
		ibmReader.next();
		String line = scanner.nextLine();
		this.alignments = new ArrayList<>();
		String[] alignments = line.split(" ");
		for (String alignment: alignments){
			List<Integer> parsedAlignments = Arrays.stream(alignment.split("-")).map(Integer::parseInt).collect(Collectors.toList());
			this.alignments.add(new Pair<>(parsedAlignments.get(idxEn), parsedAlignments.get(idxFr)));
		}
	}

	@Override
	public boolean hasNext() {
		return scanner.hasNext() && ibmReader.hasNext();
	}

	@Override
	public void close() {
		scanner.close();
		ibmReader.close();
	}

	@Override
	public String getLineNumber() {
		return "" + lineNumber + ":" + ibmReader.getLineNumber();
	}

}
