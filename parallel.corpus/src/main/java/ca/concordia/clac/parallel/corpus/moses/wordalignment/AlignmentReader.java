package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.io.Closeable;
import java.util.List;

import org.apache.commons.math3.util.Pair;

public interface AlignmentReader extends Closeable{

	List<String> getEnWords();

	List<String> getFrWords();

	List<Pair<Integer, Integer>> getAlignments();

	void next();
	
	boolean hasNext();

	String getLineNumber();
	void close();

}
