package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

public class LeaveOneOutMapper extends Mapper{
	Map<String, SetMultimap<String, Pair<String, Double>>> mappings = new HashMap<>();
	Map<String, SetMultimap<String, Pair<String, Double>>> reverseMapping = new HashMap<>();
	SetMultimap<String, Pair<String, Double>> avgMapping = HashMultimap.create();

	public LeaveOneOutMapper(File directory) throws IOException {
		List<String> pdtbRelations = new ArrayList<>(PythonMappingReader.PDTB_RELATIONS);
		List<String> lexconnRelations = new ArrayList<>(PythonMappingReader.LEXCONN_RELATIONS);
		
		Collections.sort(pdtbRelations);
		Collections.sort(lexconnRelations);
		double[][] avgMapping = new double[PythonMappingReader.PDTB_RELATIONS.size()][PythonMappingReader.LEXCONN_RELATIONS.size()];

		int dcCnt = 0;
		for (File aDc: directory.listFiles()){
			if (!aDc.isDirectory())
				continue;
			PythonMappingReader loader = new PythonMappingReader(aDc);
			SetMultimap<String, Pair<String, Double>> dcMapping = loader.getMapping();
			mappings.put(aDc.getName(), dcMapping);
			reverseMapping.put(aDc.getName(), HashMultimap.create());
//			reverseMapping.put(aDc.getName(), loader.getReverseMapping());
			for (Entry<String, Pair<String, Double>> anEntry: dcMapping.entries()){
				int pdtbIdx = pdtbRelations.indexOf(anEntry.getKey());
				int lexconnIdx = lexconnRelations.indexOf(anEntry.getValue().getFirst());
				avgMapping[pdtbIdx][lexconnIdx] = avgMapping[pdtbIdx][lexconnIdx] + anEntry.getValue().getSecond();
			}
			dcCnt++;
		}
		

		for (int i = 0; i < avgMapping.length; i++){
			for (int j = 0; j < avgMapping[i].length; j++){
				this.avgMapping.put(pdtbRelations.get(i), new Pair<>(lexconnRelations.get(j), avgMapping[i][j] / dcCnt));
			}
		}
	}
	
	public LeaveOneOutMapper(Map<String, SetMultimap<String, Pair<String, Double>>> mappings) {
		this.mappings = mappings;
	}

	@Override
	public Collection<Pair<String, Double>> getMapping(String aDc, String pdtbRel) {
		Set<Pair<String, Double>> results = null;
		try {
			results = mappings.get(aDc).get(pdtbRel);
		} catch (NullPointerException e) {
		}
		if (results == null)
			results = Collections.emptySet();
		SetMultimap<String, Pair<String, Double>> reMap = reverseMapping.get(aDc);
		for (Pair<String, Double> aMap: results){
			if (aMap.getSecond() > 0.05)
				reMap.put(aMap.getFirst(), new Pair<>(pdtbRel, aMap.getSecond()));
		}
		return results;
	}

	@Override
	public Collection<Pair<String, Double>> getReverseMapping(String aDc, String lexconnRel) {
		Set<Pair<String, Double>> results = null;
		try {
			results = reverseMapping.get(aDc).get(lexconnRel);
		} catch (NullPointerException e) {
		}
		if (results == null)
			results = Collections.emptySet();
		return results;
	}

	@Override
	public Set<String> getPDTBRelations() {
		return PythonMappingReader.PDTB_RELATIONS;
	}

	@Override
	public Set<String> getLexconnRelations() {
		return PythonMappingReader.LEXCONN_RELATIONS;
	}

	@Override
	public SetMultimap<String, Pair<String, Double>> getMapping() {
		return avgMapping;
	}

}
