package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.lexicalscope.jewel.cli.ArgumentValidationException;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.GeneralFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;
import ca.concordia.clac.util.csv.CSVMerger;
import ca.concordia.clac.util.csv.CSVUtils;
import ca.concordia.clac.util.csv.MergeStrategy;
import ca.concordia.clac.util.csv.strategy.merge.PriorityMerger;

public class CrowdFlowerToAlignmentFeature{
	CSVContent content;
	final String annotatorName;
	final String alignmentColumn;
	final Tag lang;
	final String mergedColumn;
	final String alignmentAndCommentColumn;

	public CrowdFlowerToAlignmentFeature(CSVContent content, String annotatorName, String alignmentColumn, String markedTextColumn,
			Tag lang) throws IOException {
		super();

		if (lang != Tag.EN && lang != Tag.FR)
			throw new ArgumentValidationException();
		this.content = content;
		this.annotatorName = annotatorName;
		this.alignmentColumn = alignmentColumn;
		this.lang = lang;
		
		mergedColumn = Tag.addTags(GeneralFeatures.BOLDED_FEATURE_NAME, lang);
		alignmentAndCommentColumn = Tag.addTags(DcAlignmentsFeatures.ALIGNMENT_FEATURE_NAME, lang);
		
		content.renameColumn(markedTextColumn, mergedColumn);
		prepareCrowdAnnotation();

	}

	/**
	 * Combine a column (alignmentAndCommentColumn) which contains the reason and annotation column. 
	 * Then it removes all other columns except alignmentAndCommentColumn and {@link CrowdFlowerToAlignmentFeature#mergedColumn}  
	 * @param crowdAnnotationFileInfo 
	 * @param mergedColumn
	 * @param alignmentAndCommentColumn
	 * @return
	 * @throws IOException
	 */
	private CSVContent prepareCrowdAnnotation() throws IOException {
		String reasonColumn = alignmentColumn + "_gold_reason";
		String annotatorIdColumn = "_worker_id";
		String trustColumn = "_trust";
		for (String aColumn: new String[]{reasonColumn, annotatorIdColumn, trustColumn}){
			if (content.getHeaderToColumnIdx().get(aColumn) == null){
				content.addColumn(aColumn, 0);
			}
		}
		
		content.addColumn(alignmentAndCommentColumn, this::buildAnAlignment, alignmentColumn, reasonColumn, annotatorIdColumn, trustColumn);
		CSVUtils utils = new CSVUtils(content);

		MergeStrategy mergeStrategy = AlignmentFeature.getMergeStrategy(alignmentAndCommentColumn).andThen(new PriorityMerger(0));
		utils.mergeRows(mergedColumn, mergeStrategy);

		content = utils.getCsvContent();
		for (String header: new ArrayList<>(content.getHeader())){
			if (!(header.equals(alignmentAndCommentColumn) || header.equals(mergedColumn)))
				content.deleteColumn(header);
		}


		return content;
	}
	
	private String buildAnAlignment(List<String> columns){
		String annotation = columns.get(0);
		String reason = columns.get(1);
		String name = columns.get(2);
		double score = columns.get(3).isEmpty() ? 0 : Double.parseDouble(columns.get(3));
		if (score == 1.0){
			score = 0.99; //there is no 100% sure annotations
		}
		
		AlignmentFeature alignmentFeature = null;
		if (annotatorName != null)
			alignmentFeature = new AlignmentFeature(annotation, annotatorName + ":" + reason, -1, -1, 1);
		else 
			alignmentFeature = new AlignmentFeature(annotation, name, -1, -1, score);
		return AlignmentFeature.convertToString(alignmentFeature);
	}
	
	public CSVContent mergeCrowdAnnotations(CSVContent dataset) throws IOException {
		MergeStrategy mergeStrategy = AlignmentFeature.getMergeStrategy(alignmentAndCommentColumn).andThen(new PriorityMerger(0));
		CSVMerger csvMerger = new CSVMerger(mergedColumn, mergeStrategy);
		csvMerger.merge(dataset);
		csvMerger.merge(content);
		
		dataset = csvMerger.getCsvContent();
		return dataset;
	}
	
	public CSVContent getContent() {
		return content;
	}

}