package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerFormater;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;

public class GeneralFeatures implements Function<DiscourseConnective, List<Feature>>, JCasInitializable{
	public static final String BOLDED_FEATURE_NAME = Tag.tagToString(Tag.chunk, Tag.bolded);
	private Map<DiscourseConnective, ParallelChunk> dcToParalleChunk;
	private String docId;
	private final CrowdFlowerFormater crowdFlowerFormater = new CrowdFlowerFormater();
	
	@Override
	public void init(JCas aJCas) {
		docId = DocumentMetaData.get(aJCas).getDocumentId();
		docId = docId.substring(docId.lastIndexOf('/') + 1);
		dcToParalleChunk = new HashMap<>();
		Map<DiscourseConnective, Collection<ParallelChunk>> indexChunks = JCasUtil.indexCovering(aJCas, DiscourseConnective.class, ParallelChunk.class);
		for (Entry<DiscourseConnective, Collection<ParallelChunk>> anEntry: indexChunks.entrySet()){
			ParallelChunk smallest = null;
			int smallestSize = Integer.MAX_VALUE;
			for (ParallelChunk chunk: anEntry.getValue()){
				int chunkSize = chunk.getEnd() - chunk.getBegin();
				if (chunkSize < smallestSize){
					smallestSize = chunkSize;
					smallest = chunk;
				}
			}
			dcToParalleChunk.put(anEntry.getKey(), smallest);
		}
	}

	@Override
	public List<Feature> apply(DiscourseConnective dc) {
		ParallelChunk chunk = dcToParalleChunk.get(dc);
		String chunkTextBolded = crowdFlowerFormater.makeAnnotationBold(chunk, dc);
		ParallelChunk translation = chunk.getTranslation();
		String alignedTextSuperscript = crowdFlowerFormater.putSubscriptForDuplicateWords(translation);
		
		FeatureUtils featureUtils = new FeatureUtils()
				.addFeature(chunk, Tag.chunk)
				.addFeature(new Feature(BOLDED_FEATURE_NAME, chunkTextBolded))
				.addFeature(new Feature(Tag.tagToString(Tag.chunk, Tag.id), chunk.getDocOffset()))
				
				.addFeature(translation, Tag.chunk, Tag.alignment)
				.addFeature(new Feature(Tag.tagToString(Tag.chunk, Tag.alignment, Tag.superscript), alignedTextSuperscript))
				
				.addFeature(dc, Tag.dc)
				.addFeature(new Feature(Tag.tagToString(Tag.dc, Tag.sense), dc.getSense()))
				
				.addFeature(new Feature(Tag.tagToString(Tag.doc, Tag.id), docId));

		
		return featureUtils.getFeatures();
	}
	
	public static AnalysisEngineDescription getDescription(File outputDir) throws ResourceInitializationException{
		return FeatureJsonExtractor.getDescription(outputDir, GeneralFeatures.class);
	}
	
}