package ca.concordia.clac.parallel.corpus.writer;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class LexconnEntry{
	String id;
	String relations;
	Integer freq;
	Map<String, Double> relation_probabilities;
	List<String> forme;
	
	public LexconnEntry() {
	}
	
	public LexconnEntry(String id, String relations, List<String> forme) {
		this.id = id;
		this.relations = relations;
		this.forme = forme;
	}
	
	public LexconnEntry(String id, List<String> forme, int freq, Map<String, Double> relationsProb) {
		this(id, null, forme);
		relations = new TreeSet<>(relationsProb.keySet()).toString();
		relations = relations.substring(1, relations.length() - 1);
		this.relation_probabilities = relationsProb;
		this.freq = freq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((forme == null) ? 0 : forme.hashCode());
		result = prime * result + ((freq == null) ? 0 : freq.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((relation_probabilities == null) ? 0 : relation_probabilities.hashCode());
		result = prime * result + ((relations == null) ? 0 : relations.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LexconnEntry other = (LexconnEntry) obj;
		if (forme == null) {
			if (other.forme != null)
				return false;
		} else if (!forme.equals(other.forme))
			return false;
		if (freq == null) {
			if (other.freq != null)
				return false;
		} else if (!freq.equals(other.freq))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (relation_probabilities == null) {
			if (other.relation_probabilities != null)
				return false;
		} else if (!relation_probabilities.equals(other.relation_probabilities))
			return false;
		if (relations == null) {
			if (other.relations != null)
				return false;
		} else if (!relations.equals(other.relations))
			return false;
		return true;
	}

	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}
	
	@XmlAttribute
	public void setRelations(String relations) {
		this.relations = relations;
	}
	
	public String getRelations() {
		return relations;
	}

	@XmlElement
	public void setForme(List<String> forme) {
		this.forme = forme;
	}
	
	public List<String> getForme() {
		return forme;
	}
	
	public Integer getFreq() {
		return freq;
	}
	
	@XmlElement
	public void setFreq(Integer freq) {
		this.freq = freq;
	}
	
	public Map<String, Double> getRelation_probabilities() {
		return relation_probabilities;
	}
	
	@XmlElement
	public void setRelation_probabilities(Map<String, Double> relation_probabilities) {
		this.relation_probabilities = relation_probabilities;
	}
}