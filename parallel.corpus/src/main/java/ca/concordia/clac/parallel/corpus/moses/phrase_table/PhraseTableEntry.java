package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import ca.concordia.clac.parallel.corpus.moses.MosesUtils;

public class PhraseTableEntry {
	private Entry[] entreis;
	private int fPhrase;
	private String alignments;
	private int enIdx = 0;
	private int frIdx = 1;
	
	public Entry[] getEntreis() {
		return entreis;
	}
	public void setEntreis(Entry[] entreis) {
		this.entreis = entreis;
	}
	public Entry getEnEntry() {
		return entreis[enIdx];
	}
	public void setEnEntry(Entry enEntry) {
		entreis[enIdx] = enEntry;
	}
	public Entry getFrEntry() {
		return entreis[frIdx];
	}
	public void setFrEntry(Entry frEntry) {
		entreis[frIdx] = frEntry;
	}
	public int getfPhrase() {
		return fPhrase;
	}
	public void setfPhrase(int fPhrase) {
		this.fPhrase = fPhrase;
	}
	public String getAlignments() {
		return alignments;
	}
	public void setAlignments(String alignments) {
		this.alignments = alignments;
	}
	
	@Override
	public String toString() {
		return "PhraseTableEntry [entreis=" + Arrays.toString(entreis) + ", fPhrase=" + fPhrase + ", alignments="
				+ alignments + ", enIdx=" + enIdx + ", frIdx=" + frIdx + "]";
	}
	public PhraseTableEntry(Entry[] entreis, int fPhrase, String alignments, int enIdx, int frIdx) {
		super();
		this.entreis = entreis;
		this.fPhrase = fPhrase;
		this.alignments = alignments;
		this.enIdx = enIdx;
		this.frIdx = frIdx;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alignments == null) ? 0 : alignments.hashCode());
		result = prime * result + enIdx;
		result = prime * result + Arrays.hashCode(entreis);
		result = prime * result + fPhrase;
		result = prime * result + frIdx;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhraseTableEntry other = (PhraseTableEntry) obj;
		if (alignments == null) {
			if (other.alignments != null)
				return false;
		} else if (!alignments.equals(other.alignments))
			return false;
		if (enIdx != other.enIdx)
			return false;
		if (!Arrays.equals(entreis, other.entreis))
			return false;
		if (fPhrase != other.fPhrase)
			return false;
		if (frIdx != other.frIdx)
			return false;
		return true;
	}
	public static PhraseTableEntry make(String line) {
		String[] substring = line.split("\\|\\|\\|");
		
		String frenchText = MosesUtils.escapeMosesString(substring[0].trim());
		String englishText = MosesUtils.escapeMosesString(substring[1].trim());
		String strProbs = substring[2].trim();
		String alignments = substring[3];
		String strCounts = substring[4].trim();

		double pFrGivenEn, pEnGivenFr;
		
		List<Double> probs = Stream.of(strProbs.split(" ")).map(Double::parseDouble).collect(Collectors.toList());
		pFrGivenEn = probs.get(0);
		pEnGivenFr = probs.get(2);
		
		List<Double> counts;
		counts = Stream.of(strCounts.split(" ")).map(Double::parseDouble).collect(Collectors.toList());
		PhraseTableEntry aPhraseTableEntry = new PhraseTableEntry(
				new Entry[]{
						new Entry(englishText, pEnGivenFr, counts.get(1).intValue()), 
						new Entry(frenchText , pFrGivenEn, counts.get(0).intValue())
				}, counts.get(2).intValue(), alignments, 0, 1);
		return aPhraseTableEntry;
	}

}
