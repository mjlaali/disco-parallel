package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.tcas.Annotation;

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class CrowdFlowerFormater {
	public String putSubscriptForDuplicateWords(Annotation chunk){
		List<Token> words = JCasUtil.selectCovered(Token.class, chunk); 
		Map<String, Integer> annToEnd = new HashMap<>();
		Map<String, Integer> annCnt = new HashMap<>();
		Map<Integer, Integer> indexes = new HashMap<>();
		int base = chunk.getBegin();
		for (Token ann: words){
			String text = ann.getCoveredText().toLowerCase();
			if (text.length() == 1 && !Character.isLetter(text.charAt(0)))
				continue;
			Integer prevEnd = annToEnd.put(text, ann.getEnd());
			if (prevEnd != null){ //there is duplicate value for the annotation, lets put indexes
				Integer cnt = annCnt.get(text);
				if (cnt == null){	//this is the first time that we found duplicate annotations, let add index for the first one too
					indexes.put(prevEnd - 1 - base, 1);
					cnt = 1;
				}
				annCnt.put(text, cnt + 1);
				annToEnd.put(text, ann.getEnd());
				indexes.put(ann.getEnd() - 1 - base, cnt + 1);
			}
		}
		
		return getText(chunk.getCoveredText(), indexes);
	}
	
	public String getText(String text, Map<Integer, Integer> indexes) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < text.length(); i++){
			sb.append(text.charAt(i));
			if (indexes.get(i) != null){
				sb.append("<sub>");
				sb.append(indexes.get(i).toString());
				sb.append("</sub>");
			}
		}
		
		return sb.toString();
	}
	
	public String makeAnnotationBold(Annotation parentAnnotation, Annotation childAnnotation) {
		String coveredText = parentAnnotation.getCoveredText();
		String beforeDC = coveredText.substring(0, childAnnotation.getBegin() - parentAnnotation.getBegin());
		String afterDc = coveredText.substring(childAnnotation.getEnd() - parentAnnotation.getBegin());
		return beforeDC + "<b>" + childAnnotation.getCoveredText() + "</b>" + afterDc;
	}
}
