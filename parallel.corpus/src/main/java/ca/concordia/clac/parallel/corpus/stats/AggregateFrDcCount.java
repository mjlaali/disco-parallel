package ca.concordia.clac.parallel.corpus.stats;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import ca.concordia.clac.lexconn.DefaultLexconnReader;

public class AggregateFrDcCount {

	public void aggregate(DefaultLexconnReader lexconnReader, File dcCntFile, int threshold) throws UnsupportedEncodingException, FileNotFoundException{
		
		Map<String, Integer> cnts = new HashMap<>();
		Map<String, String> formToId = new HashMap<>();
		Map<String, String> withSpace = lexconnReader.getFormToId();
		withSpace.forEach((f, id) -> formToId.put(f.replace(" ", ""), id));
		if (withSpace.size() != formToId.size())
			System.err.println("Size has been changed: " + withSpace.size() + "<>" + formToId.size());
		
		Scanner scanner = new Scanner(new InputStreamReader(new FileInputStream(dcCntFile), "UTF-8"));
		while (scanner.hasNext()){
			int freq = scanner.nextInt();
			String dc = scanner.nextLine().trim().replace(" ", "");
			String id = formToId.get(dc);
			if (id == null){
				System.err.println("Id is null for " + dc);
				continue;
			}
			if (freq < threshold)
				continue;
		
			Integer cnt = cnts.get(id);
			if (cnt == null)
				cnt = 0;
			cnts.put(id, cnt + freq);
		}
		scanner.close();
		
		Map<String, String> idToCanonicalForm = lexconnReader.getIdToCanonicalForm();
		HashSet<String> targetIds = new HashSet<>(idToCanonicalForm.keySet());
		targetIds.removeAll(cnts.keySet());
		
		System.out.println(cnts.size());
		System.out.println(targetIds.size());
		
		
		for (String id: targetIds){
			System.out.println(idToCanonicalForm.get(id));
		}
		
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		File lexconnFile = new File(new File("/Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation"), DefaultLexconnReader.LEXCONN_FILE);
		DefaultLexconnReader lexconnReader = DefaultLexconnReader.getLexconnMap(lexconnFile);
		
		File dcCntFile = new File("resources/fr_dc_sorted.freq");
		int threshold = 50;

		new AggregateFrDcCount().aggregate(lexconnReader, dcCntFile, threshold);
	}
}
