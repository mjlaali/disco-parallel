package ca.concordia.clac.parallel.corpus.builder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.moses.wordalignment.WordAlignmentAnnotator;
import ca.concordia.clac.uima.Utils;
import ca.concordia.clac.uima.engines.LookupInstanceAnnotator;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;

/**
 * Add alignment between dcs in xmi files. Save the output as xmi files.
 * The input xmi files should contain the annotations of English discourse connectives.
 * Both English and French texts need to be tokenized.
 * French discourse connectives are either associated to a sense (i.e. aligned) or not (i.e. it is aligned to a non-dc english text)
 * If French discourse connectives are not aligned to a text, it is assumed that the discourse connectives are not translated, hence
 * they are removed from the annotations.
 * Check {@link AnnotationProjectionDatasetBuilder} for how to extract features from the dataset.
 * @author majid
 *
 */
public class AnnotationProjectionDatasetBuilder extends JCasAnnotator_ImplBase{
	int negative;
	int positive;
	
	public static AnalysisEngineDescription getDescription() throws ResourceInitializationException{
		return AnalysisEngineFactory.createEngineDescription(AnnotationProjectionDatasetBuilder.class);
	}
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		for (DiscourseConnective dc: JCasUtil.select(aJCas, DiscourseConnective.class)){
			if (dc.getSense() == null)
				++negative;
			else 
				++positive;
		}
		
		System.err.println("AnnotationProjectionDatasetBuilder.process(): Positive = " + positive + ", Negative = " + negative);
	}

	
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "a",
				longName = "alignmentFile", 
				description = "The word alignment file")
		public File getAlignmentFile();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
		
		@Option(
				shortName = "f",
				longName = "filterNotReliable",
				description = "Filter not reliable annotaitons")
		public boolean isFilterOn();
	}
	
	public static void main(String[] args) throws UIMAException, IOException, ParserConfigurationException, SAXException {
		Options options = CliFactory.parseArguments(Options.class, args);
		
		System.out.println("ParallelTextExtractor.main(): Input = " + options.getInputDataset());
		System.out.println("ParallelTextExtractor.main(): filter = " + options.isFilterOn());
		System.out.println("ParallelTextExtractor.main(): Output = " + options.getOutputDir());
		
		File datasetFld = options.getInputDataset();
		File alignmentFile = options.getAlignmentFile();
		File outputFld = options.getOutputDir();
		boolean filter = options.isFilterOn();
		
		CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		
		AggregateBuilder aggregateBuilder = buildAlignmentBuilder(alignmentFile, filter, outputFld);
		
		File outputXmi = new File(outputFld, "xmi");
		AnalysisEngineDescription writer = AnalysisEngineFactory.createEngineDescription(XmiWriter.class, 
				XmiWriter.PARAM_TARGET_LOCATION, outputXmi, XmiWriter.PARAM_OVERWRITE, true);
		aggregateBuilder.add(writer);
		
		Utils.runWithProgressbar(reader, aggregateBuilder.createAggregateDescription());
	}
	
	public static AggregateBuilder buildAlignmentBuilder(File alignmentFile, boolean filterOn, File outputFld)
			throws ResourceInitializationException, ParserConfigurationException, SAXException, IOException {
		AggregateBuilder aggregateBuilder = new AggregateBuilder();

		URL lexconnFile = DefaultLexconnReader.getDefaultLexconnFile();

		File frenchDc = new File(outputFld, "frenchdc.txt");
		DefaultLexconnReader lexconn = DefaultLexconnReader.getLexconnMap(lexconnFile);
		FileUtils.writeLines(frenchDc, new ArrayList<>(lexconn.getFormToId().keySet()));
		
		File retainLinesFile = new File(alignmentFile.getParentFile(), "europarl.clean.1.lines-retained");
		if (!retainLinesFile.exists()){
			throw new RuntimeException("Please copy the retained file to the directory that contains alignment file.");
		}
		AnalysisEngineDescription wordAligner = WordAlignmentAnnotator.getDescription(alignmentFile, retainLinesFile);
		aggregateBuilder.add(wordAligner);
		
		AnalysisEngineDescription frenchDCAnnotator = LookupInstanceAnnotator.getDescription(frenchDc, DiscourseAnnotationFactory.class);
		aggregateBuilder.add(frenchDCAnnotator, CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		AnalysisEngineDescription filter = AnnotationProjectionFilter.getDescription(filterOn);
		aggregateBuilder.add(filter);

		AnalysisEngineDescription stats = getDescription();
		aggregateBuilder.add(stats, CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		return aggregateBuilder;
	}

}
