package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.util.Pair;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;

public class BatchMapper extends Mapper{
	SetMultimap<String, Pair<String, Double>> mapping;
	SetMultimap<String, Pair<String, Double>> reverseMapping;
	private final Set<String> pdtbRelations = new HashSet<>();
	private final Set<String> lexconnRelations = new HashSet<>();
	
	public BatchMapper(File mappingFile) throws IOException {
		InputStreamReader fileReader = null;
		fileReader = new InputStreamReader(new FileInputStream(mappingFile), "UTF-8");
		init(fileReader);
		fileReader.close();
	}
	
	public BatchMapper(Reader fileReader) throws IOException {
		init(fileReader);
	}
	
	private void init(Reader fileReader) throws IOException{
		mapping = HashMultimap.create();
		reverseMapping = HashMultimap.create();
		CSVFormat csvFileFormat = CSVFormat.DEFAULT;

		@SuppressWarnings("resource")
		CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
		List<CSVRecord> csvRecords  = csvFileParser.getRecords();
		for (CSVRecord record: csvRecords){
			String pdtbRelation = SenseRemoval.relationNormalizer.apply(record.get(0));
			pdtbRelations.add(pdtbRelation);
			String lexconnRelation = record.get(1).toLowerCase();
			lexconnRelations.add(lexconnRelation);
			double prob = Double.parseDouble(record.get(2));
			mapping.put(pdtbRelation, new Pair<>(lexconnRelation, prob));
			reverseMapping.put(lexconnRelation, new Pair<>(pdtbRelation, prob));
		}
	}
	
	public BatchMapper(SetMultimap<String, Pair<String, Double>> mapping) {
		this.mapping = mapping;
	}

	@Override
	public Collection<Pair<String, Double>> getMapping(String aDc, String pdtbRel) {
		return mapping.get(SenseRemoval.relationNormalizer.apply(pdtbRel));
	}

	@Override
	public Collection<Pair<String, Double>> getReverseMapping(String aDc, String lexconnRel) {
		return reverseMapping.get(lexconnRel);
	}

	@Override
	public Set<String> getPDTBRelations() {
		return pdtbRelations;
	}

	@Override
	public Set<String> getLexconnRelations() {
		return lexconnRelations;
	}
	
	@Override
	public SetMultimap<String, Pair<String, Double>> getMapping() {
		return mapping;
	}

}
