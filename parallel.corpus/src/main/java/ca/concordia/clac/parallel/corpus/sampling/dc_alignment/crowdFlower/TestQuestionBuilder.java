package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerMerger;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;

/**
 * Given a csv file contains the gold alignments, generate a new csv file with 2 additional columns that contain the gold 
 * annotations with subscripts, so that it can be imported to CrowdFlower.
 * @author majid
 *
 */
public class TestQuestionBuilder {
	private CSVContent csvContent;

	public TestQuestionBuilder(CSVContent dataset) {
		this.csvContent = dataset;
	}
	
	public void add(String goldAlignmentsColumn, String subscriptedTextColumn, String beginColumn, String crowdFlowerColumn){
		csvContent.addColumn(crowdFlowerColumn, TestQuestionBuilder::extractSubscriptTextOfAlignments, goldAlignmentsColumn, subscriptedTextColumn, beginColumn);
		
	}
	
	private static String extractSubscriptTextOfAlignments(List<String> values){
		String alingments = values.get(0);
		if (alingments.isEmpty())
			return "";
		String subscriptedText = values.get(1);
		int begin = (int) Double.parseDouble(values.get(2));
		List<String> crowdFlowerValue = new ArrayList<>();
		
		for (AlignmentFeature alignment: AlignmentFeature.parse(alingments)){
			if (alignment.getBegin() == -1)
				crowdFlowerValue.add(".");
			else
				crowdFlowerValue.add(select(subscriptedText, alignment.getBegin() - begin, alignment.getEnd() - begin));
		}
		
		return crowdFlowerValue.stream().collect(Collectors.joining("\n"));
	}
	
	static String select(String subscriptedText, int begin, int end) {
		final String BEGIN_TAG = "<sub>";
		final String END_TAG = "</sub>";
		int beginSearch = 0;
		int pos = 0;
		boolean toSave = false;
		StringBuilder sb = new StringBuilder();
		while (beginSearch < subscriptedText.length()){
			final int beginTag = subscriptedText.indexOf(BEGIN_TAG, beginSearch);
			for (int i = beginSearch; i < (beginTag == -1 ? subscriptedText.length() : beginTag); i++){
				if (pos == begin){
					toSave = true;
				} else if (pos == end) {
					toSave = false;
				}
				if (toSave)
					sb.append(subscriptedText.charAt(i));
				++pos;
			}
			
			if (beginTag != -1){
				final int endTag = subscriptedText.indexOf(END_TAG, beginTag);
				if (toSave){
					sb.append(subscriptedText.substring(beginTag + BEGIN_TAG.length(), endTag));
				}
				beginSearch = endTag + END_TAG.length();
			} else
				beginSearch = subscriptedText.length();
		}
		
		return sb.toString();
	}

	public static boolean containing(final String a, final String b){
		return (a.contains(b) && !b.isEmpty()) || (b.contains(a) && !a.isEmpty());
	}
	
	public void syncGoldAlignmentsColumns(){
		String[] params = new String[]{
				Tag.tagToString(Tag.EN, Tag.dc, Tag.alignment, Tag.gold), 
				Tag.tagToString(Tag.FR, Tag.dc, Tag.alignment, Tag.gold), 
				Tag.tagToString(Tag.EN, Tag.chunk, Tag.bolded), 
				Tag.tagToString(Tag.FR, Tag.chunk, Tag.bolded), 
				Tag.tagToString(Tag.EN, Tag.chunk, Tag.begin), 
				Tag.tagToString(Tag.FR, Tag.chunk, Tag.begin), 
		};
		
		for (int src = 0; src < 2; src++){
			mergeGoldAlignmentsColumns( 
					params[0 + src], params[0 + 1 - src], 
					params[2 + src], params[2 + 1 - src],
					params[4 + src]);
		}
	}

	/**
	 * Merge srcGoldAlignmentColumn into trgGoldAlignmentColumn 
	 * @param srcGoldAlignmentColumn
	 * @param trgGoldAlignmentColumn
	 * @param srcBoldedTextColumn
	 * @param trgBoldedTextColumn
	 * @param srcTextBeginColumn
	 */
	public void mergeGoldAlignmentsColumns(final String srcGoldAlignmentColumn, final String trgGoldAlignmentColumn,
			final String srcBoldedTextColumn, final String trgBoldedTextColumn,
			final String srcTextBeginColumn){
		
		List<String> srcGoldAlignments = csvContent.getColumn(srcGoldAlignmentColumn);
		List<String> trgGoldAlignments = csvContent.getColumn(trgGoldAlignmentColumn);
		List<String> srcBoldedTexts = csvContent.getColumn(srcBoldedTextColumn);
		List<String> trgBoldedTexts = csvContent.getColumn(trgBoldedTextColumn);
		List<String> srcTextBegins = csvContent.getColumn(srcTextBeginColumn);
		
		List<String> newColumn = new ArrayList<>();
		for (int i = 0; i < trgGoldAlignments.size(); i++){
			String updated = trgGoldAlignments.get(i);
			if (trgGoldAlignments.get(i).isEmpty() && !srcGoldAlignments.get(i).isEmpty()	//need to move src alignment to trg alignment
				&& !trgBoldedTexts.get(i).isEmpty()){	//check if there is an alignment needed here! if this field is empty it means there is not highlighted text for alignment.
				final String boldTag = "<b>"; 
				String srcBoldedText = srcBoldedTexts.get(i);
				
				int begin = srcBoldedText.indexOf(boldTag);
				int end = srcBoldedText.indexOf("</b>"); 
				int base = (int) Double.parseDouble(srcTextBegins.get(i));
				String boldedWords = srcBoldedText.substring(begin + boldTag.length(), end);		// this is a word that was shown and then user select the alignment
				end -= boldTag.length();	//original text does not have <b> tag.
				
				String alignerName = null;
				double alignScore = -1;
				String trgBoldedText = trgBoldedTexts.get(i);
				String trgBoldedWords = trgBoldedText.substring(trgBoldedText.indexOf(boldTag) + boldTag.length(), trgBoldedText.indexOf("</b>"));	//this is a word that we are looking for alingment
				for (AlignmentFeature af: AlignmentFeature.parse(srcGoldAlignments.get(i))){
					if (containing(af.getText().toLowerCase(), trgBoldedWords.toLowerCase())){
						alignerName = af.getName();
						alignScore = af.getScore();
					}
				}
				updated = AlignmentFeature.convertToString(new AlignmentFeature(boldedWords, alignerName, base + begin, base + end, alignScore));
			} 
			
			newColumn.add(updated);
		}
		
		Integer idx = csvContent.getHeaderToColumnIdx().get(trgGoldAlignmentColumn);
		csvContent.deleteColumn(trgGoldAlignmentColumn);
		csvContent.addColumn(trgGoldAlignmentColumn, idx, newColumn);
	}
	
	public CSVContent getCsvContent() {
		return csvContent;
	}
	
	public static void main(String[] args) throws IOException {
		File datasetFile = new File("resources/crowdFlower/emnlp-dataset/test-questions/merged-sampled.csv");
		CSVContent dataset = new CSVContent(datasetFile);
		
		TestQuestionBuilder builder = new TestQuestionBuilder(dataset);
		
		CrowdFlowerMerger.mergeAlignments(dataset, "FR.dc.alignment", "FR.dc.alignment.gold");
		builder.syncGoldAlignmentsColumns();
		
		String goldAlignmentsColumn = "FR.dc.alignment.gold"; 
		String subscriptedTextColumn = "FR.chunk.alignment.superscript"; 
		String beginColumn = "FR.chunk.begin.alignment"; 
		String crowdFlowerColumn = "FR.CrowdFlower";
		builder.add(goldAlignmentsColumn, subscriptedTextColumn, beginColumn, crowdFlowerColumn);
		
		File outputFile = new File("resources/crowdFlower/emnlp-dataset/test-questions/sampled-with-gold-annotations.csv");
		builder.getCsvContent().save(outputFile);
		
		System.out.println("TestQuestionBuilder.main(): Done!");
	}
}
