package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Tag{
	EN, FR, GENERAL, PARSED, NOT_PARSED, dc, chunk, begin, end, id, doc, csv, alignment, bolded, superscript, sense, dataset, len, text, name, score, gold;
	
	public static final String SEPARATOR = ".";
	
	public static String tagToString(Collection<Tag> tags){
		tags = new TreeSet<>(tags);
		return tags.stream().map(Tag::toString).collect(Collectors.joining(SEPARATOR));
	}
	
	public static String tagToString(Tag... tags){
		return tagToString(Arrays.asList(tags));
	}
	
	public static Set<Tag> parse(String tags){
		List<Tag> tagsList = Stream.of(tags.split("[" + SEPARATOR + "]")).map(Tag::valueOf).collect(Collectors.toList());
		
		return new LinkedHashSet<>(tagsList);
	}
	
	public static String addTags(String aHeader, Tag... tags) {
		return addTags(aHeader, Arrays.asList(tags));
	}
	
	public static String addTags(String aHeader, Collection<Tag> tags) {
		Set<Tag> newTags = parse(aHeader);
		newTags.addAll(tags);
		return tagToString(newTags);
	}
	
	public static Tag getLang(Collection<Tag> tags){
		if (tags.contains(EN))
			return EN;
		if (tags.contains(FR))
			return FR;
		return null;
	}
	
	public static Tag[] addTags(Tag[] tags, Tag... newTags){
		List<Tag> union = new ArrayList<>(Arrays.asList(tags));
		union.addAll(Arrays.asList(newTags));
		return union.toArray(new Tag[union.size()]);
	}
}
