package ca.concordia.clac.parallel.corpus.stats;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.uima.UIMAException;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseArgument;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.discourse.type.DiscourseRelation;
import org.cleartk.ml.Feature;
import org.cleartk.ml.weka.WekaStringOutcomeDataWriter;
import org.xml.sax.SAXException;

import ca.concordia.clac.batch_process.BatchProcess;
import ca.concordia.clac.ml.classifier.ClassifierAlgorithmFactory;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import ca.concordia.clac.ml.classifier.StringClassifierLabeller;
import ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator;

class DummyClass implements Function<DiscourseConnective, String>{

	@SuppressWarnings("unused")
	@Override
	public String apply(DiscourseConnective t) {
		String dcText = t.getCoveredText();
		DiscourseRelation discourseRelation = t.getDiscourseRelation();
		String relationText = discourseRelation.getCoveredText();
		DiscourseArgument arg1 = discourseRelation.getArguments(0);
		DiscourseArgument arg2 = discourseRelation.getArguments(2);
		String arg1Text = arg1.getCoveredText();
		return null;
	}

}

public class RelationToTextExtractor implements ClassifierAlgorithmFactory<String, DiscourseRelation>{

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
	}

	@Override
	public InstanceExtractor<DiscourseRelation> getExtractor(JCas jCas) {
		InstanceExtractor<DiscourseRelation> extractor = new InstanceExtractor<DiscourseRelation>() {

			@Override
			public Collection<DiscourseRelation> getInstances(JCas container) {
				JCas enView;
				try {
					enView = container.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
				} catch (CASException e) {
					throw new RuntimeException(e);
				}
				Collection<DiscourseRelation> relations = JCasUtil.select(enView, DiscourseRelation.class);
				for (DiscourseRelation dc: relations){
					//TODO: extract what you want here and write in console or a file.
					System.out.println(dc.getCoveredText());
				}
				return relations;
			}
		};
		return extractor;
	}

	@Override
	public List<Function<DiscourseRelation, List<Feature>>> getFeatureExtractor(JCas jCas) {
		return Collections.emptyList();
	}

	@Override
	public Function<DiscourseRelation, String> getLabelExtractor(JCas jCas) {
		return (dc) -> "dummy";
	}

	@Override
	public BiConsumer<String, DiscourseRelation> getLabeller(JCas jCas) {
		return (str, dr) -> System.out.print("");
	}

	public static AnalysisEngineDescription getWriterDescription(File outputFld) throws ResourceInitializationException, MalformedURLException, URISyntaxException{
		return StringClassifierLabeller.getWriterDescription(
				RelationToTextExtractor.class,
				WekaStringOutcomeDataWriter.class, 
				outputFld 
				);	

	}

	public static void main(String[] args) throws UIMAException, IOException, URISyntaxException, SAXException, CpeDescriptorException{
		BatchProcess batchProcess = null;
		File inputDirectory = new File("resources/small-sample");
		File outputDirectory = new File("outputs/writingRelations");
		File dummyOutput = new File("outputs/dummyOutput");
		batchProcess = new ParallelCorpusAnnotator().
				getBatchProcess(inputDirectory, outputDirectory);
		AnalysisEngineDescription writer = getWriterDescription(dummyOutput);
		String targetTask = "writingRelations";
		batchProcess.addProcess(targetTask, writer);
		batchProcess.clean(targetTask);

		batchProcess.run();
		batchProcess.save();
	}

}
