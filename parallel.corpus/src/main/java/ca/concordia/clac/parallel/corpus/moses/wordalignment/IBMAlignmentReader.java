package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.math3.util.Pair;

import ca.concordia.clac.parallel.corpus.moses.MosesUtils;

public class IBMAlignmentReader implements AlignmentReader{
	int lineNumber = -3;
	Scanner scanner;
	String[][] words = new String[2][];
	int idxEn;
	int idxFr;
	
	Pattern alignmentLinePattern = Pattern.compile("(\\S+) \\(\\{ ((\\d+ )*)\\}\\)");
	List<List<Integer>> wordAlignments;
	
	public IBMAlignmentReader(File file) throws FileNotFoundException {
		this(new FileInputStream(file), file.getName().substring(0, file.getName().indexOf('.')));
	}
	
	public IBMAlignmentReader(InputStream input, String langs){
		scanner = new Scanner(input, "UTF-8");
		idxEn = Arrays.asList(langs.split("-")).indexOf("en");
		idxFr = 1 - idxEn; 
		
	}

	@Override
	public List<String> getEnWords() {
		return Arrays.asList(words[idxEn]);
	}

	@Override
	public List<String> getFrWords() {
		return Arrays.asList(words[idxFr]);
	}

	@Override
	public List<Pair<Integer, Integer>> getAlignments() {
		List<Pair<Integer, Integer>> alignment = new ArrayList<>();
		int idx = 0;
		for (List<Integer> wordAlignment: wordAlignments){
			if (idx == 0 || wordAlignment.isEmpty()){ //If the alignment is for 'NULL' or there is no alignment
				//ignore the alignment
			} else {
				wordAlignment = wordAlignment.stream().map((i) -> i - 1).collect(Collectors.toList());
				for (Integer anAlignment: wordAlignment){
					int[] alignmentPair = new int[]{anAlignment, idx - 1};
					alignment.add(new Pair<>(alignmentPair[idxEn], alignmentPair[idxFr]));
				}
				
			}
			idx++;
		}
		
		return alignment;
	}

	@Override
	public void next() {
		lineNumber += 3;
		scanner.nextLine();
		words[0] = MosesUtils.escapeMosesString(scanner.nextLine()).split(" ");
		words[1] = parse(MosesUtils.escapeMosesString(scanner.nextLine().trim()));
		
		for (String[] aWordList: words)
			for (int i = 0; i < aWordList.length; i++){
				aWordList[i] = StringEscapeUtils.unescapeXml(aWordList[i]);
			}
				
	}
	

	private String[] parse(String aLine) {
		List<String> words = new ArrayList<>();
		wordAlignments = new ArrayList<>();
		
		Matcher matcher = alignmentLinePattern.matcher(aLine);
		int end = -1;
		while(matcher.find()){
			words.add(matcher.group(1));
			List<Integer> indexes = Arrays.asList(matcher.group(2).split(" ")).stream().filter((s) -> s.length() > 0).map(Integer::parseInt).collect(Collectors.toList());
			wordAlignments.add(indexes);
			end = matcher.end();
		}
		if (end != aLine.length()){
			throw new RuntimeException("Line does not match with pattern, stop at <" + end  + "> for: " + aLine);
		}
		if (words.get(0).equals("NULL")){
			words.remove(0);
		} else {
			throw new RuntimeException("The first word is not 'NULL': " + aLine);
		}
			
		return words.toArray(new String[words.size()]);
	}

	@Override
	public boolean hasNext() {
		return scanner.hasNext();
	}

	@Override
	public void close() {
		
	}

	@Override
	public String getLineNumber() {
		return "" + lineNumber;
	}
}
