package ca.concordia.clac.parallel.corpus.stats;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableEntry;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableReader;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableTextFilter;


class Collector implements Predicate<PhraseTableEntry>{
	List<PhraseTableEntry> entries = new ArrayList<>();
	
	@Override
	public boolean test(PhraseTableEntry entry) {
		entries.add(entry);
		return false;
	}
	
	public List<PhraseTableEntry> getEntries() {
		return entries;
	}
	
}

/**
 * select top entries of phrase table 
 * @author majid
 *
 */
public class PhraseTableSearcher {
	private File phraseTableFile;
	
	public PhraseTableSearcher(File phraseTableFile) {
		this.phraseTableFile = phraseTableFile;
	}

	public List<PhraseTableEntry> query(String frPhrase) throws IOException{
		PhraseTableReader reader = new PhraseTableReader();
		Collector collector = new Collector();
		reader.setFilter(new PhraseTableTextFilter((en, fr) -> frPhrase.equals(fr), null).and(collector));
		reader.parse(phraseTableFile);
		List<PhraseTableEntry> entries = collector.getEntries();
		Collections.sort(entries, (e1, e2) -> new Double(e2.getEnEntry().getProbGivenSource()).compareTo(e1.getEnEntry().getProbGivenSource()));
		return entries;
	}
	
	
	public static void main(String[] args) throws IOException {
		File phraseTableFile = new File("outputs/phrase-table/union.en");
		PhraseTableSearcher searcher = new PhraseTableSearcher(phraseTableFile);
		List<PhraseTableEntry> entries = searcher.query("alors");
		String topThree = entries.subList(0, 3).stream().map(PhraseTableEntry::toString).collect(Collectors.joining("\n"));
		
		System.out.println(topThree);
	}
}
