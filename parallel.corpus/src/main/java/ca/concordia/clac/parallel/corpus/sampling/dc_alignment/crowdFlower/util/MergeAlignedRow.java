package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;
import ca.concordia.clac.util.csv.MergeStrategy;
import ca.concordia.clac.util.csv.strategy.merge.PriorityMerger;

public class MergeAlignedRow {
	private MergeStrategy mergeStrategy = new PriorityMerger(0);

	private List<Pair<Integer, Integer>> getAlignmentIntervals(CSVContent csvContent, String columnName) {
		List<List<AlignmentFeature>> alignments = csvContent.getColumn(columnName).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
//			throw new RuntimeException("Can not find the boundaries of the " + Tag.tagToString(tags));

		List<Pair<Integer, Integer>> intervals = new ArrayList<>();
		for (List<AlignmentFeature> alignment: alignments){
			if (alignment.size() > 1)
				throw new RuntimeException("Multiple alignments have been found. " + AlignmentFeature.convertToString(alignment));
			if (alignment.size() == 1)
				intervals.add(new Pair<>(alignment.get(0).getBegin(), alignment.get(0).getEnd()));
			else
				intervals.add(null);
		}
		return intervals;
	}

	public CSVContent mergeAlignements(CSVContent csvContent) {
		CSVContent result = mergeTwoLang(csvContent, Tag.EN, Tag.FR);
		result = mergeTwoLang(result, Tag.FR, Tag.EN);
		
		return result;
	}

	public CSVContent mergeTwoLang(CSVContent csvContent, Tag lang1, Tag lang2) {
		List<Pair<Integer, Integer>> dcs = getIntervals(csvContent, Tag.tagToString(Tag.dc, lang1));
		List<Pair<Integer, Integer>> dcAlignments = getAlignmentIntervals(csvContent, 
				Tag.addTags(DcAlignmentsFeatures.ALIGNMENT_FEATURE_NAME, lang2, Tag.gold));
		List<Pair<Integer, Integer>> alignments = findAlignments(dcs, dcAlignments);
		return merge(csvContent, alignments);
	}

	private List<Pair<Integer, Integer>> getIntervals(CSVContent csvContent, String tags) {
		List<Integer> begins = csvContent.getColumn(Tag.addTags(tags, Tag.begin)).stream()
				.map((s) -> s.length() > 0 ? (int)Double.parseDouble(s) : null).collect(Collectors.toList());
		List<Integer> ends = csvContent.getColumn(Tag.addTags(tags, Tag.end)).stream()
				.map((s) -> s.length() > 0 ? (int)Double.parseDouble(s) : null).collect(Collectors.toList());
		
		List<Pair<Integer, Integer>> pairs = new ArrayList<>();
		for (int i = 0; i < begins.size(); i++){
			if (begins.get(i) != null)
				pairs.add(new Pair<>(begins.get(i), ends.get(i)));
			else
				pairs.add(null);
		}
		return pairs;
	}

	private CSVContent merge(CSVContent csvContent, List<Pair<Integer, Integer>> alignments) {
		List<List<String>> content = csvContent.getContent();

		List<List<String>> mergedContent = new ArrayList<>();
		Set<Integer> used = new HashSet<>();
		for (Pair<Integer, Integer> pairs: alignments){
			used.add(pairs.getFirst());
			used.add(pairs.getSecond());
			mergedContent.add(merge(content.get(pairs.getFirst()), content.get(pairs.getSecond()), csvContent.getHeader()));
		}
		
		for (int i = 0; i < content.size(); i++){
			if (!used.contains(i)){
				mergedContent.add(content.get(i));
			}
		}
		CSVContent aNewContent = new CSVContent(csvContent.getHeader(), mergedContent, "merged");
		return aNewContent;
	}

	private List<String> merge(List<String> row1, List<String> row2, List<String> headers) {
		List<String> merged = new ArrayList<>();
		for (int i = 0; i < headers.size(); i++){
			merged.add(mergeStrategy.merge(row1.get(i), row2.get(i), headers.get(i)));
		}
		return merged;
	}

	//check all pairs for intervals that are inside of each other.
	private List<Pair<Integer, Integer>> findAlignments(List<Pair<Integer, Integer>> dc,
			List<Pair<Integer, Integer>> aligned) {
		List<Pair<Integer, Integer>> alignment = new ArrayList<>();
		//keep track of aligned pairs, if we found many to one or one to many alignment, they are invalid and therefore should be removed from the pairs
		Set<Integer> alignedFirst = new HashSet<>();
		Set<Integer> alignedSecond = new HashSet<>();
		
		for (int i = 0; i < dc.size(); i++){
			for (int j = 0; j < aligned.size(); j++){
				Pair<Integer, Integer> first = dc.get(i);
				Pair<Integer, Integer> second = aligned.get(j);

				if (inside(first, second) || inside(second, first)){
					if (alignedFirst.contains(i) || alignedSecond.contains(j)){	//many to one alignments
						System.err.println("MergeAlignedRow.findAlignments(): two source/target dcs were aligned to one target/source dc. All such alignments will be removed.");
						for (Iterator<Pair<Integer, Integer>> iter = alignment.iterator(); iter.hasNext();){
							Pair<Integer, Integer> aPair = iter.next();
							if (aPair.getFirst() == i || aPair.getSecond() == j){
								iter.remove();
							}
						}
					} else {
						alignment.add(new Pair<>(i, j));
						alignedFirst.add(i);
						alignedSecond.add(j);
					}
				}
			}
		}
		return alignment;
	}

	private boolean inside(Pair<Integer, Integer> first, Pair<Integer, Integer> second) {
		if (containNull(first) || containNull(second))
			return false;

		if (second.getFirst() <= first.getFirst() && first.getFirst() <= second.getSecond()
				&& second.getFirst() <= first.getSecond() && first.getSecond() <= second.getSecond()){
			return true;
		}
		return false;
	}

	private boolean containNull(Pair<Integer, Integer> aPair){
		
		if (aPair == null || aPair.getFirst() == null || aPair.getSecond() == null)
			return true;
		return false;
	}
}
