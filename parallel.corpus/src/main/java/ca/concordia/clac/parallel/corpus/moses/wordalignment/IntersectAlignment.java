package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.util.Pair;

public class IntersectAlignment implements AlignmentReader{
	AlignmentReader first, second;

	public IntersectAlignment(AlignmentReader first, AlignmentReader second) {
		this.first = first;
		this.second = second;
	}
	
	@Override
	public List<String> getEnWords() {
		return first.getEnWords();
	}

	@Override
	public List<String> getFrWords() {
		return first.getFrWords();
	}

	@Override
	public List<Pair<Integer, Integer>> getAlignments() {
		List<Pair<Integer, Integer>> firstAlignments = first.getAlignments();
		List<Pair<Integer, Integer>> secondAlignments = second.getAlignments();
		Set<Pair<Integer, Integer>> intersection = new HashSet<>(firstAlignments);
		intersection.retainAll(secondAlignments);
		
		List<Pair<Integer, Integer>> result = new ArrayList<>(intersection);
		return result;
	}

	@Override
	public void next() {
		first.next();
		second.next();
	}

	@Override
	public boolean hasNext() {
		return first.hasNext() && second.hasNext();
	}

	@Override
	public void close() {
		first.close();
		second.close();
	}

	@Override
	public String getLineNumber() {
		return first.getLineNumber() + ":" + second.getLineNumber();
	}

}
