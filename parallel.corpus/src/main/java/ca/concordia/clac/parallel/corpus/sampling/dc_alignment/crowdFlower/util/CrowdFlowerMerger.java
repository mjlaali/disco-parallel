package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;

public class CrowdFlowerMerger {
	private static final String SEPARATOR = "/";

	private CSVContent dataset;

	public CrowdFlowerMerger(CSVContent dataset) {
		this.dataset = dataset;
	}

	public void fixAndMergeAlignments() throws IOException{
		for (Tag lang: new Tag[]{Tag.EN, Tag.FR}){
			final String alignmentColumnName = Tag.addTags(DcAlignmentsFeatures.ALIGNMENT_FEATURE_NAME, lang);
			dataset = fixAlignmentIndexes(dataset, alignmentColumnName, 
					Tag.tagToString(Tag.chunk, Tag.alignment, Tag.begin, lang),
					Tag.tagToString(Tag.chunk, Tag.alignment, Tag.superscript, lang));
			dataset = mergeAlignments(dataset, alignmentColumnName, Tag.addTags(alignmentColumnName, Tag.gold));
		}
	}

	public static CSVContent mergeAlignments(CSVContent csvContent, String alignmentColumnName, String goldAlignmentColumnName) {
		List<List<AlignmentFeature>> alignments = csvContent.getColumn(alignmentColumnName).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
		List<AlignmentFeature> mergedAlignements = alignments.stream().map(CrowdFlowerMerger::mergeAlignment).collect(Collectors.toList());
		List<String> goldAlignmentColumn = mergedAlignements.stream().map((a) -> a == null ? "" : a.toString()).collect(Collectors.toList());

		long cnt = goldAlignmentColumn.stream().filter(String::isEmpty).count();
		System.out.println("Added annotations: " + (goldAlignmentColumn.size() - cnt));
		if (csvContent.getHeader().contains(goldAlignmentColumnName)){
			List<String> toMerge = csvContent.getColumn(goldAlignmentColumnName);
			for (int i = 0; i < toMerge.size(); i++){
				if (!toMerge.get(i).isEmpty()){
					goldAlignmentColumn.set(i, toMerge.get(i));
				}
			}
			csvContent.deleteColumn(goldAlignmentColumnName);
		}
		csvContent.addColumn(goldAlignmentColumnName, 0, goldAlignmentColumn);
		return csvContent;
	}

	private static AlignmentFeature mergeAlignment(List<AlignmentFeature> alignmetns){
		//sort alignment left to right, biggest to smallest
		Collections.sort(alignmetns, (a, b) -> {
			int compareTo = new Integer(a.getBegin()).compareTo(b.getBegin());
			if (compareTo == 0){
				compareTo = new Integer(b.getEnd()).compareTo(a.getEnd());
			}
			return compareTo;
		});

		List<AlignmentFeature> alignmentScore = new ArrayList<>();
		AlignmentFeature activeAlignemnt = null;
		for (AlignmentFeature alignment: alignmetns){
			if (alignment.getScore() <= 0)	//this is automatically added alignment
				continue;

			double score = alignment.getScore();
			alignment.setName(alignment.getName().replaceAll(":.*", ""));
			alignment.setScore(score);

			if (activeAlignemnt == null || alignment.getBegin() > activeAlignemnt.getEnd()){	//move to the next alignemtn
				activeAlignemnt = alignment;
				alignmentScore.add(activeAlignemnt);
			} else { //this alignment is inside the active alignment;
				activeAlignemnt.setScore(getTrust(activeAlignemnt.getScore(), alignment.getScore()));
				activeAlignemnt.setName(activeAlignemnt.getName() + SEPARATOR + alignment.getName());
			}
		}

		AlignmentFeature bestOne = null;
		for (AlignmentFeature anAlignment: alignmentScore){
			if (bestOne == null || bestOne.getScore() < anAlignment.getScore()){
				bestOne = anAlignment;
			}
		}

		if (bestOne == null || bestOne.getScore() == 0)
			return null;
		return bestOne;
	}
	
	private static double getTrust(double t1, double t2){
		if (t1 == 1)
			t1 = 0.99;
		if (t2 == 1);
			t2 = 0.99;
		
		return (t1 * t2) / ((t1 * t2 ) + (1 - t1) * (1 - t2));
	}

	public static CSVContent fixAlignmentIndexes(CSVContent csvContent, String alignmentColumn, String beginColumn, String subscriptColumn) {
		List<List<AlignmentFeature>> alignmentsForDCs = csvContent.getColumn(alignmentColumn).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
		List<Integer> begins = csvContent.getColumn(beginColumn).stream()
				.map((s) -> s.length() > 0 ? (int)Double.parseDouble(s) : null).collect(Collectors.toList());
		List<String> subscriptedTexts = csvContent.getColumn(subscriptColumn);

		for (int i = 0; i < alignmentsForDCs.size(); i++){
			List<AlignmentFeature> alignments = alignmentsForDCs.get(i);
			String subscriptedText = subscriptedTexts.get(i).toLowerCase();
			Integer begin = begins.get(i);
			for (AlignmentFeature alignment: alignments){
				if (alignment.getBegin() == -1 && alignment.getScore() >= 0){	//automatic alignments have score 0.0
					String toSearch = alignment.getText().replaceAll("\\d+", "<sub>$0</sub>").toLowerCase();

					String text = alignment.getText().replaceAll("\\d+", "");

					if (toSearch.isEmpty() || toSearch.equals(".")){
						alignment.setText("");
					} else {
						updateTheAlignment(subscriptedText, begin, alignment, toSearch, text);
					}

					String name = alignment.getName();
					
					if (name.contains(":")){
						name = name.substring(0, name.indexOf(':'));
						alignment.setName(name);
					}
					
				}
			}
		}

		List<String> newValues = alignmentsForDCs.stream().map(AlignmentFeature::convertToString).collect(Collectors.toList());
		csvContent.setColumn(alignmentColumn, newValues);
		return csvContent;
	}

	private static void updateTheAlignment(String subscriptedText, Integer chunkBegin, AlignmentFeature alignment,
			String toSearch, String text) {
		int idx = -1;
		if (Character.isAlphabetic(toSearch.charAt(0))){
			toSearch = "\\b" + toSearch;
		}
		if (Character.isAlphabetic(toSearch.charAt(toSearch.length() - 1))){
			toSearch = toSearch + "\\b";
		}
		Pattern pattern = Pattern.compile(toSearch);
		Matcher matcher = pattern.matcher(subscriptedText);
		if (matcher.find()){
			idx = subscriptedText.substring(0, matcher.start()).replaceAll("<sub>\\d+</sub>", "").length();
		}
		if (idx == -1){	//there is something wrong, lets try to fix it.
			String rowText = subscriptedText.replaceAll("<sub>\\d+</sub>", "");
			int start = 0;
			int next = -1;
			text = text.replaceAll("\\s+", " ");
			while ((next = rowText.indexOf(text, start)) != -1){
				if (idx == -1){
					idx = next;
					start = next + text.length();
				} else {	 //there are two matches we need to manually check this.
					throw new RuntimeException("Multiple matches for :\n" + alignment.getText() + "\n in:\n" + subscriptedText + "\n" );
				}
			}
			if (idx == -1)	//if automatic check does not work.
				throw new RuntimeException("Cannot find the French dc:\n" + toSearch + "\n in:\n" + subscriptedText + "\n" + alignment.getText() );
		}
		alignment.setText(text.replaceAll("\\d+", ""));
		alignment.setBegin(chunkBegin + idx);
		alignment.setEnd(chunkBegin + idx + text.length());
	}

	public CSVContent getDataset() {
		return dataset;
	}


	public static void main(String[] args) throws IOException {
		File datasetFile = new File("outputs/crowdFlower/newapproach/csv/dataset.csv");
		File outputFile = new File("outputs/crowdFlower/newapproach/dataset-crowdFlower.csv");
		String[] crowdFlowerAnnotaitons = new String[]{
				"10dollar/leila-test-questions.csv", 
				"10dollar/andres-test-questions.csv", 
				"building-test-questions/Felix.csv",
				"building-test-questions/Leila.csv", 
				"building-test-questions/Alexis.csv",
				"building-test-questions/Andre.csv",
		};

		CSVContent dataset = new CSVContent(datasetFile);
		for (String aFile: crowdFlowerAnnotaitons){
			File csvFile = new File(new File("resources/crowdFlower"), aFile);
			int start = aFile.lastIndexOf('/') + 1;
			int end = aFile.indexOf('-', start);
			if (end == -1)
				end = aFile.indexOf('.', start);
			String name = aFile.substring(start, end).toLowerCase();
			dataset = new CrowdFlowerToAlignmentFeature(new CSVContent(csvFile), name, "french_marker", "en", Tag.EN)
					.mergeCrowdAnnotations(dataset);
		}

		CrowdFlowerMerger merger = new CrowdFlowerMerger(dataset);
		merger.fixAndMergeAlignments();
		dataset = merger.getDataset();
		dataset = new MergeAlignedRow().mergeAlignements(dataset);
		List<String> column = dataset.getColumn("EN.dc.alignment.gold");
		long cnt = column.stream().filter(String::isEmpty).count();
		System.out.println("After merged crowd flower annoations: " + (column.size() - cnt));
		System.out.println("Dataset size: " + dataset.getContent().size());
		dataset.save(outputFile);
	}
}
