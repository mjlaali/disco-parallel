package ca.concordia.clac.parallel.corpus.evaluation.lexicon;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.math3.util.Pair;
import org.xml.sax.SAXException;

import com.google.common.collect.SetMultimap;

import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.builder.LexconnBuilder;
import ca.concordia.clac.parallel.corpus.reader.UniqFreqReader;
import ca.concordia.clac.parallel.corpus.writer.LexconnEntry;

public class Loader {
	
	DefaultLexconnReader lexconnReader;
	public Loader(DefaultLexconnReader lexconnReader) {
		this.lexconnReader = lexconnReader;
	}

	public Map<Pair<String, String>, Double> loadLexconn(){
		SetMultimap<String, String> idToRelations = lexconnReader.getIdToRelations();
		Map<Pair<String, String>, Double> lexconn = new HashMap<>();
		for (String id: idToRelations.keys()){
			for (String rel: idToRelations.get(id))
				lexconn.put(new Pair<String, String>(id, rel.toLowerCase()), 1.0);
		}

		return lexconn;
	}

	public Map<Pair<String, String>, Double> loadFromPhraseTable(String phraseTableName) throws ParserConfigurationException, SAXException, IOException, JAXBException{
		List<LexconnEntry> extracted = LexconnBuilder.buildLexicon(phraseTableName, lexconnReader);
		Map<Pair<String, String>, Double> extractedLexicon = new HashMap<>();
		for (LexconnEntry anEntry: extracted){
			String id = anEntry.getId();
			for (Entry<String, Double> pdtbRelProb: anEntry.getRelation_probabilities().entrySet()){
				extractedLexicon.put(new Pair<>(id, pdtbRelProb.getKey()), pdtbRelProb.getValue());
			}
		}

		return extractedLexicon;
	}

	public Map<String, Integer> loadCounts(File frDcFreq) throws IOException{
		Map<String, String> formToId = lexconnReader.getFormToId();
		Map<String, Integer> counts = new HashMap<>();
		new UniqFreqReader(frDcFreq).getTermCount().forEach((dc, cnt) -> {
			String normalized = formToId.get(dc);
			Integer oldCnt = counts.get(normalized);
			if (oldCnt == null)
				oldCnt = 0;
			counts.put(normalized, oldCnt + cnt);
		});
		
		return counts;
	}
	
	public DefaultLexconnReader getLexconnReader() {
		return lexconnReader;
	}
	
}
