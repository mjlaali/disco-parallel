package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.math3.util.Pair;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;
import ca.concordia.clac.parallel.corpus.reader.UniqFreqReader;



public class MapperToString {
	private double threshold;
	int maxRel;
	private SetMultimap<String, Pair<String, Double>> mapping;
	private SetMultimap<String, String> manualMapping = HashMultimap.create();
	private Map<String, Integer> pdtbRelCnts = new HashMap<>();

	public MapperToString(double threshold, int maxRel, SetMultimap<String, Pair<String, Double>> mapping) {
		this.threshold = threshold;
		this.mapping = mapping;
		this.maxRel = maxRel;
		
	}
	
	public int loadAMapping(File file) throws IOException{
		UniqFreqReader reader = new UniqFreqReader(new File("resources/sense.freq"));
		Map<String, Integer> termCount = reader.getTermCount();
		termCount.forEach((r, f) -> pdtbRelCnts.put(SenseRemoval.relationNormalizer.apply(r), f));
		
		Reader fileReader = new FileReader(file); 
		CSVFormat csvFileFormat = CSVFormat.DEFAULT;

		@SuppressWarnings("resource")
		CSVParser csvFileParser = new CSVParser(fileReader, csvFileFormat);
		List<CSVRecord> csvRecords  = csvFileParser.getRecords();
		int lexconnRel = -1;
		int conllRel = -1;
		for (CSVRecord record: csvRecords){
			if (lexconnRel == -1){
				for (int i = 0; i < record.size(); i++){
					if (record.get(i).equals("LEXCONN")){
						lexconnRel = i;
					} else if (record.get(i).equals("CoNLL")){
						conllRel = i;
					}
				}
			} else {
				manualMapping.put(SenseRemoval.relationNormalizer.apply(record.get(conllRel)), SenseRemoval.relationNormalizer.apply(record.get(lexconnRel)));
			}
		}
		
		return manualMapping.size();
		
	}
	
	@Override
	public String toString() {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		PrintWriter pw = new PrintWriter(output);
		DecimalFormat df = new DecimalFormat("0.00");
		double verifiedProbmass = 0;
		double notVerifiedProbmass = 0;

		for (String pdtbRelation: PythonMappingReader.PDTB_RELATIONS){
			String[] tokens = pdtbRelation.split("[.]");
			for (int i = 0; i < 3; i++){
				if (i != 0)
					pw.print(",");
				if (i < tokens.length)
					pw.print(tokens[i]);
				if (i == 1 && tokens.length >= 3){
					pw.print(".");
				}
			}
			Integer pdtbRelCnt = pdtbRelCnts.get(pdtbRelation);
			if (pdtbRelCnt != null){
				pw.print(",");
				pw.print((int)(pdtbRelCnt / 1000.0 + 0.5));
				pw.print('K');
			}
			
			List<Pair<String, Double>> mappedRelations = new ArrayList<>(mapping.get(pdtbRelation));
			Collections.sort(mappedRelations, (a, b) -> b.getSecond().compareTo(a.getSecond()));
			double totalProb = 0;
			int cnt = 0;
			Set<String> lexconnRelations = manualMapping.get(pdtbRelation);
			for (Pair<String, Double> aMap: mappedRelations){
				String lexconnRelation = SenseRemoval.relationNormalizer.apply(aMap.getFirst());
				String color;
				if (lexconnRelations.contains(lexconnRelation)){
					color = "black";
					verifiedProbmass += aMap.getSecond();
				} else {
					color = "gray";
					notVerifiedProbmass += aMap.getSecond();
				}
				pw.print(",(" + df.format(aMap.getSecond()) + ")," + color + "," + lexconnRelation);
				totalProb += aMap.getSecond();
				++cnt;
				if (totalProb > threshold || cnt >= maxRel)
					break;
			}
			for (int i = cnt; i < maxRel; i++)
				pw.print(",,,");
			pw.println();
		}
		pw.println("Verified mapping prob mass: " + df.format(verifiedProbmass) + ", " + df.format(verifiedProbmass/PythonMappingReader.PDTB_RELATIONS.size())+ 
				", not verified = " + notVerifiedProbmass+ ", " + df.format(notVerifiedProbmass/PythonMappingReader.PDTB_RELATIONS.size()));
		pw.close();
			
		return new String(output.toByteArray());
	}
	
}
