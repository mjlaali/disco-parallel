package ca.concordia.clac.parallel.corpus;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.ParalleDocumentTextReader;
import org.cleartk.corpus.europarl.ParallelFileCollectionReader;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.batch_process.BatchProcess;
import ca.concordia.clac.discourse.FrConnectiveClassifier;
import ca.concordia.clac.parser.evaluation.CLaCParser;
import de.tudarmstadt.ukp.dkpro.core.berkeleyparser.BerkeleyParser;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;

public class ParallelCorpusAnnotator{
	public static final String STEP_LOADING = "loading";
	public static final String STEP_PARSING_EN = "parsing-en";
	public static final String STEP_PARSING_FR = "parsing-fr";
	public static final String STEP_DISCOURSEPARSING_EN = "discourseParsing-en";
	public static final String STEP_DISCOURSEPARSING_FR = "discourseParsing-fr";
	
	public static final String EXTRACTED_FRENCH_DATASET_FOLDER = "extractedFrenchDataset";
	
	public BatchProcess getBatchProcess(File inputDir, File outputDir) throws ResourceInitializationException, MalformedURLException, URISyntaxException{
		BatchProcess batchProcess = new BatchProcess(
				ParallelFileCollectionReader.getReaderDescription(inputDir, "en", "fr"), outputDir);

		batchProcess.addProcess(STEP_LOADING, 
				ParalleDocumentTextReader.getDescription(), 
				EuroparlParallelTextAnnotator.getDescription());

		batchProcess.addProcess(STEP_PARSING_EN, EuroparlParallelTextAnnotator.EN_TEXT_VIEW, 
				createEngineDescription(OpenNlpSegmenter.class),
				createEngineDescription(OpenNlpPosTagger.class),
				createEngineDescription(BerkeleyParser.class, 
						BerkeleyParser.PARAM_MODEL_LOCATION, "classpath:/eng_sm5.gr"));

		batchProcess.addProcess(STEP_PARSING_FR, EuroparlParallelTextAnnotator.FR_TEXT_VIEW, 
				createEngineDescription(OpenNlpSegmenter.class,
						OpenNlpSegmenter.PARAM_LANGUAGE, "fr", 
						OpenNlpSegmenter.PARAM_SEGMENTATION_MODEL_LOCATION, "classpath:/fr-sent.bin", 
						OpenNlpSegmenter.PARAM_TOKENIZATION_MODEL_LOCATION, "classpath:/fr-token.bin"
						),
				createEngineDescription(BerkeleyParser.class, 
						BerkeleyParser.PARAM_LANGUAGE, "fr", 
						BerkeleyParser.PARAM_MODEL_LOCATION, "classpath:/fra_sm5.gr", 
						BerkeleyParser.PARAM_WRITE_POS, true, 
						BerkeleyParser.PARAM_READ_POS, false)
				);
		
		batchProcess.addProcess(STEP_DISCOURSEPARSING_EN, EuroparlParallelTextAnnotator.EN_TEXT_VIEW,  
				new CLaCParser().getParser());
		
		batchProcess.addProcess(STEP_DISCOURSEPARSING_FR, EuroparlParallelTextAnnotator.FR_TEXT_VIEW,  
				FrConnectiveClassifier.getClassifierDescription()
				);
		return batchProcess;
	}


	
	public interface Options{
		@Option(
				defaultToNull=true,
				shortName = "i",
				longName = "inputDataset", 
				description = "Specify the input dataset for the parser. If you choose CoNLL type,\n\t"
						+ "the input dataset should be a folder with the same format as CoNLL dataset,\n\t"
						+ "otherwise it should be a simple text")
		public String getInputDataset();

		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory of the parser.")
		public String getOutputDir();
		
		@Option(
				pattern = "\\d+",
				shortName = "t",
				longName = "treadCnt",
				description = "Specify the number of thread.")
		public String getThreadCount();
	}

	public static void main(String[] args) throws UIMAException, IOException, URISyntaxException, ClassNotFoundException, SAXException, CpeDescriptorException {
		Options options = CliFactory.parseArguments(Options.class, args);

		File outputDir = new File(options.getOutputDir());
		if (!outputDir.exists())
			outputDir.mkdirs();

		BatchProcess batchProcess;
		if (options.getInputDataset() != null){
			File inputDir = new File(options.getInputDataset());
			batchProcess = new ParallelCorpusAnnotator().
				getBatchProcess(inputDir, outputDir);
		} else
			batchProcess = BatchProcess.load(outputDir);
		
		batchProcess.setThreadCount(Integer.parseInt(options.getThreadCount()));
		batchProcess.run();
		batchProcess.save();
	}

}
