package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import java.util.function.Function;

public class SpaceRemover implements Function<String, String>{

	public static String removeSpace(String aText){
		return aText.replaceAll("[ ]", "");
	}
	
	@Override
	public String apply(String t) {
		return removeSpace(t);
	}
}
