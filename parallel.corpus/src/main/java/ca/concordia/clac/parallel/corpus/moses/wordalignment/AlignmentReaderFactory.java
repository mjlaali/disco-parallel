package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.io.File;
import java.io.FileNotFoundException;

public class AlignmentReaderFactory {

	public static AlignmentReader get(File alignmentFile) throws FileNotFoundException {
		String name = alignmentFile.getName();
		if (name.contains("grow-diag-final") || !alignmentFile.exists()){
			String fullPath = alignmentFile.getAbsolutePath();
			File baseFld = new File(fullPath.substring(0, fullPath.lastIndexOf('/')));
			if (name.contains("grow-diag-final")) {
				File ibmAlignmentFile = new File(baseFld, "en-fr.A3.final");
				IBMAlignmentReader reader = new IBMAlignmentReader(ibmAlignmentFile);
				return new OchAlignmentReader(alignmentFile, reader);
			} else {
				return new IntersectAlignment(
						new IBMAlignmentReader(new File(baseFld, "en-fr." + name)), 
						new IBMAlignmentReader(new File(baseFld, "fr-en." + name)) 
						);
			}
		} else if (name.contains("A3.")) {
			return new IBMAlignmentReader(alignmentFile);
		}
		return null;
	}

}
