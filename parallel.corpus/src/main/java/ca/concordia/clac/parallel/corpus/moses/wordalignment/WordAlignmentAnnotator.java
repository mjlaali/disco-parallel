package ca.concordia.clac.parallel.corpus.moses.wordalignment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;
import org.apache.uima.UIMAException;
import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;

import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;

public class WordAlignmentAnnotator extends JCasAnnotator_ImplBase{
	public static final String ALIGNMENT_COMMENT = "Word-Alignment";
	public static final String PARAM_RETAIN_LINES_FILE = "retainLinesFile";
	public static final String PARAM_ALIGNMENT_FILE = "alignmentFile";
	
	@ConfigurationParameter(name=PARAM_RETAIN_LINES_FILE)
	File retainLinesFile;
	
	@ConfigurationParameter(name=PARAM_ALIGNMENT_FILE)
	File alignmentFile;
	
	Map<ParallelChunk, Collection<Token>> enChunkToTokens;
	Map<ParallelChunk, Collection<Token>> frChunkToTokens;
	AlignmentReader reader;
	
	Scanner scanner;
	
	JCas enView, frView;
	int lineIdx = 0;
	int nextValidLine = -1;
	
	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		super.initialize(context);
		
		try {
			getLogger().info("Start loading retained File");
			
			scanner = new Scanner(new BufferedReader(new FileReader(retainLinesFile)));
			nextValidLine = scanner.nextInt();
			
			reader = AlignmentReaderFactory.get(alignmentFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		try {
			enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
			frView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
			
			enChunkToTokens = JCasUtil.indexCovered(enView, ParallelChunk.class, Token.class);
			frChunkToTokens = JCasUtil.indexCovered(frView, ParallelChunk.class, Token.class);
			
			Collection<ParallelChunk> enChunks = JCasUtil.select(enView, ParallelChunk.class);
			
			for (ParallelChunk aChunk: enChunks){
				lineIdx++;
				if (lineIdx != nextValidLine)
					continue;
				else if (scanner.hasNextInt())
					nextValidLine = scanner.nextInt();
				
				align(aChunk, aChunk.getTranslation());
			}
			
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void align(ParallelChunk en, ParallelChunk fr) {
		reader.next();
		List<String> mosesEnWords = reader.getEnWords();
		List<String> mosesFrWords = reader.getFrWords();
		List<Token> uimaEnWords = (List)enChunkToTokens.get(en); 
		List<Token> uimaFrWords = (List)frChunkToTokens.get(fr);; 
		Map<Integer, Integer> mosesToUiMaForEn = map(mosesEnWords, uimaEnWords);
		Map<Integer, Integer> mosesToUiMaForFr = map(mosesFrWords, uimaFrWords);
		
		List<Pair<Integer, Integer>> alignments = reader.getAlignments();
		
		alignments = map(alignments, mosesToUiMaForEn, mosesToUiMaForFr);
		
		buildAnnotaiton(alignments, uimaEnWords, uimaFrWords);
	}

	static Map<Integer, Integer> map(List<String> mosesWords, List<Token> uimaWords) {
		Map<Integer, Integer> map = new HashMap<>();
		if (uimaWords.size() == 0)
			return map;
		List<String> uimaStrWords = uimaWords.stream().map(Token::getCoveredText).map(String::toLowerCase).collect(Collectors.toList());
		mosesWords = mosesWords.stream().map(String::toLowerCase).collect(Collectors.toList());
		int currentUimaIdx = 0;
		int currentMosesIdx = 0;
		String currentUimaWord = uimaStrWords.get(0);
		for (String currentMosesWord: mosesWords){
			if (currentMosesWord.length() == 0){
				System.out.println("WordAlignmentAnnotator.map(): This seems an error, an empty word in the output of moses! " + mosesWords.toString());
			} else {
				while (currentUimaWord.length() == 0){
					currentUimaIdx++;
					if (currentUimaIdx < uimaStrWords.size()){
						currentUimaWord = uimaStrWords.get(currentUimaIdx); 
					}
				}

				if (currentUimaWord.startsWith(currentMosesWord)) { // a moses word might be a part of uima word
					map.put(currentMosesIdx, currentUimaIdx);
					currentUimaWord = currentUimaWord.substring(currentMosesWord.length());
				} else {
					System.out.println("WordAlignmentAnnotator.map(): cannot aling <" + currentMosesWord + "> in: " + mosesWords + "\n" + uimaStrWords);
					map.put(currentMosesIdx, currentUimaIdx);
				}
			}
			currentMosesIdx++;
		}
		return map;
	}

	private void buildAnnotaiton(List<Pair<Integer, Integer>> alignments, List<Token> tokenEnWords,
			List<Token> tokenFrWords) {
//		System.err.println(tokenEnWords.size() + ", " + tokenFrWords.size());
//		System.err.println(alignments);
		for (Pair<Integer, Integer> anAlignment: alignments){
			Token enToken = tokenEnWords.get(anAlignment.getFirst());
			Token frToken = tokenFrWords.get(anAlignment.getSecond());
			Alignment enAlignment = initAlignment(enToken, enView);
			Alignment frAlignment = initAlignment(frToken, frView);
			enAlignment.setAlignment(frAlignment);
			frAlignment.setAlignment(enAlignment);
		}
	}

	private Alignment initAlignment(Token token, JCas view) {
		Alignment alignment = new Alignment(view, token);
		alignment.setComment(ALIGNMENT_COMMENT);
		alignment.addToIndexes();
		return alignment;
	}

	static List<Pair<Integer, Integer>> map(List<Pair<Integer, Integer>> alignments,
			Map<Integer, Integer> mosesToUimaForEn, Map<Integer, Integer> mosesToUimaForFr) {
		List<Pair<Integer, Integer>> mappedAlignments = new ArrayList<>();
		
		for (Pair<Integer, Integer> anAlignment: alignments){
			Integer first = mosesToUimaForEn.get(anAlignment.getFirst());
			Integer second = mosesToUimaForFr.get(anAlignment.getSecond());
			if (first == null || second == null){
				throw new RuntimeException("Something is wrong! check the mapping between moses and uima words");
			}
			mappedAlignments.add(new Pair<>(first, second));
		}
		return mappedAlignments;
	}

	@Override
	public void collectionProcessComplete() throws AnalysisEngineProcessException {
		super.collectionProcessComplete();
		reader.close();
		scanner.close();
	}

	public static AnalysisEngineDescription getDescription(File alignmentFile, File retainLinesFile) throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(WordAlignmentAnnotator.class, 
				PARAM_ALIGNMENT_FILE, alignmentFile.getAbsolutePath(),
				PARAM_RETAIN_LINES_FILE, retainLinesFile.getAbsolutePath());
	}
	
	public static void main(String[] args) throws UIMAException, IOException {
		File datasetFld = new File("resources/test/small-sample/");
		File outputFld = new File("resources/test/small-sample-word-aligned/");
		File baseFld = new File("/Users/majid/Documents/resource/europarl_en_fr_tokenized/");
		File[] alignmentFiles = new File[]{
			new File(baseFld, "fr-en.A3.final"),
			new File(baseFld, "aligned.1.grow-diag-final-and.fr-en"), 
			new File(baseFld, "en-fr.A3.final"),
			new File(baseFld, "A3.final")
		};
		File retainLinesFile = new File("/Users/majid/Documents/resource/europarl_en_fr_tokenized/europarl.clean.1.lines-retained");
		
		
		for (File alignmentFile: alignmentFiles){
			System.err.println("WordAlignmentAnnotator.main(): " + alignmentFile.getAbsolutePath());
			CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
					XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
					XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
			
			AnalysisEngineDescription writer = AnalysisEngineFactory.createEngineDescription(XmiWriter.class, 
					XmiWriter.PARAM_TARGET_LOCATION, new File(outputFld, alignmentFile.getName()));
			
			SimplePipeline.runPipeline(reader, WordAlignmentAnnotator.getDescription(alignmentFile, retainLinesFile), writer);
		}
	}
}
