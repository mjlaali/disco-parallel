package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import ca.concordia.clac.util.csv.MergeStrategy;

public class AlignmentFeature {
	private String text;
	private String name;
	private double score;
	private int begin;
	private int end;
	
	public AlignmentFeature() {
	}
	
	public AlignmentFeature(String line) {
		init(line);
	}
	
	public AlignmentFeature(String text, String name, int begin, int end, double score) {
		super();
		this.text = text;
		this.name = name;
		this.begin = begin;
		this.end = end;
		this.score = score;
	}
	
	public void init(String values){
		String[] tokens = values.split("\t");
		
		if (tokens.length != 5)
			throw new RuntimeException(tokens.length + ": <" + values + "> does not contain all the required information.");

		this.text = tokens[0];
		this.name = tokens[1];
		this.begin = Integer.parseInt(tokens[2]);
		this.end = Integer.parseInt(tokens[3]);
		this.score = Double.parseDouble(tokens[4]);

	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public int getBegin() {
		return begin;
	}

	public void setBegin(int begin) {
		this.begin = begin;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}
	
	@Override
	public String toString() {
		return text + "\t" + name + "\t" + begin + "\t" + end + "\t" + score;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	public static List<AlignmentFeature> parse(String alignmentLines){
		if (alignmentLines.trim().length() == 0){
			return new ArrayList<>();
		}
		String[] lines = alignmentLines.split("\n");
		return new ArrayList<>(Stream.of(lines).map((l) -> new AlignmentFeature(l)).collect(Collectors.toList()));
	}
	
	public static String convertToString(AlignmentFeature... alignments){
		return convertToString(Arrays.asList(alignments));
	}
	public static String convertToString(List<AlignmentFeature> alignments){
		return alignments.stream().map((a) -> a.toString()).collect(Collectors.joining("\n"));
	}
	
	public static MergeStrategy getMergeStrategy(String alignmentColumnName){
		return (a, b, k) ->{
			if (k.equals(alignmentColumnName)){
				List<AlignmentFeature> alignments = AlignmentFeature.parse(a);
				alignments.addAll(AlignmentFeature.parse(b));
				
				return AlignmentFeature.convertToString(alignments);
			}
			return null;
		};
	}
}
