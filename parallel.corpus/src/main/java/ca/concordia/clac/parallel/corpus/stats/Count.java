package ca.concordia.clac.parallel.corpus.stats;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.function.Function;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.weka.WekaStringOutcomeDataWriter;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.batch_process.BatchProcess;
import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseVsNonDiscourseClassifier;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import ca.concordia.clac.ml.classifier.StringClassifierLabeller;
import ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator;
import ca.concordia.clac.parallel.corpus.builder.LabelExtractor;
import ca.concordia.clac.uima.engines.LookupInstanceExtractor;

public class Count extends DiscourseVsNonDiscourseClassifier {
	@Override
	public InstanceExtractor<DiscourseConnective> getExtractor(JCas aJCas) {
		final InstanceExtractor<DiscourseConnective> extractor = super.getExtractor(aJCas);
		return (jCas) -> {
			try {
				Collection<DiscourseConnective> instances = 
						extractor.getInstances(jCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW));
				instances.forEach(DiscourseConnective::addToIndexes);
				return instances;
			} catch (CASException e) {
				throw new RuntimeException(e);
			}
		};
	}

	@Override
	public Function<DiscourseConnective, String> getLabelExtractor(JCas aJCas) {
		
		return new LabelExtractor(aJCas);
	}

	public static AnalysisEngineDescription getWriterDescription(URL dcList, File outputFld) throws ResourceInitializationException, MalformedURLException, URISyntaxException{
		return StringClassifierLabeller.getWriterDescription(
				Count.class,
				WekaStringOutcomeDataWriter.class, 
				outputFld, 
				LookupInstanceExtractor.PARAM_LOOKUP_FILE_URL, dcList,
				LookupInstanceExtractor.PARAM_ANNOTATION_FACTORY_CLASS_NAME, DiscourseAnnotationFactory.class.getName()
				);	

	}


	public interface Options{
		@Option(
				defaultToNull=true,
				shortName = "i",
				longName = "inputDataset", 
				description = "Specify the input dataset for the parser. If you choose CoNLL type,\n\t"
						+ "the input dataset should be a folder with the same format as CoNLL dataset,\n\t"
						+ "otherwise it should be a simple text")
		public String getInputDataset();

		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory of the parser")
		public String getOutputDir();
	}

	public static void main(String[] args) throws UIMAException, IOException{
		Options options = CliFactory.parseArguments(Options.class, args);

		File outputDir = new File(options.getOutputDir());
		if (!outputDir.exists())
			outputDir.mkdirs();

		BatchProcess batchProcess = null;
		try {
			if (options.getInputDataset() != null){
				File inputDir = new File(options.getInputDataset());
				batchProcess = new ParallelCorpusAnnotator().
						getBatchProcess(inputDir, outputDir);
				File dcList = new File("resources/french-dc.txt");
				File outputFld = new File("outputs/stats");
				
				batchProcess.addProcess("count", Count.getWriterDescription(dcList.toURI().toURL(), outputFld));
			} else
				batchProcess = BatchProcess.load(outputDir);
			batchProcess.run();
		} catch (URISyntaxException | ClassNotFoundException | SAXException | CpeDescriptorException e) {
			throw new RuntimeException(e);
		}

		batchProcess.save();
	}
}
