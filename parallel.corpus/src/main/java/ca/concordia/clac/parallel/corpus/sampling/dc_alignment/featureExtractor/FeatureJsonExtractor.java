package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.initializable.InitializableFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.ml.classifier.ClassifierAlgorithmFactory;
import ca.concordia.clac.ml.classifier.GenericClassifierLabeller;
import ca.concordia.clac.ml.classifier.InstanceExtractor;


/**
 * The input is xmi where DCs (either En or Fr) were annotated and the output is a CSV file where
 * features are extracted for each DC using {@link #PARAM_FEATURE_EXTRACTOR_CLS_NAME}  
 * @author majid
 *
 */
public class FeatureJsonExtractor implements ClassifierAlgorithmFactory<String, DiscourseConnective>{
	public static final String PARAM_FEATURE_EXTRACTOR_CLS_NAME = "featureExtractorName"; 
			
	@ConfigurationParameter(name = PARAM_FEATURE_EXTRACTOR_CLS_NAME, mandatory = true)
	private String featureExtractorName;
	private Function<DiscourseConnective, List<Feature>> featureExtractor;
	
	@SuppressWarnings("unchecked")
	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		ClassifierAlgorithmFactory.super.initialize(context);
		featureExtractor = InitializableFactory.create(context, featureExtractorName, Function.class);
	}

	@Override
	public InstanceExtractor<DiscourseConnective> getExtractor(JCas jCas) {
		return (aJCas) -> JCasUtil.select(aJCas, DiscourseConnective.class);
	}

	@Override
	public List<Function<DiscourseConnective, List<Feature>>> getFeatureExtractor(JCas jCas) {
		if (featureExtractor instanceof JCasInitializable){
			((JCasInitializable)featureExtractor).init(jCas);
		}
		return Arrays.asList(featureExtractor);
	}

	@Override
	public Function<DiscourseConnective, String> getLabelExtractor(JCas jCas) {
		return (p) -> "label";
	}

	@Override
	public BiConsumer<String, DiscourseConnective> getLabeller(JCas jCas) {
		return null;
	}
	
	public static AnalysisEngineDescription getDescription(File outputDir, Class<? extends Function<DiscourseConnective, List<Feature>>> cls, Object... otherParams) throws ResourceInitializationException{
		Object[] params = GenericClassifierLabeller.mergeParams(otherParams, FeatureJsonExtractor.PARAM_FEATURE_EXTRACTOR_CLS_NAME, cls.getName()); 
			return GenericClassifierLabeller.getWriterDescription(FeatureJsonExtractor.class, JSonDataWriter.class, outputDir, 
				params);
	}


}
