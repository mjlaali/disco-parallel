package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import static ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag.parse;
import static ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag.tagToString;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.uima.UIMAException;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.json.FeatureJSonConverter;
import ca.concordia.clac.json.FeatureToCSVConverter;
import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.uima.Utils;
import ca.concordia.clac.uima.engines.LookupInstanceAnnotator;
import ca.concordia.clac.util.csv.CSVContent;
import ca.concordia.clac.util.csv.CSVMerger;
import ca.concordia.clac.util.csv.strategy.merge.PriorityMerger;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;

public class XmiDatasetExtractor {
	public static final String CSV_FILE_NAME = "features.csv";
	public static final String DATASET_FILE_NAME = "dataset.csv";
	
	private File outputDir;
	private File frDcListFile;
	
	public XmiDatasetExtractor(File frDcListFile) {
		this.frDcListFile = frDcListFile;
	}
	
	public void build(File xmiDir, File outputDir) throws UIMAException, IOException, URISyntaxException{
		this.outputDir = outputDir;
		
		//Create json files for general and word alignment information for both English and French discourse connectives.
		//For French texts, both connectives that are used in discourse usage and non discourse usage are exported.
		//it is assumed that the xmi files contain discourse parsed texts. 
		runPipeline(xmiDir, outputDir);

		//convert json files to csv files, it also rename columns based on the name of the file
		buildCSVFiles();
		
		//unify all csv files.
		mergeAll();
	}

	private void buildCSVFiles() throws IOException {
		for (File aDir: outputDir.listFiles()){
			if (aDir.isFile())
				continue;
			FeatureJSonConverter converter = new FeatureJSonConverter(new FeatureToCSVConverter(-1, new File(aDir, CSV_FILE_NAME), false));
			converter.convert(new File(aDir, JSonDataWriter.JSON_FILE_NAME));
			CSVContent csvContent = new CSVContent(new File(aDir, CSV_FILE_NAME));
			
			Set<Tag> tags = Tag.parse(aDir.getName());
			List<String> header = csvContent.getHeader();
			for (String aHeader: header){
				Set<Tag> headerTag = Tag.parse(aHeader);
				if (headerTag.contains(Tag.sense)){
					csvContent.replace(aHeader, "null", Boolean.toString(tags.contains(Tag.PARSED)));
				}
				csvContent.renameColumn(aHeader, Tag.addTags(aHeader, Tag.getLang(parse(aDir.getName()))));
			}
			
			csvContent.save(new File(aDir, CSV_FILE_NAME));
		}
	}

	private void mergeAll() throws IOException {
		mergeFeatures(Tag.EN);
		mergeFeatures(Tag.FR);
		mergeLanguages();
	}

	private void mergeLanguages() throws IOException {
		CSVMerger csvMerger = new CSVMerger(null, new PriorityMerger());
		for (File file: outputDir.listFiles()){
			if (file.getName().startsWith(".") || file.isDirectory())
				continue;
			Set<Tag> tags = Tag.parse(file.getName());
			if (tags.contains(Tag.csv) && Tag.getLang(tags) != null){
				csvMerger.merge(new CSVContent(file));
			}
		}
		
		CSVContent csvContent = csvMerger.getCsvContent();
		System.out.println("Dataset size: " + csvContent.getContent().size() + " " + (csvContent.getContent().size() == 3546 ? "Ok!" : "Error."));
		csvContent.save(new File(outputDir, DATASET_FILE_NAME));
	}


	private void mergeFeatures(Tag lang) throws IOException {
		String tagToString = tagToString(lang, Tag.dc, Tag.begin);
		PriorityMerger strategy = new PriorityMerger(0);
		CSVMerger csvMerger = new CSVMerger(tagToString, strategy);
		
		for (File aDir: outputDir.listFiles()){
			if (aDir.isFile())
				continue;
			Set<Tag> tags = parse(aDir.getName());
			if (tags.contains(Tag.PARSED))	//if the dc is parsed then its information will be picked
				strategy.setToBePicked(1);
			else
				strategy.setToBePicked(0);
			if (tags.contains(lang) && aDir.isDirectory()){
				csvMerger.merge(new CSVContent(new File(aDir, CSV_FILE_NAME)));
			}
		}
		CSVContent csvContent = csvMerger.getCsvContent();
		
		csvContent.save(new File(outputDir, lang + ".csv"));
	}


	private void runPipeline(File xmiDir, File outputDir)
			throws ResourceInitializationException, UIMAException, IOException, URISyntaxException {
		CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, xmiDir, 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});

		AggregateBuilder builder = new AggregateBuilder();
		builder.add(GeneralFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.EN, Tag.GENERAL, Tag.PARSED)))), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		builder.add(DcAlignmentsFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.EN, Tag.alignment, Tag.PARSED))), "moses"), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		
		builder.add(GeneralFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.FR, Tag.GENERAL, Tag.PARSED)))), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		builder.add(DcAlignmentsFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.FR, Tag.alignment, Tag.PARSED))), "moses"), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		builder.add(LookupInstanceAnnotator.getDescription(frDcListFile, DiscourseAnnotationFactory.class), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		builder.add(GeneralFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.FR, Tag.GENERAL, Tag.NOT_PARSED)))), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		builder.add(DcAlignmentsFeatures.getDescription(mkdir(new File(outputDir, tagToString(Tag.FR, Tag.alignment, Tag.NOT_PARSED))), "moses"), 
				CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		
		Utils.runWithProgressbar(reader, builder.createAggregateDescription());
	}
	

	
	public File mkdir(File dir){
		dir.mkdirs();
		return dir;
	}


	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();
		
		@Option(
				shortName = "f",
				longName = "frDcFile",
				description = "Specify the file that contains french DC.")
		public File getFrDc();
		
	}
	
	public static CSVContent selectWithSubscritp(CSVContent dataset, String subscriptColumn, String alignmentColumn){
		CSVContent selected = new CSVContent(dataset.getHeader(), new ArrayList<>(), "");
		List<String> subscripted = dataset.getColumn(subscriptColumn);
		List<List<AlignmentFeature>> rowAlignments = dataset.getColumn(alignmentColumn).stream().map(AlignmentFeature::parse).collect(Collectors.toList());
		
		for (int i = 0; i < subscripted.size(); i++){
			List<AlignmentFeature> alignments = rowAlignments.get(i);
			if (alignments.size() > 0){
				AlignmentFeature alignment = alignments.get(0);
				if (alignment.getText().length() > 0 && subscripted.get(i).toLowerCase().contains(alignment.getText() + "<sub>")){
					selected.addRow(dataset.getContent().get(i));
				}
			}
		}
		return selected;
	}
	
	//-i outputs/crowdFlower/newapproach/xmi/french_parser
	//-f /Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation/outputs/fdtb-gold/model/dc.list
	public static void main(String[] args) throws UIMAException, IOException, URISyntaxException {
		Options options = CliFactory.parseArguments(Options.class, args);
		XmiDatasetExtractor csvBuilder = new XmiDatasetExtractor(options.getFrDc());
		
		csvBuilder.build(options.getInputDataset(), options.getOutputDir());
		
//		File input = new File("outputs/crowdFlower/newapproach/csv/dataset.csv");
//		File output = new File("outputs/crowdFlower/newapproach/examples.csv");
//		CSVContent selectWithSubscritp = selectWithSubscritp(new CSVContent(input), "FR.chunk.alignment.superscript", "FR.dc.alignment");
//		System.out.println("Examples: " + selectWithSubscritp.getContent().size());
//		selectWithSubscritp.save(output);
	}
}
