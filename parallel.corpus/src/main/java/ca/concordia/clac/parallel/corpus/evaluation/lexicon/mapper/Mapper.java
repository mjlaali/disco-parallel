package ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.SetMultimap;

public abstract class Mapper {
	
	public Map<Pair<String, String>, Double> mapLexicon(Map<Pair<String, String>, Double> pdtbLexicon){
		Map<Pair<String, String>, Double> mappedLexicon = new HashMap<>();
		for (Entry<Pair<String, String>, Double> aDcPdtbRel: pdtbLexicon.entrySet()){
			String aDc = aDcPdtbRel.getKey().getFirst();
			String pdtbRel = aDcPdtbRel.getKey().getSecond();
			double p = aDcPdtbRel.getValue();
			for (Pair<String, Double> sdrtRelProb: getMapping(aDc, pdtbRel)){
				Pair<String, String> aDcSdrtRel = new Pair<>(aDc, sdrtRelProb.getFirst());
				Double prevProb = mappedLexicon.get(aDcSdrtRel);
				if (prevProb == null)
					prevProb = 0.0;
				mappedLexicon.put(aDcSdrtRel, prevProb + p * sdrtRelProb.getSecond());
			}
		}
		
		return mappedLexicon;
	}

	public abstract Collection<Pair<String, Double>> getMapping(String aDc, String pdtbRel);
	public abstract Collection<Pair<String, Double>> getReverseMapping(String aDc, String sdrtRel);
	public abstract Set<String> getPDTBRelations();
	public abstract Set<String> getLexconnRelations();
	public abstract SetMultimap<String, Pair<String, Double>> getMapping();
}
