package ca.concordia.clac.parallel.corpus.builder;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.apache.uima.cas.CASException;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;

public class LabelExtractor implements Function<DiscourseConnective, String>{
	private JCas aJCas;
	private Map<DiscourseConnective, Collection<ParallelChunk>> dcToParallelChunk;
	public LabelExtractor(JCas aJCas) {
		this.aJCas = aJCas;
	}
	
	@Override
	public String apply(DiscourseConnective dc) {
		ParallelChunk frChunk = getDcToParallelChunk().get(dc).iterator().next();
		ParallelChunk enChunk = frChunk.getTranslation();
		if (frChunk == null || enChunk == null)
			return "ERROR";
		List<DiscourseConnective> enDCs = JCasUtil.selectCovered(DiscourseConnective.class, enChunk);
		List<DiscourseConnective> frDCs = JCasUtil.selectCovered(DiscourseConnective.class, frChunk);
		if (enDCs.size() > 1 || frDCs.size() > 1){
			return "Invalid";
		} else if (enDCs.size() == 1) {
			return Boolean.toString(true);
		} else {
			dc.removeFromIndexes();
			return  Boolean.toString(false);
		}
	}
	
	
	public Map<DiscourseConnective, Collection<ParallelChunk>> getDcToParallelChunk() {
		if (dcToParallelChunk == null){
			try {
				dcToParallelChunk = JCasUtil.indexCovering(aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW), DiscourseConnective.class, ParallelChunk.class);
			} catch (CASException e) {
				throw new RuntimeException(e);
			}
		}
		return dcToParallelChunk;
	}
	
}