package ca.concordia.clac.parallel.corpus.evaluation.lexicon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;

import org.apache.commons.math3.util.Pair;

import com.google.common.collect.SetMultimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.parallel.corpus.builder.DCDictionary;
import ca.concordia.clac.parallel.corpus.builder.DCDictionary.DicEntry;
import ca.concordia.clac.parallel.corpus.evaluation.lexicon.mapper.Mapper;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableReader;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableTextFilter;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.SenseRemoval;

public class Printer {
	Map<String, String> idToCanonical;
	DefaultLexconnReader reader;

	public Printer(DefaultLexconnReader reader) {
		this.reader = reader;
		this.idToCanonical = reader.getIdToCanonicalForm();
	}

	private List<Entry<Pair<String, String>, Double>> sort(Map<Pair<String, String>, Double> aLexicon) {
		List<Entry<Pair<String, String>, Double>> entries = new ArrayList<>(aLexicon.entrySet());
		Collections.sort(entries, (a, b) -> b.getValue().compareTo(a.getValue()));
		return entries;
	}

	public void printALexicon(Map<Pair<String, String>, Double> lexicon, Map<String, Integer> counts, File outputFile) throws UnsupportedEncodingException, FileNotFoundException{
		List<Entry<Pair<String, String>, Double>> entries = sort(lexicon);
		PrintStream ps = new PrintStream(new FileOutputStream(outputFile), true, "UTF-8");

		for (Entry<Pair<String, String>, Double> anEntry: entries){
			ps.printf("%s (%s) -> %.2f (%d)\n", anEntry.getKey(), idToCanonical.get(anEntry.getKey().getFirst()), anEntry.getValue(), counts.get(anEntry.getKey().getKey()));
		}

		ps.close();
	}

	public void saveALexicon(Map<Pair<String, String>, Double> lexicon, File outputFile, 
			String phraseTableName, Mapper mapper) throws IOException{
		SetMultimap<String, DicEntry> frToEn = readPhraseTable(phraseTableName, PhraseTableTextFilter.defaultNormalizer);

		List<ParallelConnectives> toBeSaved = new ArrayList<>();
		for (Entry<Pair<String, String>, Double> anEntry: lexicon.entrySet()){
			String sdrtSense = anEntry.getKey().getSecond();
			String frDcId = anEntry.getKey().getFirst();
			double suggestionProb = anEntry.getValue();
			if (suggestionProb < 0.01)
				continue;
			
			Collection<Pair<String, Double>> pdtbSenses = mapper.getReverseMapping(frDcId, sdrtSense);
			for (Pair<String, Double> pdtbSense: pdtbSenses){
				Set<String> frDcs = new HashSet<>(reader.getIdToForms().get(frDcId));
				Set<String> enDcs = getEnTranslation(frDcs, pdtbSense.getFirst(), frToEn);
				toBeSaved.add(new ParallelConnectives(
						enDcs, 
						frDcs,
						pdtbSense.getFirst(),
						sdrtSense,
						suggestionProb));
			}
		}

		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		Gson gson = builder.create();
		FileWriter writer = new FileWriter(outputFile);
		gson.toJson(toBeSaved, writer);
		writer.close();
	}

	private Set<String> getEnTranslation(Set<String> frDcs, String sense, SetMultimap<String, DicEntry> frToEn)
			throws FileNotFoundException {
		Function<String, String> normalizer = PhraseTableTextFilter.defaultNormalizer;
		
		Set<String> keys = frToEn.keySet();
		Map<String, String> noSpaceToSpace = new HashMap<>();
		for (String str: keys){
			noSpaceToSpace.put(normalizer.apply(str), str);
		}
		
		Set<String> enDcs = new HashSet<>();
		for (String aForm: frDcs){
			String tokenized = noSpaceToSpace.get(normalizer.apply(aForm));
			if (tokenized == null)
				continue;
			Set<DicEntry> set = frToEn.get(tokenized);
			
			for (DicEntry anEntry: set){
				String translation = anEntry.getTranslation();
				String entrySense = SenseRemoval.getSense(translation);
				if (entrySense.equals(sense))
					enDcs.add(SenseRemoval.removeSense(translation));
			}
		}
		
//		if (enDcs.size() == 0)
//			System.err.println("Printer.getEnTranslation()" + frDcs + ", " + sense);
		return enDcs;
	}

	private SetMultimap<String, DicEntry> readPhraseTable(String phraseTableName, Function<String, String> normalizer)
			throws IOException {
		File phraseTableFile = new File("outputs/phrase-table/" + phraseTableName);
		
		DCDictionary dictionary = new DCDictionary(normalizer);
		PhraseTableReader phraseTableReader = new PhraseTableReader();
		phraseTableReader.setFilter(dictionary);
		phraseTableReader.parse(phraseTableFile);
		SetMultimap<String, DicEntry> frToEn = dictionary.getFrDcToEn();
		return frToEn;
	}
	
	
}