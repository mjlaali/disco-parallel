package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.xml.sax.SAXException;

import ca.concordia.clac.lexconn.DefaultLexconnReader;

public class PhraseTableTextFilter implements Predicate<PhraseTableEntry>{
	private BiPredicate<String, String> textFilter;
	private Function<String, String> normalizer;

	public PhraseTableTextFilter(BiPredicate<String, String> textFilter, Function<String, String> normalizer) {
		this.normalizer = normalizer;
		this.textFilter = textFilter;
	}

	@Override
	public boolean test(PhraseTableEntry entry) {
		String frenchText = entry.getFrEntry().getText();
		String englishText = entry.getEnEntry().getText();
		
		if (normalizer != null){
			frenchText = normalizer.apply(frenchText);
			englishText = normalizer.apply(englishText);
		}

		return textFilter.test(englishText, frenchText);
	}



	public static final Function<String, String> defaultNormalizer = new SenseRemoval().andThen(new SpaceRemover());
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		for(String extension: new String[]{".enfr", ".en"}){
			String phraseTablePath = "/Users/majid/Documents/resource/phrase-table" + extension;
			File outputDir = new File("outputs/phrase-table");
			outputDir.mkdirs();

			File unionFile = new File(outputDir, "union" + extension);
			File intersectionFile = new File(outputDir, "intersection" + extension);;
			File enDiscourseUsageFile = new File(outputDir, "en-discourse-usage" + extension);;
			File enFrDiscourseUsageFile = new File(outputDir, "enfr-discourse-usage" + extension);;
			File lexconnFile = new File(new File("/Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation"), DefaultLexconnReader.LEXCONN_FILE);

			File phraseTableFile = new File(phraseTablePath);

			DefaultLexconnReader lexconnMap = DefaultLexconnReader.getLexconnMap(lexconnFile);
			Set<String> frenchDcs = lexconnMap.getFormToId().keySet().stream().map(new SpaceRemover()).collect(Collectors.toSet());

			File englishDcsFile = new File("resources/english-dc.txt");
			Set<String> englishDcs = FileUtils.readLines(englishDcsFile, StandardCharsets.UTF_8).stream().map(new SpaceRemover()).collect(Collectors.toSet());

			PhraseTableReader phraseTableReader;
			phraseTableReader = new PhraseTableReader();
			phraseTableReader.setFilter(new PhraseTableTextFilter((en, fr) -> englishDcs.contains(en) || frenchDcs.contains(fr), defaultNormalizer));
			phraseTableReader.setOutput(new PrintStream(new FileOutputStream(unionFile), true, "UTF-8"));
			phraseTableReader.parse(phraseTableFile);

			System.out.println("PhraseTableTextFilter.main(): creating intersection ...");

			phraseTableReader = new PhraseTableReader();
			phraseTableReader.setFilter(new PhraseTableTextFilter((en, fr) -> englishDcs.contains(en) && frenchDcs.contains(fr), defaultNormalizer));
			phraseTableReader.setOutput(new PrintStream(new FileOutputStream(intersectionFile), true, "UTF-8"));
			phraseTableReader.parse(unionFile);

			System.out.println("PhraseTableTextFilter.main(): keep only in discourse usage ...");

			phraseTableReader = new PhraseTableReader();
			phraseTableReader.setFilter(new PhraseTableTextFilter((en, fr) -> en.contains(SenseRemoval.SENSE_SEPARATOR), null));
			phraseTableReader.setOutput(new PrintStream(new FileOutputStream(enDiscourseUsageFile), true, "UTF-8"));
			phraseTableReader.parse(intersectionFile);

			phraseTableReader = new PhraseTableReader();
			phraseTableReader.setFilter(new PhraseTableTextFilter((en, fr) -> fr.contains(SenseRemoval.SENSE_SEPARATOR), null));
			phraseTableReader.setOutput(new PrintStream(new FileOutputStream(enFrDiscourseUsageFile), true, "UTF-8"));
			phraseTableReader.parse(enDiscourseUsageFile);
		}
		System.out.println("DCDictionary.main()");

	}

}
