package ca.concordia.clac.parallel.corpus.moses.phrase_table;

public class Entry{
	String text;
	double probGivenSource;
	int freq;
	
	@Override
	public String toString() {
		return "Entry [text=" + text + ", probGivenSource=" + probGivenSource + ", freq=" + freq + "]";
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public double getProbGivenSource() {
		return probGivenSource;
	}

	public void setProbGivenSource(double probGivenSource) {
		this.probGivenSource = probGivenSource;
	}

	public int getFreq() {
		return freq;
	}

	public void setFreq(int freq) {
		this.freq = freq;
	}

	public Entry(String text, double probGivenSource, int freq) {
		super();
		this.text = text;
		this.probGivenSource = probGivenSource;
		this.freq = freq;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + freq;
		long temp;
		temp = Double.doubleToLongBits(probGivenSource);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entry other = (Entry) obj;
		if (freq != other.freq)
			return false;
		if (Double.doubleToLongBits(probGivenSource) != Double.doubleToLongBits(other.probGivenSource))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}


}