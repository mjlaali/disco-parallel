package ca.concordia.clac.parallel.corpus.builder;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.uima.cas.CASException;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;

import ca.concordia.clac.ml.classifier.InstanceExtractor;

public class CandidateExtractor implements InstanceExtractor<DiscourseConnective>{
	private InstanceExtractor<DiscourseConnective> toBeDecorated;
	public CandidateExtractor(InstanceExtractor<DiscourseConnective> toBeDecorated) {
		this.toBeDecorated = toBeDecorated;
	}

	@Override
	public Collection<DiscourseConnective> getInstances(JCas aJCas) {
		Function<JCas, Collection<DiscourseConnective>> instanceExtractor = toBeDecorated::getInstances;
		JCas frView = null;
		JCas enView = null;
		try {
			frView = aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
			enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		} catch (CASException e1) {
			e1.printStackTrace();
		} 
		
		Collection<DiscourseConnective> frDcs = instanceExtractor.apply(frView);
//		frDcs.stream().forEach((dc) -> System.out.println(dc.getCoveredText()));
		frDcs.stream().forEach(DiscourseConnective::addToIndexes);
		
		//remove parallel chunks with more than one DC in French side
		Map<ParallelChunk, Integer> frParallelChunkToFrDc = getParallelChunkToDcs(frView);
		Map<DiscourseConnective, Collection<ParallelChunk>> dcToFrParallelChunk = 
				JCasUtil.indexCovering(frView, DiscourseConnective.class, ParallelChunk.class);
		frDcs = pruneList(frDcs, frParallelChunkToFrDc, dcToFrParallelChunk);
//		System.out.println("------");
//		frDcs.stream().forEach((dc) -> System.out.println(dc.getCoveredText()));
		
		//remove parallel chunks with more than one DC in English side
		Map<ParallelChunk, Integer> enParallelChunkToEnDc = getParallelChunkToDcs(enView);
		Map<ParallelChunk, Integer> frParallelChunkToEnDc = new HashMap<>();
		enParallelChunkToEnDc.forEach((enChunk, count) -> frParallelChunkToEnDc.put(enChunk.getTranslation(), count));
		frDcs = pruneList(frDcs, frParallelChunkToEnDc, dcToFrParallelChunk);
//		System.out.println("------");
//		frDcs.stream().forEach((dc) -> System.out.println(dc.getCoveredText()));
		return frDcs;
	}

	private static List<DiscourseConnective> pruneList(Collection<DiscourseConnective> lFrDc,
			Map<ParallelChunk, Integer> parallelChunkToDc,
			Map<DiscourseConnective, Collection<ParallelChunk>> dcToParallelChunk) {
		//found all invalid DC (i.e. DCs that appear in parallel chunks with more than one DC
		Set<DiscourseConnective> willBeRemoved = new HashSet<>();
		for (DiscourseConnective aDc: lFrDc){
			for (ParallelChunk parallelChunk: dcToParallelChunk.get(aDc)){
				Integer cnt = parallelChunkToDc.get(parallelChunk);
				if (cnt == null || cnt < 0 || cnt > 1)
					willBeRemoved.add(aDc);
			}
		}
		
		//remove invalid DCs and add them to index
		List<DiscourseConnective> res = lFrDc.stream().filter((aDc) -> !willBeRemoved.contains(aDc)).collect(Collectors.toList());
		willBeRemoved.stream().forEach(DiscourseConnective::removeFromIndexes);
		res.forEach(DiscourseConnective::addToIndexes);
		return res;
	}

	private static Map<ParallelChunk, Integer> getParallelChunkToDcs(JCas frView) {
		Map<ParallelChunk, Integer> index = new HashMap<>();
		//initialize the index.
		JCasUtil.select(frView, ParallelChunk.class).stream().forEach((aChunk) -> index.put(aChunk, 0));
		Map<DiscourseConnective, Collection<ParallelChunk>> indexCovering = 
				JCasUtil.indexCovering(frView, DiscourseConnective.class, ParallelChunk.class);
		indexCovering.forEach(
				(aDc, chunkList) -> 
					chunkList.stream().forEach((aChunk) -> 
						index.put(aChunk, index.get(aChunk) + 1)));
		
		return index;
	}

}
