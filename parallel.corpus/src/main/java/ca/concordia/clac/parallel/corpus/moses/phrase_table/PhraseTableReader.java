package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;
import java.util.function.Predicate;

import org.apache.commons.io.output.NullOutputStream;

public class PhraseTableReader {
	private Predicate<PhraseTableEntry> filter;
	private PrintStream output;
	
	public PhraseTableReader() {
		output = new PrintStream(new NullOutputStream());
	}
	
	public void setFilter(Predicate<PhraseTableEntry> filter) {
		this.filter = filter;
	}

	public void setOutput(PrintStream output) {
		this.output = output;
	}
	
	public void parse(File file) throws IOException{
		BufferedInputStream input = new BufferedInputStream(new FileInputStream(file));
		parse(input);
		input.close();
	}
	
	public void parse(InputStream input){
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(input, "UTF-8");
		int lineIdx = 0;
		
		while (scanner.hasNextLine()){
			lineIdx++;
			if (lineIdx % 100000 == 0){
				System.out.println("PhraseTableReader.parse() " + lineIdx);
			}
			String line = scanner.nextLine();
			
			PhraseTableEntry aPhraseTableEntry = PhraseTableEntry.make(line);
			try {
				if (filter.test(aPhraseTableEntry))
					output.println(line);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	
}
