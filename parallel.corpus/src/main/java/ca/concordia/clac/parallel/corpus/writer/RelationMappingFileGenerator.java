package ca.concordia.clac.parallel.corpus.writer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.ArrayUtils;
import org.xml.sax.SAXException;

import ca.concordia.clac.lexconn.DefaultLexconnReader;

public class RelationMappingFileGenerator {
	Map<String, Integer> dcId = new TreeMap<>();
	Map<String, Integer> lexconnRelId = new TreeMap<>();
	Map<String, Integer> pdtbRelId = new TreeMap<>();
	Lexconn lexicon, lexconn;
	
	public RelationMappingFileGenerator(Lexconn lexconn, Lexconn lexicon){
		this.lexconn = lexconn;
		this.lexicon = lexicon;
	
		Set<String> ids = lexicon.getIds();
		ids.retainAll(lexconn.getIds());
		setIds(ids);
	}

	private void setIds(Set<String> ids) {
		dcId.clear(); lexconnRelId.clear(); pdtbRelId.clear();
		setId(ids, dcId);
		setId(getRelations(lexconn, ids), lexconnRelId);
		setId(getRelations(lexicon, ids), pdtbRelId);
	}

	private Set<String> getRelations(Lexconn aLexicon, Set<String> ids) {
		List<LexconnEntry> list = aLexicon.getEntries().stream().filter((e) -> ids.contains(e.getId())).collect(Collectors.toList());
		return new Lexconn(list).getRelations();
	}

	private void setId(Set<String> strs, Map<String, Integer> ids){
		strs.forEach((str) -> ids.put(str, ids.size())); 
	}
	
	private void save(Set<String> keys, OutputStream output) throws IOException{
		PrintStream ps = new PrintStream(output);
		keys.forEach((key) -> ps.println(key));
	}
	
	public void prun(int minFreq){
		Set<String> ids = lexicon.getIds();
		ids.retainAll(lexconn.getIds());
		
		for (LexconnEntry entry: lexicon.getEntries()){
			if (entry.getFreq() < minFreq)
				ids.remove(entry.getId());
		}
		setIds(ids);
	}
	
	public List<List<Double>> generateEmission(){
		double[][] emission = new double[dcId.size()][pdtbRelId.size()];
		
		for (LexconnEntry entry: lexicon.getEntries()){
			Map<String, Double> relProbs = entry.getRelation_probabilities();
			Integer dcIdx = dcId.get(entry.getId());
			if (dcIdx == null){
				continue;
			}
			for (Entry<String, Double> relProb: relProbs.entrySet()){
				int relId = pdtbRelId.get(relProb.getKey());
				emission[dcIdx][relId] = relProb.getValue();
			}
		}
		
		List<List<Double>> result = new ArrayList<>();
		for (int i = 0; i < emission.length; i++){
			Double[] ds = ArrayUtils.toObject(emission[i]);
			List<Double> asList = Arrays.asList(ds);
			result.add(asList);
		}
		return result;
	}
	
	public List<List<Integer>> generateEntries(){
		List<List<Integer>> lexconnEntry = new ArrayList<>();
		
		for (LexconnEntry entry: lexconn.getEntries()){
			Integer dcIdx = dcId.get(entry.getId());
			if (dcIdx == null || entry.getRelations() == null)
				continue;
			for (String relation: entry.getRelations().split("[, ]")){
				relation = relation.trim();
				if (relation.length() == 0)
					continue;
				int relId = lexconnRelId.get(relation);
				lexconnEntry.add(Arrays.asList(dcIdx, relId));
			}
		}
		
		return lexconnEntry;
		
	}
	
	
	public void saveConfigs(OutputStream lexconnRelations, OutputStream lexiconRelations, OutputStream connectivesId) throws IOException{
		save(lexconnRelId.keySet(), lexconnRelations);
		save(pdtbRelId.keySet(), lexiconRelations);
		save(dcId.keySet(), connectivesId);
	}
	
	public Map<String, Integer> getDcId() {
		return dcId;
	}
	
	public Map<String, Integer> getLexconnRelId() {
		return lexconnRelId;
	}
	
	public Map<String, Integer> getPdtbRelId() {
		return pdtbRelId;
	}

	public static <T> void print(List<List<T>> m, Writer out) throws IOException{
		CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withDelimiter(' ').withRecordSeparator('\n'));
		
		for (List<T> row: m){
			printer.printRecord(row);
		}
		printer.close();
	}
	
	public static void leaveOneOut(Lexconn lexconn, Lexconn lexicon, int minFreq, File outDir) throws IOException{
		RelationMappingFileGenerator main = new RelationMappingFileGenerator(lexconn, lexicon);
		main.prun(minFreq);
		Map<String, Integer> validIds = main.getDcId();
		List<LexconnEntry> allEntries = lexconn.getEntries();
		
		String[] fileNames = new String[]{"emission.txt", "entries.txt", "lexconn-relations.txt", "pdtb-relations.txt", "dcIds.txt"};
		
		for (LexconnEntry anEntry: allEntries){
			if (validIds.containsKey(anEntry.getId())){
				ArrayList<LexconnEntry> trainEntries = new ArrayList<>(allEntries);
				trainEntries.remove(anEntry);
				RelationMappingFileGenerator train = new RelationMappingFileGenerator(new Lexconn(trainEntries), lexicon);
				train.prun(minFreq);
				
				File entryOutputDir = new File(outDir, anEntry.getId());
				entryOutputDir.mkdirs();
				
				OutputStream[] outs = new OutputStream[fileNames.length];
				for (int i = 0; i < outs.length; i++){
					outs[i] = new FileOutputStream(new File(entryOutputDir, fileNames[i]));
				}
				print(train.generateEmission(), new OutputStreamWriter(outs[0]));
				print(train.generateEntries(), new OutputStreamWriter(outs[1]));
				train.saveConfigs(outs[2], outs[3], outs[4]);
				
				for (int i = 0; i < outs.length; i++){
					outs[i].close();
				}
			}
		}
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, JAXBException {
		File lexiconFile = new File("outputs/lexicon/en-discourse-usage.en.xml");
		File outDir = new File("outputs/leave-one-out");
		Lexconn lexconn = LexconnWriter.convert(DefaultLexconnReader.getLexconnMap());
		Lexconn lexicon = LexconnWriter.loadFromAnXML(lexiconFile);
		
		leaveOneOut(lexconn, lexicon, 50, outDir);
	}
}
