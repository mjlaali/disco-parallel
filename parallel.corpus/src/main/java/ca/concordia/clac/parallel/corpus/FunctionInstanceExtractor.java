package ca.concordia.clac.parallel.corpus;

import java.util.Collection;

import org.apache.uima.jcas.JCas;

import ca.concordia.clac.ml.classifier.InstanceExtractor;

public interface FunctionInstanceExtractor<INSTANCE_TYPE> extends InstanceExtractor<INSTANCE_TYPE>{

	
	@Override
	default Collection<INSTANCE_TYPE> getInstances(JCas aJCas) {
		return null;
	}

}
