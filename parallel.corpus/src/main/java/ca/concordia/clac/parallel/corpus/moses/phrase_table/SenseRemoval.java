package ca.concordia.clac.parallel.corpus.moses.phrase_table;

import java.util.function.Function;

public class SenseRemoval implements Function<String, String>{
	public static final Function<String, String> relationNormalizer = new RelationNormalzier();
	public static class RelationNormalzier implements Function<String, String>{
		@Override
		public String apply(String t) {
			String[] tokens = t.toLowerCase().split("[._-]");
			StringBuilder sb = new StringBuilder();
			int tokenIdx = 0;
			for (String token: tokens){
				if (token.length() == 0)
					continue;
				if (tokenIdx > 0)
					if (tokenIdx < 3)
						sb.append(".");
					else
						sb.append("-");
				sb.append(Character.toUpperCase(token.charAt(0)));
				sb.append(token.substring(1));
				tokenIdx++;
			}
			return sb.toString();
		}
		
	}
	
	public static final String SENSE_SEPARATOR = "__";

	public static String getSense(String dcWithSense){
		int sensePos = dcWithSense.indexOf(SENSE_SEPARATOR);
		if (sensePos != -1){
			String sense = dcWithSense.substring(sensePos + SENSE_SEPARATOR.length());
			if (sense.contains(" ") || sense.contains(SENSE_SEPARATOR)){
				throw new RuntimeException("Invalid sense");
			}
			return relationNormalizer.apply(sense);
		}
		return null;
	}
	
	@Override
	public String apply(String aText) {
		return removeSense(aText);
	}
	
	public static String removeSense(String aText){
		int sensePos = aText.indexOf(SENSE_SEPARATOR);
		if (sensePos != -1 && !aText.contains(" ")){
			int nextWord = 0;
			StringBuilder sb = new StringBuilder();
			while (sensePos != -1){
				sb.append(aText.substring(nextWord, sensePos));
				nextWord = aText.indexOf(' ', sensePos);
				if (nextWord != -1)
					sensePos = aText.indexOf(SENSE_SEPARATOR, nextWord);
				else 
					sensePos = -1;
			}
			if (nextWord != -1)
				sb.append(aText.substring(nextWord));
			aText = sb.toString();
		}


		return aText.replaceAll("(\\w)_(\\w)", "$1 $2");
	}

	public static void main(String[] args) {
		System.out.println(SenseRemoval.removeSense("en__Null bref"));
	}
}