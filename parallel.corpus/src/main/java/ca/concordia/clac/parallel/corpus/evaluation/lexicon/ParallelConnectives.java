package ca.concordia.clac.parallel.corpus.evaluation.lexicon;

import java.io.Serializable;
import java.util.Set;

public class ParallelConnectives implements Serializable{
	private static final long serialVersionUID = 1L;
	private String pdtbRelation;
	private String lexconnRelation;
	private Double prob;
	private Set<String> enDcs;
	private Set<String> frDcs;

	public ParallelConnectives(Set<String> enDcs, Set<String> frDcs, String pdtbRelation, String lexconnRelation, Double prob){
		this.enDcs = enDcs;
		this.frDcs = frDcs;
		this.pdtbRelation = pdtbRelation;
		this.lexconnRelation = lexconnRelation;
		this.prob = prob;
	}
	
	public String getPdtbRelation() {
		return pdtbRelation;
	}
	
	public String getLexconnRelation() {
		return lexconnRelation;
	}
	
	public Set<String> getFrDcs() {
		return frDcs;
	}
	
	public Set<String> getEnDcs() {
		return enDcs;
	}
	
	public Double getProb() {
		return prob;
	}
}
