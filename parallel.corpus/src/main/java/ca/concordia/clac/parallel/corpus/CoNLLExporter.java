package ca.concordia.clac.parallel.corpus;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.uima.UIMAException;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.cleartk.corpus.conll2015.ConllJSonGoldExporter;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.batch_process.BatchProcess;

public class CoNLLExporter {

	public interface Options{
		@Option(
				defaultToNull=true,
				shortName = "i",
				longName = "inputDataset", 
				description = "Specify the input dataset for the parser. If you choose CoNLL type,\n\t"
						+ "the input dataset should be a folder with the same format as CoNLL dataset,\n\t"
						+ "otherwise it should be a simple text")
		public String getInputDataset();

		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory of the parser")
		public String getOutputDir();
	}

	public static void main(String[] args) throws UIMAException, IOException, URISyntaxException, ClassNotFoundException, SAXException, CpeDescriptorException {
		Options options = CliFactory.parseArguments(Options.class, args);

		File outputDir = new File(options.getOutputDir());
		if (!outputDir.exists())
			outputDir.mkdirs();

		BatchProcess batchProcess = new BatchProcess(BatchProcess.getXmiReader(new File(options.getInputDataset())), outputDir);
		batchProcess.addProcess("Exporter", EuroparlParallelTextAnnotator.EN_TEXT_VIEW,
				ConllJSonGoldExporter.getDescription(new File(outputDir, "relations.json")));
		
		batchProcess.run();
		batchProcess.save();
	}
}
