package ca.concordia.clac.parallel.corpus.builder;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.conll2015.TokenListTools;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.DcAlignmentsFeatures;
import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class AnnotationProjectionFilter extends JCasAnnotator_ImplBase{
	public static final String ALIGNMENT_COMMENT = "DC-Alignment";
	public static final String PARAM_FILTER_ON = "filterOn";
	
	private String enViewName = EuroparlParallelTextAnnotator.EN_TEXT_VIEW;
	private String frViewName = EuroparlParallelTextAnnotator.FR_TEXT_VIEW;
	
	private DcAlignmentsFeatures alignmentMapper = new DcAlignmentsFeatures();
	
	@ConfigurationParameter(name=PARAM_FILTER_ON)
	private boolean filterOn;

	public static AnalysisEngineDescription getDescription(boolean filterOn) throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(AnnotationProjectionFilter.class, 
				PARAM_FILTER_ON, filterOn);
	}
	

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		JCas frView;
		JCas enView;
		
		try {
			frView = aJCas.getView(frViewName);
			enView = aJCas.getView(enViewName);
		} catch (CASException e) {
			throw new RuntimeException(e);
		}
		
		Collection<DiscourseConnective> frDcs = new ArrayList<>(JCasUtil.select(frView, DiscourseConnective.class));
		Collection<DiscourseConnective> enDcs = JCasUtil.select(enView, DiscourseConnective.class);
		Map<Token, DiscourseConnective> enTokenToEnDc = new HashMap<>();
		alignmentMapper.init(frView);
		
		for (DiscourseConnective dc: enDcs){
			for (Token token: TokenListTools.convertToTokens(dc)){
				enTokenToEnDc.put(token, dc);
			}
		}
		
		for (DiscourseConnective dc: frDcs){
			boolean mapped = false; 
			List<Token> alignedEnTokens = alignmentMapper.getAlignedTokens(dc);
			for (Token alingedEnToken: alignedEnTokens){
				mapped = true;
				DiscourseConnective alignedEnDc = enTokenToEnDc.get(alingedEnToken);
				if (alignedEnDc != null){
					dc.setSense(alignedEnDc.getSense());
							
					Alignment frAlignment = initAlignment(dc, frView);
					Alignment enAlignment = initAlignment(alignedEnDc, enView);
					frAlignment.setAlignment(enAlignment);
					enAlignment.setAlignment(frAlignment);;
					break;
				} 
			}
			
			if (filterOn && !mapped){
				dc.removeFromIndexes();
			}
		}
	}
	
	
	
	private Alignment initAlignment(DiscourseConnective dc, JCas view) {
		Alignment alignment = new Alignment(view, dc);
		alignment.setComment(ALIGNMENT_COMMENT);
		alignment.addToIndexes();
		return alignment;
	}


}
