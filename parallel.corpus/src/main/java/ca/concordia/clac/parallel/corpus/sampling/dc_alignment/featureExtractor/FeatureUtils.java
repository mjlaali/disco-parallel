package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import org.apache.uima.jcas.tcas.Annotation;
import org.cleartk.ml.Feature;

public class FeatureUtils {

	List<Feature> features;

	public FeatureUtils() {
		features = new ArrayList<>();
	}
	
	public FeatureUtils(List<Feature> features) {
		this.features = new ArrayList<>(features); 
	}

	public <T extends Annotation> FeatureUtils addFeature(T annotation, String annotationText, Tag... tags){
		return addFeature(annotation, (ann) -> annotationText, tags);
	}
	
	public <T extends Annotation> FeatureUtils addFeature(T annotation, Tag... tags){
		return addFeature(annotation, (ann) -> ann.getCoveredText(), tags);
	}
	
	public <T extends Annotation> FeatureUtils addFeature(T annotation, Function<T, String> toString, Tag... tags){
		String text = toString.apply(annotation);
		features.addAll(Arrays.asList(
				new Feature(getTextFeatureName(tags), text),
				new Feature(getBeginFeatureName(tags), annotation.getBegin()),
				new Feature(getEndFeatureName(tags), annotation.getEnd()),
				new Feature(getLenFeatureName(tags), text.length())
				));

		return this;
	}

	public List<Feature> getFeatures() {
		return features;
	}
	
	public static String getLenFeatureName(Tag... tags) {
		return Tag.tagToString(Tag.addTags(tags, Tag.len));
	}

	public static String getEndFeatureName(Tag... tags) {
		return Tag.tagToString(Tag.addTags(tags, Tag.end));
	}

	public static String getTextFeatureName(Tag... tags) {
		return Tag.tagToString(Tag.addTags(tags, Tag.text));
	}
	
	public <T extends Annotation> FeatureUtils addFeature(Feature feature){
		features.add(feature);
		
		return this;
	}
		
	public static String getBeginFeatureName(Tag... tags){
		return Tag.tagToString(Tag.addTags(tags, Tag.begin));
	}
}
