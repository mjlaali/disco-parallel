package ca.concordia.clac.parallel.corpus.builder;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.CASException;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.metadata.CpeDescriptorException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.conll2015.TokenListTools;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;
import org.xml.sax.SAXException;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.uima.Utils;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextWriter;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;


public class ParallelTextExtractor extends JCasAnnotator_ImplBase {
	public static final String PARAM_OUTPUT_VIEW = "outputView";
	public static final String PARAM_TOKENIZE_OUTPUT = "tokenizeOutput";
	public static final String PARAM_LABEL_CONNECTIVES = "labelConnectives";

	@ConfigurationParameter(name=PARAM_TOKENIZE_OUTPUT)
	private boolean tokenizeOutput;

	@ConfigurationParameter(name=PARAM_LABEL_CONNECTIVES)
	private boolean labelConnectives;
	
	@ConfigurationParameter(name=PARAM_OUTPUT_VIEW)
	private String outputView; 
	
	public static AnalysisEngineDescription getEngineDescription(boolean tokenizeOutput, boolean labelConnectives, String outputView) throws ResourceInitializationException {
		return AnalysisEngineFactory.createEngineDescription(ParallelTextExtractor.class, 
				PARAM_TOKENIZE_OUTPUT, tokenizeOutput,
				PARAM_OUTPUT_VIEW, outputView, 
				PARAM_LABEL_CONNECTIVES, labelConnectives);
	}
	
	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		Map<Token, Collection<DiscourseConnective>> coveredDcs = JCasUtil.indexCovering(aJCas, Token.class, DiscourseConnective.class);
		
		Set<Token> toBeIgnored = new HashSet<>();
		
		StringBuilder sb = new StringBuilder();
		int lastEnd = 0;
		String text = aJCas.getDocumentText();
		for (Token token: JCasUtil.select(aJCas, Token.class)){
			if (toBeIgnored.remove(token)){	//this token was processed before for a DC.
				
			} else {
				String separator = text.substring(lastEnd, token.getBegin());
				if (tokenizeOutput)
					if (separator.isEmpty())
						separator = " ";
				sb.append(separator);
				Collection<DiscourseConnective> coveredDc = coveredDcs.get(token);
				if (coveredDc.size() != 0 && labelConnectives){
					if (coveredDc.size() != 1)
						throw new AnalysisEngineProcessException("Multiple DCs for shareing one token " + coveredDc.size(), null);
					DiscourseConnective dc = coveredDc.iterator().next();
					sb.append(TokenListTools.getTokenListText(dc).replace(" ", "_"));
					sb.append("__");
					String sense = dc.getSense();
					if (sense == null){
						sense = "Null";
					}
					
					sb.append(sense.replace('.', '_').replace(' ', '_'));
					toBeIgnored.addAll(TokenListTools.convertToTokens(dc));
				} else
					sb.append(token.getCoveredText());
				
			}
			lastEnd = token.getEnd();
		}
		
		if (lastEnd < text.length())
			sb.append(text.substring(lastEnd));
		
		try {
			JCas dcAnnotatedView = aJCas.createView(outputView);
			dcAnnotatedView.setDocumentText(sb.toString());
			
			DocumentMetaData.copy(aJCas, dcAnnotatedView);
		} catch (IllegalArgumentException e){
			//Do nothing. this means there is not Document metadata available in the view
			getLogger().warn("No DocumentMetadata has been found for the CAS");
		} catch (CASException e) {
			throw new AnalysisEngineProcessException(e);
		}
		
	}

	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public String getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public String getOutputDir();

	}
	
	public static void main(String[] args) throws URISyntaxException, UIMAException, IOException, SAXException, CpeDescriptorException {
		Options options = CliFactory.parseArguments(Options.class, args);
		
		System.out.println("ParallelTextExtractor.main(): Input = " + options.getInputDataset());
		System.out.println("ParallelTextExtractor.main(): Output = " + options.getOutputDir());
		
		final String ENGLISH_OUTPUT = "EnglishOutputView";
		final String FRENCH_OUTPUT = "FrenchOutputView";
		final String FRENCH_WITHOUT_DC_OUTPUT = "FrenchNoDcOutputView";
		final String ENGLISH_WITHOUT_DC_OUTPUT = "EnglishNoDcOutputView";
		
		AggregateBuilder builder = new AggregateBuilder();
		builder.add(ParallelTextExtractor.getEngineDescription(true, true, ENGLISH_OUTPUT), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		builder.add(ParallelTextExtractor.getEngineDescription(true, true, FRENCH_OUTPUT), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		builder.add(ParallelTextExtractor.getEngineDescription(true, false, FRENCH_WITHOUT_DC_OUTPUT), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);
		builder.add(ParallelTextExtractor.getEngineDescription(true, false, ENGLISH_WITHOUT_DC_OUTPUT), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		File textOutputDir = new File(new File(options.getOutputDir()), "texts");
		
		builder.add(AnalysisEngineFactory.createEngineDescription(TextWriter.class, 
					TextWriter.PARAM_TARGET_LOCATION, textOutputDir), 
				CAS.NAME_DEFAULT_SOFA, ENGLISH_OUTPUT);
		builder.add(AnalysisEngineFactory.createEngineDescription(TextWriter.class, 
				TextWriter.PARAM_TARGET_LOCATION, textOutputDir), 
				CAS.NAME_DEFAULT_SOFA, FRENCH_OUTPUT);

		builder.add(AnalysisEngineFactory.createEngineDescription(TextWriter.class, 
				TextWriter.PARAM_TARGET_LOCATION, new File(textOutputDir, "noDc")), 
				CAS.NAME_DEFAULT_SOFA, FRENCH_WITHOUT_DC_OUTPUT);
		builder.add(AnalysisEngineFactory.createEngineDescription(TextWriter.class, 
				TextWriter.PARAM_TARGET_LOCATION, new File(textOutputDir, "noDc")), 
				CAS.NAME_DEFAULT_SOFA, ENGLISH_WITHOUT_DC_OUTPUT);
		
		CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, options.getInputDataset(), 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		
		Utils.runWithProgressbar(reader, builder.createAggregateDescription());
	}

	
}
