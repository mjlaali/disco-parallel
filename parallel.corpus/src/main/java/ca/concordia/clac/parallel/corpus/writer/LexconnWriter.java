package ca.concordia.clac.parallel.corpus.writer;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import ca.concordia.clac.lexconn.DefaultLexconnReader;

public class LexconnWriter {
	
	public static void saveAsXml(DefaultLexconnReader reader, OutputStream output) throws JAXBException {
		saveAsXml(convert(reader), output);
	}

	
	public static void saveAsXml(List<LexconnEntry> lexconn, OutputStream output) throws JAXBException{
		saveAsXml(new Lexconn(lexconn), output);
	}

	public static void saveAsTbl(List<LexconnEntry> lexconn, OutputStream output, double eps) throws JAXBException{
		PrintStream ps = new PrintStream(output);
		for (LexconnEntry entry: lexconn){
			ps.print(entry.getId() + ",");
			ps.print(entry.getFreq() + ",");
			ps.print(entry.getForme().stream().collect(Collectors.joining("&")) + ",");
			entry.getRelation_probabilities().forEach((k, v) -> {if (v > eps) ps.print(String.format("%s=%.4f&", k, v));});
			ps.println();
		}
	}
	
	
	public static void saveAsXml(Lexconn lexconn, OutputStream output) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(LexconnEntry.class, Lexconn.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(lexconn, output);
	}
	
	public static Lexconn loadFromAnXML(File file) throws JAXBException, IOException{
		Reader reader = new FileReader(file);
		Lexconn result = loadFromAnXML(reader);
		reader.close();
		return result;
	}
	public static Lexconn loadFromAnXML(Reader reader) throws JAXBException{
		JAXBContext jaxbContext = JAXBContext.newInstance(LexconnEntry.class, Lexconn.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		return  (Lexconn) jaxbUnmarshaller.unmarshal(reader);
	}
	
	public static Lexconn convert(DefaultLexconnReader reader){
		Map<String, Set<String>> idToForms = reader.getIdToForms();
		Map<String, Collection<String>> idToRelations = reader.getIdToRelations().asMap();
		Map<String, String> idToCanonical = reader.getIdToCanonicalForm();
		List<LexconnEntry> entries = new ArrayList<>();
		for (Entry<String, Set<String>> anEntry: idToForms.entrySet()){
			String id = anEntry.getKey();
			Collection<String> relations = idToRelations.get(id);
			String strRelations = null;
			if (relations != null && relations.size() > 0){
				strRelations = relations.stream().collect(Collectors.joining(", "));
			}
			List<String> forms = new ArrayList<>(anEntry.getValue());
			forms.remove(idToCanonical.get(id));
			forms.add(0, idToCanonical.get(id));
			entries.add(new LexconnEntry(id, strRelations, forms));
		}
		Lexconn lexconn = new Lexconn(entries);
		return lexconn;
	}


}
