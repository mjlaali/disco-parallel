package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.uima.UimaContext;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.component.initialize.ConfigurationParameterInitializer;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.initializable.Initializable;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.discourse.type.DiscourseConnective;
import org.cleartk.ml.Feature;

import ca.concordia.clac.uima.types.paralleltexts.Alignment;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;

public class DcAlignmentsFeatures implements Function<DiscourseConnective, List<Feature>>, JCasInitializable, Initializable{
	public static final String PARAM_ALIGNMENT_NAME = "alignmentName";
	private final static Pattern punctuationPoses = Pattern.compile("[^\\w\\s]+"); 


	public static final String ALIGNMENT_FEATURE_NAME = Tag.tagToString(Tag.dc, Tag.alignment);

	@ConfigurationParameter(name=PARAM_ALIGNMENT_NAME, mandatory=true)
	private String alignmentName;

	Map<Token, Collection<Alignment>> tokenAlignments;

	public List<Token> getAlignedTokens(DiscourseConnective dc){
		Set<Token> tokenSet = new HashSet<>();
		for (int i = 0; i < dc.getTokens().size(); i++){
			for (Alignment alignment: tokenAlignments.get(dc.getTokens(i))){
				Annotation wrappedAnnotation = alignment.getAlignment().getWrappedAnnotation();
				if (wrappedAnnotation instanceof Token){
					Token alingedToken = (Token)wrappedAnnotation;
					if (punctuationPoses.matcher(alingedToken.getPos().getPosValue()).matches()){
						continue;
					}

					tokenSet.add(alingedToken);
				}
			}
		}

		List<Token> tokens = new ArrayList<>(tokenSet);
		Collections.sort(tokens, (t1, t2) -> new Integer(t1.getBegin()).compareTo(t2.getBegin()));
		return tokens;
	}

	@Override
	public List<Feature> apply(DiscourseConnective dc) {
		List<Token> tokens = getAlignedTokens(dc);

		String alignment = tokens.stream().map(Token::getCoveredText).collect(Collectors.joining(" "));
		int alignmentBegin = tokens.size() == 0 ? -1 : tokens.get(0).getBegin();
		int alignmentEnd = tokens.size() == 0 ? -1 : tokens.get(tokens.size() - 1).getEnd();

		return new FeatureUtils()
				.addFeature(dc, Tag.dc)
				.addFeature(
						new Feature(ALIGNMENT_FEATURE_NAME, new AlignmentFeature(alignment, alignmentName, alignmentBegin, alignmentEnd, 0).toString()))
				.getFeatures();
	}

	public static AnalysisEngineDescription getDescription(File outputDir, String name) throws ResourceInitializationException{
		return FeatureJsonExtractor.getDescription(outputDir, DcAlignmentsFeatures.class, PARAM_ALIGNMENT_NAME, name);
	}

	@Override
	public void init(JCas jcas) {
		tokenAlignments = JCasUtil.indexCovering(jcas, Token.class, Alignment.class);
	}

	@Override
	public void initialize(UimaContext context) throws ResourceInitializationException {
		ConfigurationParameterInitializer.initialize(this, context);
	}


}
