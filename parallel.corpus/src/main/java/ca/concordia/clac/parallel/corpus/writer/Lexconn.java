package ca.concordia.clac.parallel.corpus.writer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="liste")
public class Lexconn{
	@XmlElement(name="connecteur")
	List<LexconnEntry> entries;
	
	public Lexconn() {
	}

	public Lexconn(List<LexconnEntry> entries) {
		super();
		this.entries = entries;
	}
	
	public Set<String> getRelations(){
		
		Set<String> relations = new TreeSet<>(entries.stream()
				.map(LexconnEntry::getRelations)
				.map((str)-> str != null ? Arrays.asList(str.split("[, ]")) : new ArrayList<String>())
				.flatMap((s) -> s.stream())
				.filter((str) -> str.length() > 0)
				.collect(Collectors.toSet()));
		return relations;
	}

	public Set<String> getIds(){
		Set<String> relations = new TreeSet<>(entries.stream()
				.map(LexconnEntry::getId)
				.map(String::trim)
				.collect(Collectors.toSet()));
		return relations;
	}
	
	public List<LexconnEntry> getEntries() {
		return entries;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lexconn other = (Lexconn) obj;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		return true;
	}
	
}