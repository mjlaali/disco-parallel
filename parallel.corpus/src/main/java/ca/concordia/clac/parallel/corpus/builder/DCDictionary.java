package ca.concordia.clac.parallel.corpus.builder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.xml.sax.SAXException;

import com.google.common.collect.SetMultimap;
import com.google.common.collect.TreeMultimap;

import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableEntry;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableReader;
import ca.concordia.clac.parallel.corpus.moses.phrase_table.PhraseTableTextFilter;

public class DCDictionary implements Predicate<PhraseTableEntry>, Serializable{
	private static final long serialVersionUID = 1L;
	public static double THRESHOLD = 0.1;

	public static class DicEntry implements Comparable<DicEntry>, Serializable{
		private static final long serialVersionUID = 1L;

		int fPhrase;
		int fTranslation;
		double prob;
		String translation;

		public DicEntry(String translation, double prob, int fPhrase, int fTranslation){
			this.fPhrase = fPhrase;
			this.fTranslation = fTranslation;
			this.translation = translation;
			this.prob = prob;
		}

		@Override
		public int compareTo(DicEntry o) {
			double diff = o.fTranslation - fTranslation;
			return (int)Math.signum(diff);
		}

		@Override
		public String toString() {
			return String.format("%s\t%f\t%d\t%d", translation, prob, fPhrase, fTranslation);
		}

		@Override
		public boolean equals(Object obj) {
			return EqualsBuilder.reflectionEquals(this, obj);
		}
		
		public String getTranslation() {
			return translation;
		}

		public int getfPhrase() {
			return fPhrase;
		}

		public int getfTranslation() {
			return fTranslation;
		}

		public double getProb() {
			return prob;
		}
	}

	SetMultimap<String, DicEntry> enDcToFr = TreeMultimap.create();
	SetMultimap<String, DicEntry> frDcToEn = TreeMultimap.create();
	Map<String, Integer> enCounts = new HashMap<>();
	Map<String, Integer> frCounts = new HashMap<>();
	
	Map<String, String> noSpaceToStdForm = new HashMap<>();
	Function<String, String> normalizer;

	public DCDictionary(Function<String, String> normalizer) throws FileNotFoundException {
		this.normalizer = normalizer;
	}



	@Override
	public boolean test(PhraseTableEntry entry) {
		String englishText = entry.getEnEntry().getText();
		double pEnGivenFr = entry.getEnEntry().getProbGivenSource();
		int fEn = entry.getEnEntry().getFreq();
		
		String frenchText = entry.getFrEntry().getText();
		double pFrGivenEn = entry.getFrEntry().getProbGivenSource();
		int fFr = entry.getFrEntry().getFreq();
		int fPhrase = entry.getfPhrase();
		
		enDcToFr.put(englishText, new DicEntry(frenchText, pFrGivenEn, fFr, fPhrase));
		frDcToEn.put(frenchText, new DicEntry(englishText, pEnGivenFr, fEn, fPhrase));
		enCounts.put(englishText, fEn);
		enCounts.put(englishText, fFr);
		return false;
	}


	public void writeToFile(File directory) throws IOException{
		write(new File(directory, "en2fr.txt"), enDcToFr, enCounts);
		write(new File(directory, "fr2en.txt"), frDcToEn, frCounts);
		
//		ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(new File(directory, "dic.obj")));
//		output.writeObject(this);
//		output.close();
	}

	public SetMultimap<String, DicEntry> getEnDcToFr() {
		return enDcToFr;
	}
	
	public SetMultimap<String, DicEntry> getFrDcToEn() {
		return frDcToEn;
	}
	
	private void write(File file, SetMultimap<String, DicEntry> dictionary, Map<String, Integer> counts) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(file);

		for (String key: dictionary.keySet()){
			Integer count = counts.get(key);
			if (count == null)
				count = 0;
			pw.println(key + "\t" + count);
			Set<DicEntry> entries = dictionary.get(key);
			for (DicEntry entry: entries){
				pw.print("\t");
				if (entry.fPhrase != 0 || entries.size() == 1)
					pw.println(entry.toString());

			}
			pw.println();
		}

		pw.close();
	}
	
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		File phraseTableDir = new File("outputs/phrase-table/");
		File outputDir = new File("outputs/dic/");
		outputDir.mkdirs();

		for (File phraseTableFile: phraseTableDir.listFiles()){
			DCDictionary dictionary = new DCDictionary(PhraseTableTextFilter.defaultNormalizer);
			PhraseTableReader phraseTableReader = new PhraseTableReader();
			phraseTableReader.setFilter(dictionary);
			phraseTableReader.parse(phraseTableFile);

			File outDir = new File(outputDir, phraseTableFile.getName());
			outDir.mkdirs();
			dictionary.writeToFile(outDir);
		}

		System.out.println("DCDictionary.main()");
	}
}
