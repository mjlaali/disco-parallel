package ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.crowdFlower.util.CrowdFlowerMerger;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.AlignmentFeature;
import ca.concordia.clac.parallel.corpus.sampling.dc_alignment.featureExtractor.Tag;
import ca.concordia.clac.util.csv.CSVContent;
import ca.concordia.clac.util.csv.CSVMerger;
import ca.concordia.clac.util.csv.CSVUtils;
import ca.concordia.clac.util.csv.MergeStrategy;
import ca.concordia.clac.util.csv.strategy.merge.PriorityMerger;

/**
 * read a full report (full-report.csv) of a CrowdFlower project plus a CrowdFlower contributors csv file (contributors.csv) 
 * then merged all rowed (aggregate them) and generate a csv file that contains alignments of each line with correct indexes. 
 * Then the csv file is merged with the csv file of the original dataset that the CrowdFlower project is based on.
 * @author majid
 *
 */
public class FullReportReader {
	public static final String WORKER_ID_COLUMN = "worker_id";
	private final Map<String, String> contributorNames;
	private final String judgmentColumn;
	private final String sourceTextColumn;
	private final String targetTextColumn;
	private final String beginTargetTextColumn;
	
	public FullReportReader(final Map<String, String> contributorNames, final String judgmentColumn, 
			final String sourceTextColumn, final String targetTextColumn, final String beginTargetTextColumn) {
		this.contributorNames = contributorNames;
		this.judgmentColumn = judgmentColumn;
		this.sourceTextColumn = sourceTextColumn;
		this.targetTextColumn = targetTextColumn;
		this.beginTargetTextColumn = beginTargetTextColumn;
	}

	public CSVContent summary(final CSVContent content, final String alignmentsColumn){
		String summaryWorkerIdColumn = "_" + WORKER_ID_COLUMN;
		for (Entry<String, String> worderId2Name: contributorNames.entrySet()) {
			content.replace(summaryWorkerIdColumn, worderId2Name.getKey(), worderId2Name.getValue());
		}
		
		CSVUtils processor = new CSVUtils(content);
		processor.addColumn(alignmentsColumn, judgmentColumn, summaryWorkerIdColumn, (a, b) -> {
			List<AlignmentFeature> alignments = Arrays.asList(new AlignmentFeature(a, b, -1, -1, -1));
			return AlignmentFeature.convertToString(alignments);
		});
		
		processor.updateColumn(alignmentsColumn, "_trust", (a, b) -> {
			List<AlignmentFeature> alignments = AlignmentFeature.parse(a);
			alignments.get(0).setScore(Double.parseDouble(b));
			return AlignmentFeature.convertToString(alignments);
		});
		
		CrowdFlowerMerger.fixAlignmentIndexes(processor.getCsvContent(), alignmentsColumn, beginTargetTextColumn, targetTextColumn);
		
		
		MergeStrategy mergeStrategy  = (a, b, k) -> k.equals(judgmentColumn) ? a + "\n" + b: null;
		mergeStrategy = mergeStrategy.andThen(AlignmentFeature.getMergeStrategy(alignmentsColumn))
					 .andThen((a, b, k) -> a);
		
		processor.mergeRows(sourceTextColumn, mergeStrategy);
		
		CSVContent results = processor.getCsvContent();
		List<String> header = results.getHeader();
		Set<String> importantColumns = new HashSet<>(Arrays.asList(sourceTextColumn, alignmentsColumn));
		for (int i = header.size() - 1; i >= 0; i--){
			String currentHeader = header.get(i);
			if (!importantColumns.contains(currentHeader)){
				results.deleteColumn(i);
			}
		}

		return results;
	}
	
	public static Map<String, String> getContributorsName(final CSVContent csvContent, final String contributorsNameColumn){
		Map<String, String> contributorsName = new HashMap<>();
		List<String> ids = csvContent.getColumn(WORKER_ID_COLUMN);
		List<String> names = csvContent.getColumn(contributorsNameColumn);
		for (int i = 0; i < ids.size(); i++){
			String preName = contributorsName.put(ids.get(i), names.get(i));
			if (preName != null)
				throw new RuntimeException("An id maped to two different names.");
		}
		return contributorsName;
	}
	
	public static CSVContent mergeWithDataset(final CSVContent dataset, final CSVContent summary, final String sourceTextColumn, final String alignmentsColumn){
		List<String> headers = dataset.getHeader();
		String orgSourceTextColumn = null;
		for (String header: headers){
			String normaledHeader = header.replace(Tag.SEPARATOR, "").toLowerCase();
			if (normaledHeader.equals(sourceTextColumn)){
				orgSourceTextColumn = header;
			}
		}
		if (orgSourceTextColumn == null)
			throw new RuntimeException("Cannot find the original source text column header");
		
		Set<Tag> tags = Tag.parse(orgSourceTextColumn);
		Tag lang = Tag.FR;
		if (tags.contains(Tag.EN)) {
			lang = Tag.EN;
		}
		String orgAlignmentColumn = Tag.tagToString(lang, Tag.dc, Tag.alignment);
		if (!headers.contains(orgAlignmentColumn)){
			throw new RuntimeException("Cannot find the alignment column");
		}
		
		summary.renameColumn(sourceTextColumn, orgSourceTextColumn);
		summary.renameColumn(alignmentsColumn, orgAlignmentColumn);
		
		CSVMerger merger = new CSVMerger(orgSourceTextColumn, AlignmentFeature.getMergeStrategy(orgAlignmentColumn).andThen(new PriorityMerger(0)));
		merger.merge(dataset);
		merger.merge(summary);
		
		return merger.getCsvContent();
	}

	public static void main(String[] args) throws IOException {
//		mergeTestQuestions();
		mergeSampledDataset();
	}

	public static void mergeTestQuestions() throws IOException {
		File datasetFld = new File("/Users/majid/Documents/git/parallel.corpus.sampling/parallel.corpus.sampling/");
		File dataste = new File(datasetFld, "resources/crowdFlower/emnlp-dataset/sampled.csv");
		File baseFld = new File(datasetFld, "resources/crowdFlower/emnlp-dataset/test-questions");
		File csvFile = new File(baseFld, "full-report.csv");
		File outputFile = new File(baseFld, "full-report-converted.csv");
		File mergedDatasetFile = new File(baseFld, "merged-" + dataste.getName());
		File contributersFile = new File(baseFld, "contributors.csv");
		
		
		String judgmentColumn = "english_marker";
		String sourceTextColumn = "frchunkbolded";
		String targetTextColumn = "frchunkalignmentsuperscript";
		String beginTargetTextColumn = "frchunkbeginalignment";
		
		CSVContent csvContent = new CSVContent(csvFile);
		
		Map<String, String> contributorsName = getContributorsName(new CSVContent(contributersFile), "name");
		
		String alignmentsColumn = "alignments";
		CSVContent summary = new FullReportReader(contributorsName, judgmentColumn, sourceTextColumn, targetTextColumn, beginTargetTextColumn)
				.summary(csvContent, alignmentsColumn);
		summary.save(outputFile);
		mergeWithDataset(new CSVContent(dataste), summary, sourceTextColumn, alignmentsColumn).save(mergedDatasetFile);
		
		
		System.out.println("FullReportReader.main()");
	}
	
	private static void mergeSampledDataset() throws IOException {
		File datasetFld = new File("/Users/majid/Documents/git/parallel.corpus.sampling/parallel.corpus.sampling/");
		File dataste = new File(datasetFld, "resources/crowdFlower/emnlp-dataset/sampled.csv");
		File baseFld = new File(datasetFld, "resources/crowdFlower/emnlp-dataset/");
		File csvFile = new File(baseFld, "full-report-fixed-error.csv");
		File outputFile = new File(baseFld, "full-report-converted.csv");
		File mergedDatasetFile = new File(baseFld, "merged-" + dataste.getName());
		File contributersFile = new File(baseFld, "contributors.csv");
		
		
		String judgmentColumn = "english_marker";
		String sourceTextColumn = "frchunkbolded";
		String targetTextColumn = "frchunkalignmentsuperscript";
		String beginTargetTextColumn = "frchunkbeginalignment";
		
		CSVContent csvContent = new CSVContent(csvFile);
		
		Map<String, String> contributorsName = getContributorsName(new CSVContent(contributersFile), "name");
		
		String alignmentsColumn = "alignments";
		CSVContent summary = new FullReportReader(contributorsName, judgmentColumn, sourceTextColumn, targetTextColumn, beginTargetTextColumn)
				.summary(csvContent, alignmentsColumn);
		summary.save(outputFile);
		mergeWithDataset(new CSVContent(dataste), summary, sourceTextColumn, alignmentsColumn).save(mergedDatasetFile);
		
		
		System.out.println("FullReportReader.main()");
	}
}
