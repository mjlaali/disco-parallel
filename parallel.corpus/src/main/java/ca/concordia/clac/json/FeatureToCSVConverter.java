package ca.concordia.clac.json;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.cleartk.ml.Feature;
import org.cleartk.ml.Instance;

import com.google.gson.JsonSyntaxException;

public class FeatureToCSVConverter implements FeatureConverter {
	private int numOutput = 5;
	private int cnt;
	private List<String> headers = null;
	private CSVPrinter printer;
	private boolean noHeader;


	public FeatureToCSVConverter(int numOutput, File output, boolean append) throws IOException {
		this.numOutput = numOutput;
		printer = new CSVPrinter(new FileWriter(output, append), CSVFormat.RFC4180);
		noHeader = false;
		if (output.exists() && append){
			noHeader = true;
		}

	}

	public boolean convert(Instance<?> fromJson) throws IOException{
		List<Feature> features = fromJson.getFeatures();
		List<String> featureNames = features.stream().map(Feature::getName).collect(Collectors.toList());
		List<String> featureValues = features.stream().map(Feature::getValue).map((obj) -> obj == null ? "null" : obj.toString()).collect(Collectors.toList());

		if (headers == null){
			headers = featureNames;
			if (!noHeader)
				printer.printRecord(headers);
		}

		if (!featureNames.equals(headers)){
			throw new RuntimeException("Some features are missing or added: " + headers.toString() + "<>" + featureNames.toString());
		}

		printer.printRecord(featureValues);
		cnt++;
		if (cnt == numOutput)
			return false;
		return true;
	}

	public static void main(String[] args) throws JsonSyntaxException, IOException {

		File json = new File("outputs/connective-instances/");
		File output = new File("outputs/CrowdFlower");
		output.mkdirs();

		for (File dir: json.listFiles()){
			String sense = dir.getName();
			sense = sense.substring(sense.lastIndexOf('-') + 1);
			new FeatureJSonConverter(new FeatureToCSVConverter(5, new File(output, sense + ".csv"), true)).convert(new File(dir, "features.json"));
		}
	}

	@Override
	public void close() throws IOException {
		printer.close();

	}
}
