package ca.concordia.clac.json;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.cleartk.ml.CleartkProcessingException;
import org.cleartk.ml.DataWriter;
import org.cleartk.ml.Instance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JSonDataWriter<OUTCOME_TYPE> implements DataWriter<OUTCOME_TYPE>{
	public static final String JSON_FILE_NAME = "features.json";
	private PrintStream ps;
	private boolean first = true;
	private Gson gson;

	public JSonDataWriter(File outputDir) throws IOException {
		ps = new PrintStream(new FileOutputStream(new File(outputDir, JSON_FILE_NAME)), true, "UTF-8");
		ps.println("[");
		GsonBuilder builder = new GsonBuilder();
//		builder.setPrettyPrinting();
		gson = builder.create();
	}

	@Override
	public void write(Instance<OUTCOME_TYPE> instance) throws CleartkProcessingException {
		if (!first){
			ps.println(",");
		}
		first = false;
		String json = gson.toJson(instance);
		ps.print(json);
	}

	@Override
	public void finish() throws CleartkProcessingException {
		ps.println();
		ps.println("]");
		ps.close();
	}

}
