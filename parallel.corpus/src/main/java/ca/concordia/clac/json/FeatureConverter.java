package ca.concordia.clac.json;

import java.io.Closeable;
import java.io.IOException;

import org.cleartk.ml.Instance;

public interface FeatureConverter extends Closeable{

	public boolean convert(Instance<?> instance) throws IOException;
	public void close() throws IOException;
}
