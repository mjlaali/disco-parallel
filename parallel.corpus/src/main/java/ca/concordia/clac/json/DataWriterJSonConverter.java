package ca.concordia.clac.json;

import java.io.File;
import java.io.IOException;

import org.apache.uima.UIMAException;
import org.cleartk.ml.CleartkProcessingException;
import org.cleartk.ml.DataWriter;
import org.cleartk.ml.Instance;
import org.cleartk.ml.weka.WekaStringOutcomeDataWriter;

import com.google.gson.JsonSyntaxException;
import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

public class DataWriterJSonConverter<OUTCOME> implements FeatureConverter{
	DataWriter<OUTCOME> dataWriter;
	
	public DataWriterJSonConverter(DataWriter<OUTCOME> dataWriter) {
		this.dataWriter = dataWriter;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean convert(Instance<?> instance) throws IOException {
		try {
			dataWriter.write((Instance<OUTCOME>)instance);
		} catch (CleartkProcessingException e) {
			throw new RuntimeException(e);
		}
		return true;
	}

	@Override
	public void close() throws IOException {
		try {
			dataWriter.finish();
		} catch (CleartkProcessingException e) {
			throw new RuntimeException(e);
		}
	}
	
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputJson", 
				description = "Input json file" )
		public File getInputJsonFile();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();

	}
	
	public static void main(String[] args) throws UIMAException, JsonSyntaxException, IOException{
		Options options = CliFactory.parseArguments(Options.class, args);
		
		System.out.println("ParallelTextExtractor.main(): Input = " + options.getInputJsonFile());
		System.out.println("ParallelTextExtractor.main(): Output = " + options.getOutputDir());
		
		File json = options.getInputJsonFile();
		File outputFld = options.getOutputDir();
		
		WekaStringOutcomeDataWriter dataWriter = new WekaStringOutcomeDataWriter(outputFld);
		new FeatureJSonConverter(new DataWriterJSonConverter<>(dataWriter)).convert(json);
	}
}
