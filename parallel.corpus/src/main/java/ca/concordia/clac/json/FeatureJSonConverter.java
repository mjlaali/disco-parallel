package ca.concordia.clac.json;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.cleartk.ml.Instance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class FeatureJSonConverter {
	public static final String NEW_LINE_SEPARATOR = "\n";
	private FeatureConverter converter;
	
	public FeatureJSonConverter(FeatureConverter converter) {
		this.converter = converter;
	}
	
	public void convert(File json) throws JsonSyntaxException, IOException{
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.create();

		BufferedReader br = new BufferedReader(new FileReader(json));
		String line;
		try{
			while ((line = br.readLine()) != null){
				if (line.trim().equals("[") || line.trim().equals("]"))
					continue;

				if (line.endsWith(","))
					line = line.substring(0, line.length() - 1);
				Instance<?> fromJson = gson.fromJson(line, Instance.class);
				
				if (fromJson == null || !converter.convert(fromJson)){
					break;
				}
			}
		}finally {
			br.close();
			converter.close();
		}
	}
}
