package ca.concordia.clac.discourse;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.math3.util.Pair;

import ca.concordia.clac.util.csv.CSVContent;
import ir.laali.tools.ds.DSManagment;
import ir.laali.tools.ds.DSPrinter;

public class ClassifierDifference {
	private Map<String, Integer> dcToGoldCnt = new TreeMap<>();
	private Map<String, List<Double>> dcToF1 = new TreeMap<>();
	private List<String> labels = new ArrayList<>();
	private String metric;

	public ClassifierDifference(String metric) {
		this.metric = metric;
	}
	public void add(File classifierResults) throws IOException{
		labels.add(classifierResults.getParentFile().getParentFile().getName());

		CSVContent csvContent = new CSVContent(classifierResults, '\t');
		List<String> dcs = csvContent.getColumn("dc");
		List<String> fscores = csvContent.getColumn(metric);
		List<String> goldCnt = csvContent.getColumn("Gold");

		for (int i = 0; i < dcs.size(); i++){
			String dc = dcs.get(i).trim();
			if (dc.length() == 0)
				continue;
			DSManagment.addToList(dcToF1, dc, Double.parseDouble(fscores.get(i).trim()));
			dcToGoldCnt.put(dc, Integer.parseInt(goldCnt.get(i).trim()));
		}

	}

	private static List<List<String>> getCombination(List<String> aList){
		List<List<String>> results = new ArrayList<>();

		if (aList.size() == 1){
			results.add(new ArrayList<>(aList));
		} else {
			for (int i = 0;i < aList.size(); i++){
				String removed = aList.remove(i);
				List<List<String>> combinations = getCombination(aList);
				for (List<String> aCombination: combinations){
					aCombination.add(removed);
					results.add(aCombination);
				}
				aList.add(i, removed);
			}
		}

		return results;
	}

	public Map<String, Map<String, List<Number>>> getDifferences(){
		final String THE_SMAE = "The Same";
		Map<String, Map<String, List<Number>>> diff = new TreeMap<>();
		
		for (List<String> aCombination: getCombination(labels)){
			diff.put(aCombination.toString(), new TreeMap<>());
		}
		diff.put(THE_SMAE, new TreeMap<>());
		
		for (Entry<String, List<Double>> anEntry: dcToF1.entrySet()){
			String dc = anEntry.getKey();
			List<Double> fscores = anEntry.getValue();
			List<Pair<String, Double>> fscoreLabels = new ArrayList<>();
			for (int i = 0;i < fscores.size(); i++){
				fscoreLabels.add(new Pair<>(labels.get(i), fscores.get(i)));
			}
			
			Collections.sort(fscoreLabels, (a, b) -> b.getValue().compareTo(a.getValue()));
			
			if (dcToGoldCnt.get(dc) > 0){
				String label = THE_SMAE;
				if (fscoreLabels.get(0).getValue() > fscoreLabels.get(fscoreLabels.size() - 1).getValue()){
					label = fscoreLabels.stream().map((a) -> a.getKey()).collect(Collectors.toList()).toString();
				} 
					
				Map<String, List<Number>> dcStats = diff.get(label);
				List<Number> stats = new ArrayList<>();
				stats.add(dcToGoldCnt.get(dc));
				stats.addAll(fscoreLabels.stream().map((a) -> a.getValue()).collect(Collectors.toList()));
				dcStats.put(dc, stats);
			}
			
		}
		
		return diff;
	}

	public static void main(String[] args) throws IOException {
		File[] modelDirs = new File[]{
//				new File("outputs/fdtb-base/model/"),
				new File("/Users/majid/Documents/resource/discourseParsing-fr-dc-projected-features"),
				new File("/Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation/outputs/fdtb-gold/model/"),
		};

		ClassifierDifference classifierDifference = new ClassifierDifference("F");
		for (File modelDir: modelDirs){
			classifierDifference.add(new File(modelDir, TestDCClassifierFromJSon.REPORT_FILE));
		}

		DSPrinter.printMap("Diff", classifierDifference.getDifferences().entrySet(), System.out, (a) -> a.size() + ":" + a.toString());
	}
}
