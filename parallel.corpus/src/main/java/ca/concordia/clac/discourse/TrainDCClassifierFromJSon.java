package ca.concordia.clac.discourse;

import java.io.File;

import org.cleartk.ml.jar.Train;
import org.cleartk.ml.opennlp.maxent.MaxentStringOutcomeDataWriter;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.json.DataWriterJSonConverter;
import ca.concordia.clac.json.FeatureJSonConverter;

public class TrainDCClassifierFromJSon {

	public void train(File jsonFile, File outputFld) throws Exception{
		MaxentStringOutcomeDataWriter dataWriter = new MaxentStringOutcomeDataWriter(outputFld);
		new FeatureJSonConverter(new DataWriterJSonConverter<>(dataWriter)).convert(jsonFile);
		Train.main(outputFld, "1000", "10");
	}
	
	interface Options{
		@Option(
				shortName = "i",
				longName = "inputJson", 
				description = "Input dataset that contains all parsed files" )
		public File getInputJsonFile();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();

	}
	
	public static void main(String[] args) throws Exception{
		Options options = CliFactory.parseArguments(Options.class, args);
		
		System.out.println("ParallelTextExtractor.main(): Input = " + options.getInputJsonFile());
		System.out.println("ParallelTextExtractor.main(): Output = " + options.getOutputDir());
		
		new TrainDCClassifierFromJSon().train(options.getInputJsonFile(), options.getOutputDir());
	}
}
