package ca.concordia.clac.discourse;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.function.Function;

import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.discourse.type.DiscourseConnective;

import com.lexicalscope.jewel.cli.CliFactory;
import com.lexicalscope.jewel.cli.Option;

import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.json.JSonDataWriter;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.ml.classifier.InstanceExtractor;
import ca.concordia.clac.ml.classifier.StringClassifierLabeller;
import ca.concordia.clac.parallel.corpus.builder.AnnotationProjectionDatasetBuilder;
import ca.concordia.clac.uima.Utils;
import ca.concordia.clac.uima.engines.LookupInstanceExtractor;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;

public class AnnotationProjectionJSonFile extends FrConnectiveClassifier{
	
	@Override
	public InstanceExtractor<DiscourseConnective> getExtractor(JCas aJCas) {
		return (jcas) -> JCasUtil.select(jcas, DiscourseConnective.class);
	}
	
	

	@Override
	public Function<DiscourseConnective, String> getLabelExtractor(JCas aJCas) {
		return (dc) -> {
			return Boolean.toString(dc.getSense() != null);
		};
	}

	public static AnalysisEngineDescription getWriterDescription(File outputFld, URL lexconnFile) throws ResourceInitializationException, MalformedURLException{
		File dcList = new File(outputFld, "dc.list");
		return StringClassifierLabeller.getWriterDescription(
				AnnotationProjectionJSonFile.class,
				JSonDataWriter.class,
				outputFld, 
				FrConnectiveClassifier.PARAM_GENERATE_DC_LIST, true,
				FrConnectiveClassifier.PARAM_JUST_CANONICAL_FEATURE, false,
				LookupInstanceExtractor.PARAM_LOOKUP_FILE_URL, dcList.toURI().toURL().toString(),
				LookupInstanceExtractor.PARAM_ANNOTATION_FACTORY_CLASS_NAME, DiscourseAnnotationFactory.class.getName(), 
				FrConnectiveClassifier.PARAM_LEXCONN_FILE, lexconnFile
				);
	}

	interface Options{
		@Option(
				shortName = "i",
				longName = "inputDataset", 
				description = "Input dataset that contains all parsed files" )
		public File getInputDataset();
		
		@Option(
				shortName = "o",
				longName = "outputDir",
				description = "Specify the output directory which stores the output files.")
		public File getOutputDir();

		@Option(
				defaultToNull = true,
				shortName = "a",
				longName = "alignmentFile", 
				description = "The word alignment file")
		public File getAlignmentFile();
		
		@Option(
				shortName = "f",
				longName = "filterNotReliable",
				description = "Filter not reliable annotaitons")
		public Boolean isFilterOn();
	}
	
	public static void main(String[] args) throws Exception{
		Options options = CliFactory.parseArguments(Options.class, args);
		
		File datasetFld = options.getInputDataset();
		File outputFld = options.getOutputDir();
		URL lexconnFile = DefaultLexconnReader.getDefaultLexconnFile();
		
		System.out.println("ParallelTextExtractor.main(): Input = " + datasetFld);
		System.out.println("ParallelTextExtractor.main(): Output = " + outputFld);
		
		
		try {

			if (outputFld.exists())
				FileUtils.deleteDirectory(outputFld);
			outputFld.mkdirs();
			
			CollectionReader reader = CollectionReaderFactory.createReader(XmiReader.class, 
					XmiReader.PARAM_SOURCE_LOCATION, datasetFld, 
					XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
			
			AggregateBuilder builder = null;
			
			System.out.println("Filter is " + (options.isFilterOn() ? "on." : "off."));
			
			if (options.getAlignmentFile() == null)
				builder = new AggregateBuilder();
			else 
//				builder = new AggregateBuilder();
				builder = AnnotationProjectionDatasetBuilder.buildAlignmentBuilder(options.getAlignmentFile(), options.isFilterOn(), options.getOutputDir());
			
			builder.add(AnalysisEngineFactory.createEngineDescription(XmiWriter.class, 
					XmiWriter.PARAM_TARGET_LOCATION, new File(options.getOutputDir(), "xmi"), 
					XmiWriter.PARAM_OVERWRITE, true));
			builder.add(getWriterDescription(outputFld, lexconnFile), CAS.NAME_DEFAULT_SOFA, EuroparlParallelTextAnnotator.FR_TEXT_VIEW);

			Utils.runWithProgressbar(reader,
					builder.createAggregateDescription()
					);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
