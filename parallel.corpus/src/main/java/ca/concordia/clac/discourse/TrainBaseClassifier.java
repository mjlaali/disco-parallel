package ca.concordia.clac.discourse;

import java.io.File;
import java.net.URL;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.cleartk.eval.EvaluationConstants;
import org.cleartk.ml.jar.Train;
import org.cleartk.ml.weka.WekaStringOutcomeDataWriter;

import ca.concordia.clac.batch_process.BatchProcess;
import ca.concordia.clac.discourse.parser.dc.disambiguation.DiscourseAnnotationFactory;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.ml.classifier.StringClassifierLabeller;
import ca.concordia.clac.uima.engines.LookupInstanceExtractor;

public class TrainBaseClassifier {
	public void train() throws Exception{
		File fdtbBaseDir = new File("/Users/majid/Documents/git/french-connective-disambiguation/connective-disambiguation");
		File output = new File("outputs/fdtb-base/");
		File modelDir = new File(output, "model/");
		URL lexconnFile = DefaultLexconnReader.getDefaultLexconnFile();
		modelDir.mkdirs();
		
		BatchProcess process = new FDTBPipelineFactory(fdtbBaseDir).getInstance(new File(output, "process"), false);
		
		File dcList = new File(output, "dc.list");
		AnalysisEngineDescription wekaClassifier = StringClassifierLabeller.getWriterDescription(
				FrConnectiveClassifier.class,
				WekaStringOutcomeDataWriter.class, modelDir, 
				FrConnectiveClassifier.PARAM_GENERATE_DC_LIST, true,
				FrConnectiveClassifier.PARAM_JUST_CANONICAL_FEATURE, true,
				LookupInstanceExtractor.PARAM_LOOKUP_FILE_URL, dcList.toURI().toURL().toString(),
				LookupInstanceExtractor.PARAM_ANNOTATION_FACTORY_CLASS_NAME, DiscourseAnnotationFactory.class.getName(), 
				FrConnectiveClassifier.PARAM_LEXCONN_FILE, lexconnFile
				);
		process.addProcess("train", EvaluationConstants.GOLD_VIEW, wekaClassifier);
		
		process.clean("train");
		process.run();
		
		trainModel(modelDir);
	}
	
	public static void trainModel(File modelDir) throws Exception {
		Train.main(modelDir, "weka.classifiers.bayes.NaiveBayes");
	}
	

	public static void main(String[] args) throws Exception {
		new TrainBaseClassifier().train();
		System.out.println("TrainModel.main(): Done.");
	}
}
