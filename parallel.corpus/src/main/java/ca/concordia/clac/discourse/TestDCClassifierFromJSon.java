package ca.concordia.clac.discourse;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.CAS;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.component.ViewTextCopierAnnotator;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.factory.CollectionReaderFactory;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.discourse_parsing.module.dcAnnotator.DCEvaluator;
import org.cleartk.eval.EvaluationConstants;
import org.xml.sax.SAXException;

import ca.concordia.clac.fdtb.FDTBGoldAnnotator;
import ca.concordia.clac.lexconn.DefaultLexconnReader;
import ca.concordia.clac.uima.engines.ViewAnnotationCopier;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;

public class TestDCClassifierFromJSon {
	public static final String REPORT_FILE = "report.txt";
	File xmiDir;
	
	public TestDCClassifierFromJSon(File xmiDir) {
		xmiDir.mkdirs();
		this.xmiDir = xmiDir;
	}
	
	public void addTestCollection(File discorseAnnotationFile, File preprocessedXmiDir) throws ResourceInitializationException, UIMAException, IOException{
		CollectionReaderDescription reader = CollectionReaderFactory.createReaderDescription(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, preprocessedXmiDir.getAbsolutePath(), 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		
		AggregateBuilder pipeline = createGoldView(discorseAnnotationFile);
		
		AnalysisEngineDescription xmiWriter = createEngineDescription(XmiWriter.class, 
				XmiWriter.PARAM_TARGET_LOCATION, xmiDir, 
				XmiWriter.PARAM_OVERWRITE, true);
		pipeline.add(xmiWriter);
		
		SimplePipeline.runPipeline(reader, pipeline.createAggregateDescription());
		
	}

	public void test(File modelDir, File output, boolean justCanonical) throws URISyntaxException, UIMAException, IOException, ParserConfigurationException, SAXException{
		
		CollectionReaderDescription reader = CollectionReaderFactory.createReaderDescription(XmiReader.class, 
				XmiReader.PARAM_SOURCE_LOCATION, xmiDir.getAbsolutePath(), 
				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
		
		AggregateBuilder pipeline = new AggregateBuilder();
		AnalysisEngineDescription classifier = FrConnectiveClassifier.getClassifierDescription(modelDir.toURI().toURL(), justCanonical);
		pipeline.add(classifier);
		File frDcToHead = new File(output.getParentFile(), "frDc2Head.txt");
		buildFrDcToHead(frDcToHead);
		AnalysisEngineDescription evaluator = DCEvaluator.getDescription(output.getAbsolutePath(), frDcToHead.getAbsolutePath());
		pipeline.add(evaluator);
		
		SimplePipeline.runPipeline(reader, pipeline.createAggregateDescription());
		
		System.out.println(FileUtils.readFileToString(output, StandardCharsets.UTF_8));
	}

	private void buildFrDcToHead(File frDcToHead) throws ParserConfigurationException, SAXException, IOException {
		DefaultLexconnReader lexconn = DefaultLexconnReader.getLexconnMap();
		Map<String, String> formToId = lexconn.getFormToId();
		Map<String, String> idToCanonicalForm = lexconn.getIdToCanonicalForm();
		
		PrintStream ps = new PrintStream(new FileOutputStream(frDcToHead), true, "UTF-8");
		formToId.forEach((f, id) -> ps.printf("%s:%s(%s)\n", f, idToCanonicalForm.get(id), id));
		
		ps.close();
	}

	private AggregateBuilder createGoldView(File discourseAnnotation) throws ResourceInitializationException {
		AggregateBuilder aggregateBuilder = new AggregateBuilder();
		
		aggregateBuilder.add(createEngineDescription(ViewTextCopierAnnotator.class, 
				ViewTextCopierAnnotator.PARAM_SOURCE_VIEW_NAME, CAS.NAME_DEFAULT_SOFA, 
				ViewTextCopierAnnotator.PARAM_DESTINATION_VIEW_NAME, EvaluationConstants.GOLD_VIEW));
		aggregateBuilder.add(createEngineDescription(ViewAnnotationCopier.class, 
				ViewAnnotationCopier.PARAM_TARGET_VIEW_NAME, EvaluationConstants.GOLD_VIEW));
	
		AnalysisEngineDescription goldAnnotator = FDTBGoldAnnotator.getDescription(discourseAnnotation.getAbsolutePath());
		aggregateBuilder.add(goldAnnotator, CAS.NAME_DEFAULT_SOFA, EvaluationConstants.GOLD_VIEW);

		return aggregateBuilder;
	}
	
	public static void main(String[] args) throws UIMAException, URISyntaxException, IOException, ParserConfigurationException, SAXException {
		boolean fdtb = false;
		File[] discorseAnnotationFiles = fdtb ? new File[]{
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/original/fdtb1/fdtb1.xml"),
		}: new File[]{
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/original/d_sequoia/d_frwiki_50.1000.xml"),
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/original/d_sequoia/d_annodis.er.xml"),
		};
		File[] preprocessedXmiDirs = fdtb ? new File[]{
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/conll-format/fdtb/xmi"),
		}: new File[]{
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/conll-format/frwiki/xmi"),
				new File("/Users/majid/git/french-dc-disambiguation/data/fdtb-project/conll-format/annodis/xmi")
		};
		
		File inducedModelsBaseDir = new File("/Users/majid/Documents/resource/europarl-dataset-17-03-18/models");
		File[] modelDirs = new File[]{
			new File("outputs/fdtb-base/model/"),
			new File("/Users/majid/git/french-dc-disambiguation/outputs/fdtb-berkely/model/"),
			new File("/Users/majid/Documents/resource/discourseParsing-fr-dc-projected-features"),
			new File(inducedModelsBaseDir, "intersection"),
			new File(inducedModelsBaseDir, "grow-diag"),
			new File(inducedModelsBaseDir, "grow-diag-vanilla"),
			new File(inducedModelsBaseDir, "fr-en"),
			new File(inducedModelsBaseDir, "en-fr"),
		};
		
		File xmiDir = new File("outputs/evaluation/xmi");
		if (xmiDir.exists()){
			FileUtils.deleteDirectory(xmiDir);
		}
		
		TestDCClassifierFromJSon tester = new TestDCClassifierFromJSon(xmiDir);
		for (int i = 0; i < discorseAnnotationFiles.length; i++){
			tester.addTestCollection(discorseAnnotationFiles[i], preprocessedXmiDirs[i]);
		}
		
//		if (tester.printTestCollectionStats())
//			return;
		
		for (File modelDir: modelDirs) {
			System.out.println(modelDir.getAbsolutePath());
			tester.test(new File(modelDir, "model.jar"), new File(modelDir, fdtb ? REPORT_FILE + "-fdtb" : REPORT_FILE), modelDir.getAbsolutePath().contains("fdtb-base"));
		}
		
		
	}

//	private boolean printTestCollectionStats() throws UIMAException, IOException {
//		CollectionReaderDescription reader = CollectionReaderFactory.createReaderDescription(XmiReader.class, 
//				XmiReader.PARAM_SOURCE_LOCATION, xmiDir.getAbsolutePath(), 
//				XmiReader.PARAM_PATTERNS, new String[]{"*.xmi"});
//		SimplePipeline.runPipeline(reader, 
//				FrConnectiveClassifier.getWriterDescription(new File(xmiDir.getParent(), "stats"), DefaultLexconnReader.getDefaultLexconnFile()));
//		return true;
//	}
}
