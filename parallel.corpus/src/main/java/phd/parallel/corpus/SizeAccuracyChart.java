package phd.parallel.corpus;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import ca.concordia.clac.utils.DefaultExperiment;
import ca.concordia.clac.utils.DefaultTestCriterion;
import ca.concordia.clac.utils.Experiment;
import ca.concordia.clac.utils.ExperimentUtility;
import ca.concordia.clac.utils.TestCriterion;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class SizeAccuracyChart {

	private Instances train, test;
	
	public SizeAccuracyChart(File trainArffFile, File testArffFile) throws Exception {
		train = DataSource.read(new FileInputStream(trainArffFile));
		train.setClassIndex(train.numAttributes() - 1);
		
		test = DataSource.read(new FileInputStream(testArffFile));
		test.setClassIndex(test.numAttributes() - 1);
	}
	
	public Map<Double, Double> getChart(String classifierConfig, double percentage) throws Exception{
		List<Experiment> experiments = new ArrayList<>();
		int seed = new Random().nextInt();
		for (double p = percentage; p < 1; p = Math.min(p + percentage, 1.0)){
			DefaultExperiment system = new DefaultExperiment();
			system.setClassifier(classifierConfig);
			String format = String.format("weka.filters.unsupervised.instance.Resample -S %.2f -Z %.2f -no-replacement", seed, p * 100);
			system.setFilter(format);
			system.addTestCriterion(new DefaultTestCriterion(new Double(p)));
			experiments.add(system);
		}
		
		Map<TestCriterion, Evaluation> results = ExperimentUtility.run(train, test, experiments);
		Map<Double, Double> chart = new TreeMap<>();
		
		results.forEach((t, e) -> chart.put((Double)t.getId(), e.pctCorrect()));
		
		return chart;
		
	}
	
	public static void main(String[] args) throws Exception {
		File trainArffFile = new File("outputs/aussi-arff-files/europarl-compatible.csv.arff");
		File testArffFile = new File("outputs/aussi-arff-files/fdtb-compatible.csv.arff");
		
		SizeAccuracyChart sizeAccuracyChart = new SizeAccuracyChart(trainArffFile, testArffFile);
		Map<Double, Double> avgChart = new TreeMap<>();
		int numRun = 100;
		for (int i = 0; i < numRun; i++){
			Map<Double, Double> aChart = sizeAccuracyChart.getChart("weka.classifiers.trees.J48 -C 0.25 -M 2", 0.1);
			aChart.forEach((p, a) -> avgChart.put(p, (avgChart.get(p) == null ? 0 : avgChart.get(p)) + a / numRun));
		}
		
		avgChart.forEach((s, a) -> System.out.printf("%.2f\t%.2f\n", s , a));
	}
}
