package phd.parallel.corpus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.factory.AnalysisEngineFactory;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.cleartk.corpus.europarl.EuroparlParallelTextAnnotator;
import org.cleartk.corpus.europarl.type.ParallelChunk;
import org.cleartk.discourse.type.DiscourseConnective;

import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;

public class ParallelSentenceExtractor extends JCasAnnotator_ImplBase{
	public final static String PARAM_OUTPUT_DIR = "outputDir";
	
	public static AnalysisEngineDescription getDescription(File outputDir) throws ResourceInitializationException{
		return AnalysisEngineFactory.createEngineDescription(ParallelSentenceExtractor.class, 
				PARAM_OUTPUT_DIR, 
				outputDir);
	}
	
	@ConfigurationParameter(
			name = PARAM_OUTPUT_DIR)
	private File outputDir;

	@Override
	public void process(JCas aJCas) throws AnalysisEngineProcessException {
		
		try {
			String fileName = getFileName(aJCas);
		    JCas enView = aJCas.getView(EuroparlParallelTextAnnotator.EN_TEXT_VIEW);
		    
			if (JCasUtil.select(enView, DiscourseConnective.class).size() == 0){
				System.err.println("ParallelSentenceExtractor.process(): There is not any discourse connective in " + fileName);
				return;
			}
			List<ParallelChunk> enChunks = new ArrayList<>(JCasUtil.select(
					enView, ParallelChunk.class));
			List<ParallelChunk> frChunks = new ArrayList<>(JCasUtil.select(
					aJCas.getView(EuroparlParallelTextAnnotator.FR_TEXT_VIEW), ParallelChunk.class));
			
			List<Integer> indexes = getValidCandidate(enChunks);
			
			
			
			exportToFile(enChunks, frChunks, indexes, fileName + ".txt");
		} catch (CASException | FileNotFoundException e) {
			throw new AnalysisEngineProcessException(e);
		}
		
	}

	private String getFileName(JCas aJCas) throws AnalysisEngineProcessException {
		DocumentMetaData documentMetaData = DocumentMetaData.get(aJCas);
		
		return documentMetaData.getDocumentId();
	}

	private void exportToFile(List<ParallelChunk> enSents, List<ParallelChunk> frSents, List<Integer> indexes, String fileName) throws FileNotFoundException {
		PrintStream output = new PrintStream(new File(outputDir, fileName));
		for (Integer i: indexes){
			ParallelChunk parallelChunk = enSents.get(i);
			DiscourseConnective dc = JCasUtil.selectCovered(DiscourseConnective.class, parallelChunk).get(0);
			output.printf("%s\n%s\n<%s>:<%s>\n\n", 
					parallelChunk.getCoveredText(), 
					frSents.get(i).getCoveredText(),
					dc.getCoveredText(), 
					dc.getSense());
		}
		output.close();
	}

	private List<Integer> getValidCandidate(List<ParallelChunk> enSents) {
		List<Integer> output = new ArrayList<>();
		
		for (int i = 0; i < enSents.size(); i++){
			ParallelChunk parallelChunk = enSents.get(i);
			List<DiscourseConnective> dcs = JCasUtil.selectCovered(DiscourseConnective.class, parallelChunk);
			if (JCasUtil.selectCovered(Sentence.class, parallelChunk).size() == 1 &&
					dcs.size() == 1){
				output.add(i);
			}
		}
		
		return output;
	}

}
