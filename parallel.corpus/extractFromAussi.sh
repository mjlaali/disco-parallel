#!/bin/bash
java -Xmx12g -cp "target/parallel.corpus-0.0.1-SNAPSHOT.jar:target/dependency/*" -Dlog4j.configurationFile=src/main/java/log4j2.xml ca.concordia.clac.parallel.corpus.DiscourseVsNonDiscourseClassifierUsingAnnotationProjection -i resources/aussi/ -o outputs/aussi/
