#!/bin/bash

java -Xmx100g -cp target/parallel.corpus-2.1.0-SNAPSHOT.jar:target/dependency/* ca.concordia.clac.parallel.corpus.builder.AnnotationProjectionDatasetBuilder -i outputs/europarl/discourseParsing-en -a /nobackup2/clac/majid/data/europarl_en_fr_tokenized/aligned.1.grow-diag-final-and.fr-en -o outputs/europarl-en-discourse-alignments-naive -f
