#!/bin/bash

INPUT=/nobackup2/clac/majid/europarl/aligned/en-fr
OUTPUT=outputs/europarl/

echo $INPUT
echo $OUTPUT

java -Xmx2g -cp target/parallel.corpus-0.0.1-SNAPSHOT.jar:target/dependency/* ca.concordia.clac.parallel.corpus.builder.ParallelTextExtractor -i $INPUT -o $OUTPUT
