#!/bin/bash

java -Xmx100g -cp target/parallel.corpus-0.0.1-SNAPSHOT.jar:target/dependency/* ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator -i /nobackup2/clac/majid/europarl/aligned/en-fr -o outputs/europarl/ -t 1
