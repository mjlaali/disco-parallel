#!/bin/bash
java -Xmx8g -cp "target/parallel.corpus-0.0.1-SNAPSHOT.jar:target/dependency/*" -Dlog4j.configurationFile=src/main/java/log4j2.xml ca.concordia.clac.parallel.corpus.stats.Count -i $1 -o $2
