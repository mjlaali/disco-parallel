#!/bin/bash

rm -rf outputs/x*
for f in /nobackup2/clac/corpora/wmt15/corpus-split/x*
do
	echo $f
	fname=$(basename "$f")
	mkdir "outputs/$fname"
	java -Xmx50g -cp target/parallel.corpus-2.1.0-SNAPSHOT.jar:target/dependency/* ca.concordia.clac.parallel.corpus.ParallelCorpusAnnotator -i $f -o outputs/$fname -t 1 &> outputs/$fname/stderr.txt &
	sleep 1
done
